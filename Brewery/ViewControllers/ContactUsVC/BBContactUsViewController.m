//
//  BBContactUsViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBContactUsViewController.h"

@interface BBContactUsViewController ()

@end

@implementation BBContactUsViewController

#pragma mark - Defaults

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set Title
    self.title = NSLocalizedString(@"Contact Us", nil);
    
    //Update scrollview
    [scrollView setContentSize:background.frame.size];
    
    //set placeholder for textview
    tfMessage.placeholder = @"Your message";
    
    
    //add btnHome in navigationBar
    [self addBtnHome];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - fake data

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    [param setObject:tfName.text       forKey:@"name"];
    [param setObject:tfEmail.text      forKey:@"email"];
    [param setObject:tfMessage.text    forKey:@"message"];
    
    //call API
    [Services callAPIWithUrl:API_CONTACT_US TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                return ;
            }
            
             TDAlertWithParam(nil, [result objectForKey:@"message"]);
            
            tfMessage.text = @"";
            tfEmail.text   = @"";
            tfName.text    = @"";
            
            stopProgressHUD();
        }
    }];
}

#pragma mark - Action

- (IBAction)clickSend:(id)sender
{
    if (![self validateTextFiled]) {
        return;
    }
    
    [self callAPI];
    [scrollView touchesEnded:nil withEvent:nil];
}

- (BOOL)validateTextFiled
{
    if ([tfName.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_NAME);
        return NO;
    }
    
    if ([tfEmail.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_EMAIL);
        return NO;
    }
    if ([tfMessage.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_MESSAGE);
        return NO;
    }
    
    return YES;
}



@end

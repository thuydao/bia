//
//  BBContactUsViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "UIPlaceHolderTextView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface BBContactUsViewController : BaseViewController
{
    
    IBOutlet UIImageView                  *background;
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    
    IBOutlet UIPlaceHolderTextView *tfMessage;
    IBOutlet UITextField *tfName;
    IBOutlet UITextField *tfEmail;
}

- (IBAction)clickSend:(id)sender;

@end

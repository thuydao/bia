//
//  BBChangeUserNameViewController.m
//  Brewery
//
//  Created by Thuy Dao on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBChangeUserNameViewController.h"

@interface BBChangeUserNameViewController ()

@end

@implementation BBChangeUserNameViewController

- (void)dealloc
{
    background = nil;
    scrollView = nil;
    lbNewName = nil;
    lbPassword = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //set Title
    self.title = NSLocalizedString(@"Change Username", nil);
    
    //update scrollview
    [scrollView setContentSize:background.frame.size];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action

- (IBAction)clickChange:(id)sender
{
    QLog(@"");
}

@end

//
//  BBChangeUserNameViewController.h
//  Brewery
//
//  Created by Thuy Dao on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"

@interface BBChangeUserNameViewController : BaseViewController
{
    IBOutlet UIImageView *background;
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UITextField *tfNewName;
    IBOutlet UITextField *tfPassword;
}

- (IBAction)clickChange:(id)sender;

@end

//
//  BBInviteViewController.h
//  Brewery
//
//  Created by Bunlv on 8/21/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBBaseISViewController.h"

@interface BBInviteViewController : BBBaseISViewController

#pragma mark - View default

#pragma mark - SVG
#pragma mark + Set
- (void)setViewInfo;

#pragma mark + Get
- (void)getDataByLoadFromServer;

#pragma mark + Fake

#pragma mark + Add

#pragma mark + Remove

#pragma mark + Control Item
- (void)controlItem;

#pragma mark - Draw

#pragma mark - Call API

#pragma mark - Action

#pragma mark - Delegate

#pragma mark - Subclass need overwrite

#pragma mark - Overwrite

@end

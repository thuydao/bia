//
//  BBStatesListViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBStatesListViewController.h"

@interface BBStatesListViewController ()

@end

@implementation BBStatesListViewController

#pragma mark - Defaults

- (void)dealloc
{
    QLog(@"//===============================================================================");
    background = nil;
    self.tableView  = nil;
    self.dataSource = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //set Title
    self.title = NSLocalizedString(@"List Of Breweries", nil);
    
    //add home button in navigationbar
    [self addBtnHome];
    
    //disable more & refresh
    [self.tableView setShowRefreshView:NO];
    [self.tableView setShowLoadMoreView:NO];
    
    [self.tableView setDragDelegate:nil refreshDatePermanentKey:@"TDTableview-Drag"];
    
    
    //fake data
//    [self fakeData];
    
    [self callAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - OVerWriter

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - fake data

- (void)fakeData
{
    for (int i = 0; i < 100; i ++) {
        [self.dataSource addObject:@"Vincent"];
    }
    
    [self.tableView reloadData];
}

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    NSString *url = [NSString stringWithFormat:@"%@%ld",API_STATE_LIST,self.page + 1];
    [Services callAPIWithUrl:url TypeAPI:GET withParams:nil completed:^(NSDictionary *result, NSError *error) {
        if ( error )
        {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        else
        {
            if ([Services requestError:result]) return ;
            self.page = self.page + 1;
            NSMutableArray *states = [result objectForKey:K_STATES];
            NSMutableArray *arr = [NSMutableArray new];
            for (NSDictionary *dict in states) {
                State *state = [[State alloc] initWithDictionary:dict];
                [arr addObject:state];
            }
            [self copyData:arr];
             stopProgressHUD();
        }
    }];
}

#pragma mark - Action

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"StateCell";
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    @try {
        State *obj = [self.dataSource objectAtIndex:indexPath.row];
        [cell labelWithTag:1].text = obj.name;
    }
    @catch (NSException *exception) {
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    State *obj = [self.dataSource objectAtIndex:indexPath.row];
    BBBreweriesListViewController *vc = [self getViewControllerWithNibName:NSStringFromClass([BBBreweriesListViewController class])];
    vc.title =  obj.name;
    vc.stateObj = obj;
    [self.navigationController pushViewController:vc animated:YES];
}

@end

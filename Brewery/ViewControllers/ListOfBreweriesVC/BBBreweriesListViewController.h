//
//  BBBreweriesListViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BBDetailViewController.h"
#import "TDSort.h"
#import "UITableView+DragLoad.h"
#import "State.h"

@interface BBBreweriesListViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIImageView *background;
    IBOutlet UITableView *tableView;
    
    IBOutlet UIImageView *imvBorder;
    
    NSMutableArray *data;
    NSMutableArray *dataSection;
    NSMutableArray *dataSource;
    
    NSInteger page;
}

@property (nonatomic, retain) State *stateObj;

@end

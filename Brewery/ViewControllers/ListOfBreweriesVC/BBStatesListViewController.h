//
//  BBStatesListViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BBBreweriesListViewController.h"
#import "State.h"

@interface BBStatesListViewController : BaseTableViewController
{
    IBOutlet UIImageView *background;
}

@end

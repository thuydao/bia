//
//  BBBreweriesListViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBBreweriesListViewController.h"


@interface BBBreweriesListViewController ()

@end

@implementation BBBreweriesListViewController

#pragma mark - Defaults

- (void)dealloc
{
    QLog(@"");
    background  = nil;
    tableView   = nil;
    dataSource  = nil;
    data        = nil;
    dataSection = nil;
    imvBorder   = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //init datasource
    dataSource  = [NSMutableArray new];
    data        = [NSMutableArray new];
    dataSection = [NSMutableArray new];
    
    //add filter button
    [self addFilter];
    
    //set more & refresh
    [tableView setShowRefreshView:YES];
    [tableView setShowLoadMoreView:YES];
    
    [tableView setDragDelegate:(id)self refreshDatePermanentKey:@"TDTableview-Drag"];
    
    //border tableview
    imvBorder.layer.borderColor = [UIColor lightGrayColor].CGColor;
    imvBorder.layer.borderWidth = 0.5;
    imvBorder.layer.cornerRadius = 5;
//    [imvBorder setBackgroundColor:[UIColor lightGrayColor]];
    
    tableView.backgroundColor = [UIColor clearColor];
    
    //add ads top
    [self addAds:top];
    
    //fake data
//    [self fakeData];
    
    //call APi
    [self callAPI];
    
    //page init
    page = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - fake data

- (void)fakeData
{
    
    for (NSInteger i = 0; i < 10; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Atarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        [data addObject:obj0];
    }
    
    for (NSInteger i = 0; i < 10; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Btarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        [data addObject:obj0];
    }
    
    for (NSInteger i = 0; i < 2; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Ctarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        [data addObject:obj0];
    }
    
    for (NSInteger i = 0; i < 2; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Etarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        [data addObject:obj0];
    }
    
    
    
    for (NSInteger i = 0; i < 1; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Ftarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        [data addObject:obj0];
    }
    
    NSDictionary *dict = [TDSort wordsFrom:data];
    dataSection = [dict objectForKey:@"sections"];
    dataSource = [dict objectForKey:@"sources"];
    
    [tableView reloadData];
}

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    [param setObject:[NSString stringWithFormat:@"%ld",page +1 ] forKey:@"page"];
    [param setObject:self.stateObj.identifier forKey:@"state_id"];
    
    if (![[BBUserDefault filter] isEqualToStringInsensitive:NSLocalizedString(@"All", nil)]) {
        NSString *visited = @"0";
        if ([[BBUserDefault filter] isEqualToStringInsensitive:NSLocalizedString(@"Visited", nil)]) {
            visited = @"1";
        }
        [param setObject:visited forKey:@"visited"];
    }
    [Services callAPIWithUrl:API_BREWERY_LIST TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
            [tableView stopLoadMore];
            [tableView stopRefresh];
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                [tableView stopLoadMore];
                [tableView stopRefresh];
                
                //if page == 0 ==> clear table
                if (page == 0 ) {
                    [self copyData:[NSArray new]];
                }
                
                return ;
            }
            page = page + 1;
            NSMutableArray *brewerys = [result objectForKey:K_BREWERYS];
            NSMutableArray *arr = [NSMutableArray new];
            for (NSDictionary *dict in brewerys) {
                Brewery *obj = [[Brewery alloc] initWithDictionary:dict];
                [arr addObject:obj];
            }
            [self copyData:arr];
            stopProgressHUD();
        }
    }];
}

#pragma mark - Action

- (void)changeFilter
{
    QLog(@"");
    //call api & refresh tableView
    
    //reset page
    page = 0;
    
    //call api
    [self callAPI];
}

- (void)copyData:(NSArray *)arr
{
    if (!data) data = [NSMutableArray new];
    
    //refresh data
    if (page == 0 ||page == 1) {
        [data removeAllObjects];
        [data addObjectsFromArray:arr];
        
        [tableView stopRefresh];
        //        [self.tableView finishRefresh];
    }
    
    //more data
    else
    {
        [data addObjectsFromArray:arr];
        [tableView stopLoadMore];
        //        [self.tableView finishLoadMore];
    }
    
    [self reloadData];
}

- (void)reloadData
{
    NSDictionary *dict = [TDSort wordsFrom:data];
    dataSection = [dict objectForKey:@"sections"];
    dataSource = [dict objectForKey:@"sources"];
    
    [tableView reloadData];

}


#pragma mark - Table View
//BreweriesCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataSection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *arr = [dataSource objectAtIndex:section];
    return [arr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{return 30;}

- (UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"BreweriesCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //set color for cell
    cell.backgroundColor = [UIColor clearColor];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
    //get object for cell
    NSMutableArray *arr = [dataSource objectAtIndex:indexPath.section];
    Brewery *obj = [arr objectAtIndex:indexPath.row];
    
    //fill data
    [[cell imageViewWithTag:1] setImage:[UIImage imageNamed:obj.icon]];
    [cell labelWithTag:2].text = obj.name;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return dataSection;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //init viewheader
    UIView *viewheader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 305, 30)];
    [viewheader setBackgroundColor:[UIColor clearColor]];
    
    //int view color
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 30)];
    
    //init label title + set title
    UILabel *lbl  = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 280, 21)];
    lbl.text      = [dataSection objectAtIndex:section];
    lbl.textColor = [UIColor grayColor];
    lbl.font      = [UIFont fontWithName:FONT_NAME size:17];
    
    [view addSubview:lbl];
    [view setBackgroundColor:[UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0]];
    
    [viewheader addSubview:view];
    return viewheader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *arr = [dataSource objectAtIndex:indexPath.section];
    Brewery *obj = [arr objectAtIndex:indexPath.row];
    
    BBDetailViewController *detailVC = [self getViewControllerWithNibName:NSStringFromClass([BBDetailViewController class])];
    detailVC.BreweryID = obj.identifier;
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - UITableViewDragLoadDelegate
//Called when table trigger a refresh event.
- (void)dragTableDidTriggerRefresh:(UITableView *)tableView1
{
    //    [self.tableView setShowLoadMoreView:NO];
    page = 0;
    [self callAPI];
}

//When trigger a load more event, refresh will be canceled. Then this method will be called.
//Cancel operations(Network requests, etc.) related to refresh in this method.
- (void)dragTableRefreshCanceled:(UITableView *)tableView1
{
    //    [self.tableView stopRefresh];
}

//Called when table trigger a load more event.
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView1
{
    //    [self.tableView setShowRefreshView:NO];
    [self callAPI];
}

//When trigger a refresh event, load more will be canceled. Then this method will be called.
//Cancel operations(Network requests, etc.) related to load more in this method.
- (void)dragTableLoadMoreCanceled:(UITableView *)tableView1
{
    //    [self.tableView stopLoadMore];
    
}


@end

//
//  BBMyProfileViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBMyProfileViewController.h"

@interface BBMyProfileViewController ()

@end

@implementation BBMyProfileViewController

#pragma mark - Defaults

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    QLog(@"//===============================================================================");
    background      = nil;
    tableView       = nil;
    scrollView      = nil;
    dataSouce       = nil;
    viewcontrollers = nil;
    friendTabbarController = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //set Title
    self.title = NSLocalizedString(@"My Profile", nil);
    
    //init datasource
    dataSouce       = [NSMutableArray new];
    viewcontrollers = [NSMutableArray new];
    
    
    [dataSouce addObject:@"My Friends"];
    [viewcontrollers addObject:NSStringFromClass([BBMyFriendViewController class])];
    
    [dataSouce addObject:@"My Badges"];
    [viewcontrollers addObject:NSStringFromClass([BBMyBadgeViewController class])];
    
    [dataSouce addObject:@"Notification Center"];
    [viewcontrollers addObject:NSStringFromClass([BBNotificationViewController class])];
    
    [dataSouce addObject:@"Settings"];
    [viewcontrollers addObject:NSStringFromClass([BBSettingViewController class])];
    
    [dataSouce addObject:@"Logout"];
    
    //border tableview
    tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tableView.layer.borderWidth = 0.5;
    tableView.layer.cornerRadius = 5;
    
    [self updateContentSize];
    
    
    //add Home button in NavigationBar
    [self addBtnHome];
    
    //add ads in top
    [self addAds:top];
    
    //set first indexselected
    indexSelected = -1;
    
    //get my profile
    [self callAPI];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                            (unsigned long)NULL), ^(void) {
        [self findTabbarFriend];
    });
}

- (void)updateContentSize
{
    CGRect frame = background.frame;
    frame.size.height = tableView.frame.origin.y + tableView.frame.size.height + 15;
   
    [background setFrame:frame];
    [scrollView setContentSize:background.frame.size];
    
}

- (void)reloadData:(NSDictionary *)userDict
{
    lbBadges.text      = [userDict stringForKey:@"badges"];
    lbName.text        = [userDict stringForKey:@"username"];
    lbPoints.text      = [userDict stringForKey:@"total_points"];
    lbNumVerified.text = [userDict stringForKey:@"total_verified"];
    lbNumVisited.text  = [userDict stringForKey:@"total_visited"];
    
    NSDictionary *user_type = [userDict dictionaryForKey:@"user_type"];
    
    int i = 1;
    for (NSString *key in user_type.allKeys) {
        if ([user_type boolForKey:key]) {
            UIImageView *imv = [self.view imageViewWithTag:i];
            if ([key isEqualToStringInsensitive:K_IS_FACEBOOK])
            {
                [imv setImage:[UIImage imageNamed:@"ic_facebook.png"]];
            }
            
            if ([key isEqualToStringInsensitive:K_IS_GOOGLE])
            {
                [imv setImage:[UIImage imageNamed:@"ic_google.png"]];
            }
            else
            {
                [imv setImage:[UIImage imageNamed:@"ic_twitter.png"]];
            }
            
            i= i + 1;
        }
    }
}

#pragma mark - fake data

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    
    //call API
    [Services callAPIWithUrl:API_MY_PROFILE TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                return ;
            }
            
            //TODO
            NSDictionary *userDict = [result objectForKey:@"user"];
            [self reloadData:userDict];
            
            stopProgressHUD();
        }
    }];

}

- (void)callAPILogout
{
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    
    //call API
    [Services callAPIWithUrl:API_LOGOUT TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {}];

}

#pragma mark - Action

- (void)findTabbarFriend
{
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[BBHomeViewController class]]) {
            BBHomeViewController *vch = (BBHomeViewController *)vc;
            friendTabbarController = vch.tabbarFriend;
        }
    }
}

#pragma mark - Table View
//ProfileCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSouce count];
}

- (UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"ProfileCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = [dataSouce objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FONT_NAME size:17];
    
    //check seleced
    if (indexSelected == indexPath.row) {
        [[cell imageViewWithTag:1] setBackgroundColor:TEXT_COLOR];
        cell.textLabel.textColor = TEXT_COLOR;
    }
    else
    {
        [[cell imageViewWithTag:1] setBackgroundColor:[UIColor clearColor]];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //update interface
    NSInteger old = indexSelected;
    indexSelected = indexPath.row;
    
    NSMutableArray *arr = [NSMutableArray new];
    [arr addObject:[NSIndexPath indexPathForRow:indexSelected inSection:0]];
    [arr addObject:[NSIndexPath indexPathForRow:old inSection:0]];
    
    if (old != indexSelected) {
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
    
    if (indexPath.row == 0) {
        if (!friendTabbarController) {
            [self findTabbarFriend];
        }
        friendTabbarController.selectedIndex = 0;;
        [BaseTabbarControler setSelected:friendTabbarController indexSelected:0];
        
        //present tabbar
        [self presentViewController:friendTabbarController animated:YES completion:^{
            
        }];
        
        return;
    }
    
    if (indexPath.row == [dataSouce count] -1) {
        QLog(@"Logout");
        
        [self callAPILogout];
        
        NSMutableArray *arr = [NSMutableArray new];
        [arr addObject:[self getViewControllerWithNibName:NSStringFromClass([BBLoginViewController class])]];
        self.navigationController.viewControllers = arr;
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    
    [self pushViewControllerWithNibName:[viewcontrollers objectAtIndex:indexPath.row]];
}


@end

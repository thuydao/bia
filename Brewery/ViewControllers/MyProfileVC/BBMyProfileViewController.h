//
//  BBMyProfileViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BBMyFriendViewController.h"
#import "BBMyBadgeViewController.h"
#import "BBNotificationViewController.h"
#import "BBSettingViewController.h"
#import "BaseTabbarControler.h"
#import "BBHomeViewController.h"
#import "BBLoginViewController.h"

@interface BBMyProfileViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIImageView  *background;
    IBOutlet UITableView  *tableView;
    IBOutlet UILabel      *lbName;
    IBOutlet UILabel      *lbPoints;
    IBOutlet UILabel      *lbBadges;
    IBOutlet UILabel      *lbNumVerified;
    IBOutlet UILabel      *lbNumVisited;
    
    NSMutableArray *dataSouce;
    NSMutableArray *viewcontrollers;
    
    NSInteger indexSelected;
    UITabBarController *friendTabbarController;
}



@end

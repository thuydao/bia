//
//  BBMyBadgeViewController.h
//  Brewery
//
//  Created by Thuy Dao on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BBDetailViewController.h"

@interface BBMyBadgeViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIWebView *wvTitle;
    IBOutlet UITableView *tableView;
    
    
    NSMutableArray *dataSource;
}
@end

//
//  BBMyBadgeViewController.m
//  Brewery
//
//  Created by Thuy Dao on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBMyBadgeViewController.h"

@interface BBMyBadgeViewController ()

@end

@implementation BBMyBadgeViewController

- (void)dealloc
{
    wvTitle     = nil;
    tableView   = nil;
    dataSource  = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //dataSource init
    dataSource = [NSMutableArray new];
    
    //set Title
    self.title = NSLocalizedString(@"My Badges", nil);
    
    //add btn Home in navigationbar
    [self addBtnHome];
    
    //add ads top
    [self addAds:top];
    
    //border tableview
    tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tableView.layer.borderWidth = 0.5;
    tableView.layer.cornerRadius = 5;
    
    //fakedata
    //    [self fakeData];
    [self callAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)fakeData
{
    //get string
    NSString *path = [[NSBundle mainBundle] pathForResource:@"mybadges" ofType:@"html"];
    NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    //put data
    html = [NSString stringWithFormat:html,@"2001",@"204535",@"545",nil];
    
    //reload data
    [wvTitle loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
    wvTitle.scrollView.scrollEnabled = NO;
    
    //fake breweries
    for (NSInteger i = 0; i < 100; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Ftarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        [dataSource addObject:obj0];
    }
    
    //reload table
    [tableView reloadData];
}

#pragma mark - API

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    
    //call API
    [Services callAPIWithUrl:API_MY_BADGES TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                return ;
            }
            
            //get manual object, because api is shit
            NSArray *business = [result arrayForKey:@"businesss"];
            for (NSDictionary *dict in business) {
                Brewery *obj   = [Brewery new];
                obj.name       = [dict stringForKey:@"name"];
                obj.verified   = [dict boolForKey:@"verified"];
                obj.visited    = [dict boolForKey:@"visited"];
                obj.identifier = [dict stringForKey:@"id"];
                [obj genIcon];
                
                [dataSource addObject:obj];
            }
            
            //reload table
            [tableView reloadData];
            
            //get string
            NSString *path = [[NSBundle mainBundle] pathForResource:@"mybadges" ofType:@"html"];
            NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
            
            //put data
            html = [NSString stringWithFormat:html, [result stringForKey:@"total_verified"], [result stringForKey:@"total_visited"], [result stringForKey:@"total_points"] , nil];
            
            //reload data
            [wvTitle loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
            wvTitle.scrollView.scrollEnabled = NO;
            
            
            stopProgressHUD();
        }
    }];

}

#pragma mark - Table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"badgescell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Brewery *obj = [dataSource objectAtIndex:indexPath.row];
    
    [cell labelWithTag:2].text = obj.name;
    [[cell imageViewWithTag:1] setImage:[UIImage imageNamed:obj.icon]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Brewery *obj = [dataSource objectAtIndex:indexPath.row];
    
    BBDetailViewController *detailVC = [self getViewControllerWithNibName:NSStringFromClass([BBDetailViewController class])];
    detailVC.BreweryID = obj.identifier;
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end

//
//  BBInfoViewController.h
//  Brewery
//
//  Created by Thuy Dao on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BBStaticWebViewController.h"
#import "BBContactUsViewController.h"
#import "BBMyProfileViewController.h"

@interface BBInfoViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIImageView *background;
    IBOutlet UITableView *tableView;
    
    NSMutableArray *dataSouce;
    NSMutableArray *arrVCName;
    NSInteger indexSelected;
}

@end

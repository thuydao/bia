//
//  BBInfoViewController.m
//  Brewery
//
//  Created by Thuy Dao on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBInfoViewController.h"

@interface BBInfoViewController ()

@end

@implementation BBInfoViewController

- (void)dealloc
{
    background = nil;
    tableView = nil;
    arrVCName = nil;
    dataSouce = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set Title
    self.title = NSLocalizedString(@"Brewery Explorer Info", nil);
    
    //init datasource
    dataSouce = [NSMutableArray new];
    arrVCName = [NSMutableArray new];
    
    [dataSouce addObject:@"About"];
    [arrVCName addObject:NSStringFromClass([BBStaticWebViewController class])];
    
    [dataSouce addObject:@"How-to Guide"];
    [arrVCName addObject:NSStringFromClass([BBStaticWebViewController class])];
    
    [dataSouce addObject:@"My Profile"];
    [arrVCName addObject:NSStringFromClass([BBMyProfileViewController class])];
    
    [dataSouce addObject:@"Teams of Service"];
    [arrVCName addObject:NSStringFromClass([BBStaticWebViewController class])];
    
    [dataSouce addObject:@"Privacy Policy"];
    [arrVCName addObject:NSStringFromClass([BBStaticWebViewController class])];
    
    [dataSouce addObject:@"Contact Us"];
    [arrVCName addObject:NSStringFromClass([BBContactUsViewController class])];
    
    //border tableview
    tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tableView.layer.borderWidth = 0.5;
    tableView.layer.cornerRadius = 5;
    
    //add home button in navigationbar
    [self addBtnHome];
    
    //add ads in top
    [self addAds:top];
    //set first indexselected
    indexSelected = -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //change frame tableview
    CGRect frame = tableView.frame;
    frame.size.height = 264;
    [tableView setFrame:frame];
    tableView.scrollEnabled = NO;
}

#pragma mark - Table View
//StateCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSouce count];
}

- (UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"InfoCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = [dataSouce objectAtIndex:indexPath.row];
    
    if (indexSelected == indexPath.row) {
        [[cell imageViewWithTag:1] setBackgroundColor:TEXT_COLOR];
        cell.textLabel.textColor = TEXT_COLOR;
    }
    else
    {
        [[cell imageViewWithTag:1] setBackgroundColor:[UIColor clearColor]];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //update selected
    [self reloadSelected:indexPath.row];
    
    if (indexPath.row == 0) {
         [BBUserDefault setSource:@"About"];
    }
    if (indexPath.row == 3) {
        [BBUserDefault setSource:@"TermOfServices"];
    }
    if (indexPath.row == 4) {
        [BBUserDefault setSource:@"Privacy"];
    }
    
    [self pushViewControllerWithNibName:[arrVCName objectAtIndex:indexPath.row]];
}

- (void)reloadSelected:(NSInteger)row
{
    //update interface
    NSInteger old = indexSelected;
    indexSelected = row;
    
    NSMutableArray *arr = [NSMutableArray new];
    [arr addObject:[NSIndexPath indexPathForRow:indexSelected inSection:0]];
    [arr addObject:[NSIndexPath indexPathForRow:old inSection:0]];
    
    if (old != indexSelected) {
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
}

@end

//
//  BBLeaderBoardViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBLeaderBoardViewController.h"

@interface BBLeaderBoardViewController ()

@end

@implementation BBLeaderBoardViewController

#pragma mark - Defaults

- (void)dealloc
{
    background = nil;
    tableView  = nil;
    dataSource = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //dataSource init
    dataSource = [NSMutableArray new];
    
    //set Title
    self.title = NSLocalizedString(@"LEADERBOARD", nil);
    
    //border tableview
    tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tableView.layer.borderWidth = 0.5;
    tableView.layer.cornerRadius = 5;
    
    //add top ads
    [self addAds:top];
    
    //self addTimeFilter
    [self addTimeFilter];
    
    //fake data
//    [self fakeData];
    
    //self callAPI
    [self callAPI];
    
    //set first indexselected
    indexSelected = -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - OVerWriter

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - fake data

- (void)fakeData
{
    for (NSInteger i = 0; i < 100; i++) {
        User *u = [[User alloc] init];
        u.name = @"vincent";
        u.score = @"100";
        [dataSource addObject:u];
    }
    
    //reload data
    [tableView reloadData];
}

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    NSInteger this_week = 0;
    NSInteger this_month = 0;
    if ([[BBUserDefault timeFilter] isEqualToStringInsensitive:@"This Week"])
    {
        this_week = 1;
    }
    
    else if ([[BBUserDefault timeFilter] isEqualToStringInsensitive:@"This Month"])
    {
        this_month = 1;
    }
    
    [param setObject:[NSString stringFromInteger:this_week] forKey:@"this_week"];
    [param setObject:[NSString stringFromInteger:this_month] forKey:@"this_month"];
    
    //call API
    [Services callAPIWithUrl:API_LEADERBOAD TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                return ;
            }
            
            //TODO
            NSArray *users = [result arrayForKey:@"users"];
            for (NSDictionary *dict in users) {
                User *u = [[User alloc] initWithDictionary:dict];
                [dataSource addObject:u];
            }
            
            [tableView reloadData];
            
            stopProgressHUD();
        }
    }];
}

#pragma mark - Action

#pragma mark - overwrite
- (void)changeTimeFilter
{
    [self callAPI];
}

#pragma mark - Table View
//StateCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"LeaderBoardCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //get object
    User *obj = [dataSource objectAtIndex:indexPath.row];
    
    if (indexSelected != indexPath.row) {
        [[cell labelWithTag:1]setFont: [UIFont fontWithName:FONT_NAME size:15]];
        [[cell labelWithTag:2]setFont: [UIFont fontWithName:FONT_NAME size:13]];
        [[cell imageViewWithTag:3] setBackgroundColor:[UIColor clearColor]];
    }
    else
    {
        [[cell labelWithTag:1] setFont:[UIFont fontWithName:FONT_NAME_BOLD size:15]];
        [[cell labelWithTag:2] setFont:[UIFont fontWithName:FONT_NAME_BOLD size:13]];
        [[cell imageViewWithTag:3] setBackgroundColor:TEXT_COLOR];
    }
    
    
    
    [cell labelWithTag:1].text = obj.name;
    [cell labelWithTag:2].text = obj.score;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger old = indexSelected;
    indexSelected = indexPath.row;
    
    NSMutableArray *arr = [NSMutableArray new];
    [arr addObject:[NSIndexPath indexPathForRow:indexSelected inSection:0]];
    [arr addObject:[NSIndexPath indexPathForRow:old inSection:0]];
    
    if (old != indexSelected) {
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
    
    //get object
    User *obj = [dataSource objectAtIndex:indexPath.row];
    if (![obj.identifier isEqualToStringInsensitive:[User identifier]]) {
        BBUserDetailViewController *vc = [self getViewControllerWithNibName:NSStringFromClass([BBUserDetailViewController class])];
        vc.object = obj;
        vc.title = obj.name;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
//    else
//    {
//        //go to my profile
//        [self pushViewControllerWithNibName:NSStringFromClass([BBMyProfileViewController class])];
//    }
}



@end

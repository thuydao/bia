//
//  BBLeaderBoardViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BBUserDetailViewController.h"
#import "BBMyProfileViewController.h"

typedef enum : NSUInteger {
    TypeGlobal = 0,
    TYpeFriend = 1,
} typeLeaderBoard;

@interface BBLeaderBoardViewController : BaseViewController  <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIImageView *background;
    IBOutlet UITableView *tableView;
    
    
    NSMutableArray *dataSource;
    NSInteger indexSelected;
}

@property (nonatomic, assign) typeLeaderBoard type;

@end

//
//  BBDetailViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBDetailViewController.h"

@interface BBDetailViewController ()

@end

@implementation BBDetailViewController

#pragma mark - Defaults

- (void)dealloc
{
    background  = nil;
    scrollView  = nil;
    imvIcon     = nil;
    lbName      = nil;
    lbAddress   = nil;
    lbStartTime1 = nil;
    lbStartTime2   = nil;
    lbWebLink   = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set Title
    self.title = NSLocalizedString(@"Detail", nil);
    
    //Update scrollview
    [scrollView setContentSize:background.frame.size];
    
    //add Home button in navigationbar
    [self addBtnHome];
    
    //add top ads
    [self addAds:top];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //reload data
    [self reloadData];
}

- (void)setLinkToAddress:(NSString *)adr
{
    lbWebLink.text = adr;
    NSRange r = NSMakeRange(0, adr.length);
    [lbWebLink addLinkToURL:[NSURL URLWithString:@"action://show-help"] withRange:r];
    lbWebLink.delegate = (id)self;
}


- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([[url scheme] hasPrefix:@"action"]) {
        if ([[url host] hasPrefix:@"show-help"]) {
            
            //open safary
            NSURL *url = [NSURL URLWithString:label.text];
            
            if (![[UIApplication sharedApplication] openURL:url]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
            
        } else if ([[url host] hasPrefix:@"show-settings"]) {
            /* load settings screen */
        }
    } else {
        /* deal with http links here */
    }
}

#pragma mark - fake data

#pragma mark - CallAPI
//===================================== callAPI ==========================================
- (void)callAPI
{
    if (!self.BreweryID)
    {
        TDAlertWithParam(@"Error", @"Brewery don't exit!");
        return;
    }
    
    //object not exit
    if (!self.BreweryObj) {
        //call api
        
        //param init
        NSMutableDictionary *param = [NSMutableDictionary new];
        [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
        [param setObject:self.BreweryID forKey:@"brewery_id"];
        
        [Services callAPIWithUrl:API_BREWERY_DETAIL TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
            if (error) {
                TDAlertWithParam(@"Loi", @"Data load fails");
                
                stopProgressHUD();
            }
            
            else
            {
                if ([Services requestError:result])
                {
                    stopProgressHUD();
                    return ;
                }
                NSDictionary *dict = [result objectForKey:K_BREWERY];
                self.BreweryObj = [[Brewery alloc] initWithDictionary:dict];
                
                [self reloadData];
                stopProgressHUD();
            }
        }];
        

    }
}

//===================================== callAPIVisit ==========================================
- (void)callAPIVisit
{
    
}

//===================================== callAPIVerify ==========================================
- (void)callAPIVerify
{
    startProgressHUD(@"Loading");
    
    [[TDLocationManager shared] getCLLocationCompleteBlock:^(CLLocation *result, NSError *error) {
        
        //param init
        NSMutableDictionary *param = [NSMutableDictionary new];
        [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
        [param setObject:self.BreweryID forKey:@"brewery_id"];
        [param setObject:[NSString stringFromFloat:result.coordinate.latitude] forKey:@"lat"];
        [param setObject:[NSString stringFromFloat:result.coordinate.longitude] forKey:@"lng"];
        
        //call API
        [Services callAPIWithUrl:API_VERIFY TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
            if (error) {
                TDAlertWithParam(@"Loi", @"Data load fails");
                
                stopProgressHUD();
            }
            
            else
            {
                if ([Services requestError:result])
                {
                    stopProgressHUD();
                    return ;
                }
                
                NSDictionary *dict = [result objectForKey:K_BREWERY];
                self.BreweryObj = [[Brewery alloc] initWithDictionary:dict];
                
                [self reloadData];
                stopProgressHUD();
            }
        }];
        
        
    }];
}

#pragma mark - Action
//===================================== reloadData ==========================================
- (void)reloadData
{
    //check input
    if (!self.BreweryObj) {
        [self callAPI];
    }
    
    //set icon
    [imvIcon setImage:[UIImage imageNamed:self.BreweryObj.icon]];
    
    //set name
    [lbName setText:self.BreweryObj.name];
    
    //set address
    [lbAddress setText:self.BreweryObj.address];
    
    //set starttime
    [lbStartTime1 setText:self.BreweryObj.startTime1];
    
    //set endtime
    [lbStartTime2 setText:self.BreweryObj.startTime2];
    
    //set link web
    [self setLinkToAddress:self.BreweryObj.linkweb];
    
    //set buttonVerify state
   
    if (self.BreweryObj.visited) {
        btnVerify.enabled = YES;
    }
    else
    {
        btnVerify.enabled = NO;
        [imvIcon handleTapBlock:^(UITapGestureRecognizer *sender) {
            //call API Visted
            [self callAPIVisit];
        }];
    }
}

- (IBAction)clickVerify:(id)sender
{
    QLog(@"");
    [self callAPIVerify];
}



@end

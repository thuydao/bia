//
//  BBDetailViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "TDLocationManager.h"
#import "TTTAttributedLabel.h"

@interface BBDetailViewController : BaseViewController
{
    IBOutlet UIImageView    *background;
    IBOutlet UIScrollView   *scrollView;
    
    IBOutlet UIImageView    *imvIcon;
    IBOutlet UILabel        *lbName;
    IBOutlet UILabel        *lbAddress;
    IBOutlet UILabel        *lbStartTime1;
    IBOutlet UILabel        *lbStartTime2;
    IBOutlet TTTAttributedLabel        *lbWebLink;
    
    IBOutlet UIButton       *btnVerify;
}

@property (nonatomic, retain) Brewery *BreweryObj;
@property (nonatomic, retain) NSString *BreweryID;

- (IBAction)clickVerify:(id)sender;

@end

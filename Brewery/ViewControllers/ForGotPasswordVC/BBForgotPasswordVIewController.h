//
//  BBForgotPasswordVIewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"

@interface BBForgotPasswordVIewController : BaseViewController
{
    IBOutlet UIImageView    *background;
    IBOutlet UIScrollView   *scrollView;
    IBOutlet UITextField    *tfEmail;
}

- (IBAction)clickSend:(id)sender;

@end

//
//  BBForgotPasswordVIewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBForgotPasswordVIewController.h"

@interface BBForgotPasswordVIewController ()

@end

@implementation BBForgotPasswordVIewController

#pragma mark - Defaults

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Forgot Password", @"Forgot Password");
    
    [scrollView setContentSize:background.frame.size];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - fake data

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:tfEmail.text forKey:@"email"];
    
    //call API
    [Services callAPIWithUrl:API_FORGOT_PASSWORD TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                return ;
            }
            
            //TODO
            TDAlertWithParam(@"", [result objectForKey:@"message"]);
            
            stopProgressHUD();
        }
    }];

}

#pragma mark - Action

//===================================== clickSend ==========================================

- (IBAction)clickSend:(id)sender
{
    QLog(@"");
    if ([tfEmail.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_EMAIL);
        return;
    }
    else
    {
        [self callAPI];
    }
}




@end

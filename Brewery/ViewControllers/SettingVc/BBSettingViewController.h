//
//  BBSettingViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BBChangePasswordViewController.h"
#import "BBChangeUserNameViewController.h"

@interface BBSettingViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tableView;
    IBOutlet UIImageView *background;
    
    NSMutableArray *dataSource;
    NSInteger indexSelected;
}

@end

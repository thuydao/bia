//
//  BBSettingViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBSettingViewController.h"

@interface BBSettingViewController ()

@end

@implementation BBSettingViewController

#pragma mark - Defaults

- (void)dealloc
{
    background      = nil;
    tableView       = nil;
    dataSource      = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //datasource init
    dataSource = [NSMutableArray new];
    
    [dataSource addObject:@"Change Username"];
    [dataSource addObject:@"Change Password"];
    [dataSource addObject:@"Participate in Public Leaderborad"];
    [dataSource addObject:@"Push Setting"];
    [dataSource addObject:@"Friends obtain “Verified” badge"];
    [dataSource addObject:@"Friends obtain “Visited” badge"];
    
    //set Title
    self.title = NSLocalizedString(@"Settings", nil);
    
    //border tableview
    tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tableView.layer.borderWidth = 0.5;
    tableView.layer.cornerRadius = 5;
    
    //disable scrolltable
    tableView.scrollEnabled = NO;
    
    //add ads in top
    [self addAds:top];
    
    //set the frist indexSelected
    indexSelected = -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - fake data

#pragma mark - CallAPI

#pragma mark - Action


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 1) {
        return 44;
    }
    if (indexPath.row == 3) {
        return 30;
    }
    return 55;
}

- (UITableViewCell*)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"cell1";
    if (indexPath.row == 0 || indexPath.row == 1) {
        cellIdentifier = @"cell0";
    }
    if (indexPath.row == 3) {
        cellIdentifier = @"cell2";
    }
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell labelWithTag:2].text = [dataSource objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexSelected == indexPath.row) {
        [[cell imageViewWithTag:1] setBackgroundColor:TEXT_COLOR];
        [cell labelWithTag:2].textColor = TEXT_COLOR;
    }
    else
    {
        [[cell imageViewWithTag:1] setBackgroundColor:[UIColor clearColor]];
        [cell labelWithTag:2].textColor = [UIColor blackColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 0 && indexPath.row != 1) {
        return;
    }
    
    [self reloadSelected:indexPath.row];
    
    if (indexPath.row == 0) {
        [self pushViewControllerWithNibName:NSStringFromClass([BBChangeUserNameViewController class])];
    }
    if (indexPath.row == 1)
    {
        [self pushViewControllerWithNibName:NSStringFromClass([BBChangePasswordViewController class])];
    }
}

- (void)reloadSelected:(NSInteger)row
{
    //update interface
    NSInteger old = indexSelected;
    indexSelected = row;
    
    NSMutableArray *arr = [NSMutableArray new];
    [arr addObject:[NSIndexPath indexPathForRow:indexSelected inSection:0]];
    [arr addObject:[NSIndexPath indexPathForRow:old inSection:0]];
    
    if (old != indexSelected) {
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
}


@end

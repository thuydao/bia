#import "BaseViewController.h"
#import "BBRegisterViewController.h"
#import "BBForgotPasswordVIewController.h"
#import "BBHomeViewController.h"
#import "UIViewController+FB.h"
#import "UIViewController+Google.h"
#import "UIViewController+Twitter.h"
#import "UIUnderlinedButton.h"

@interface BBLoginViewController : BaseViewController
{
    IBOutlet UIImageView    *background;
    IBOutlet UIScrollView   *scrollView;
    IBOutlet UIButton       *btnFacebook;
    IBOutlet UIButton       *btnGoogle;
    
    IBOutlet UITextField    *tfName;
    IBOutlet UITextField    *tfPassword;
    
    IBOutlet UIUnderlinedButton *btnForgot;
}




- (IBAction)clickRegister:(id)sender;
- (IBAction)clickForGotPassword:(id)sender;
- (IBAction)clickLogin:(id)sender;
- (IBAction)clickLoginTwitter:(id)sender;
- (IBAction)clickLoginGoogle:(id)sender;

@end

#import "BBLoginViewController.h"

@implementation BBLoginViewController

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Sign in", @"Login");
    
    [scrollView setContentSize:background.frame.size];
    
    //add LoginFacebook
    __block BBLoginViewController *welf = self;
    
    [self addLoginFBButtonWithFrame:btnFacebook.frame FBBlock:^(LogType type, FBLoginView *loginView, id<FBGraphUser> user, NSError *err) {
        if (type == LoggedIn) {
            
            NSLog(@"=========================================== login");
            [welf callAPILoginFacebook];
            
        }
        
        if (type == LoggedOut) {
            
            NSLog(@"=========================================== logout");
            
        }
        
        if (err) {
            
            NSLog(@"=========================================== error");
            
            NSLog(@"FBLoginView encountered an error=%@", err);
        }
        
        if (user) {
            
            NSLog(@"=========================================== get info %@",user.first_name);
            
            NSLog(@"token %@",[welf FBToken]);
            
        }
    }];
    
    
    
    //add login g+ button
    //    [self addButtonGGLogin:btnGoogle.frame];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fakeData];
    
    //hide backbutton
    self.navigationItem.leftBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - fakeData
- (void)fakeData
{
    tfName.text = @"brannoncloud@live.com";
    tfPassword.text = @"123456";
}

#pragma mark - IBACTION

//===================================== clickRegister ==========================================

- (IBAction)clickRegister:(id)sender
{
    QLog(@"");
    [self pushViewControllerWithNibName:NSStringFromClass([BBRegisterViewController class])];
}

//===================================== clickForGotPassword ==========================================

- (IBAction)clickForGotPassword:(id)sender
{
    QLog(@"");
    [self pushViewControllerWithNibName:NSStringFromClass([BBForgotPasswordVIewController class])];
}

//===================================== clickLogin ==========================================

- (IBAction)clickLogin:(id)sender
{
    QLog(@"");
    [self callAPILoginEmail];
}

//===================================== clickLoginTwitter ==========================================

- (IBAction)clickLoginTwitter:(id)sender
{
    QLog(@"");
    [self signInTwitter];
}

//===================================== clickLoginGoogle ==========================================

- (IBAction)clickLoginGoogle:(id)sender
{
    QLog(@"");
    __block BBLoginViewController *welf = self;
    [self signInGoogle:^(GTMOAuth2Authentication *auth, NSError *err) {
        if (err) {
            QLog(@"err: %@",err);
            return ;
        }
        
        NSString *token = [NSString stringWithFormat:@"%@",auth];
        
        [welf callAPILoginGoogle:token];
    }];
}


#pragma mark - Action

- (BOOL)validate
{
    if ([tfName.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_EMAIL);
        return NO;
    }
    
    if ([tfPassword.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_PASSWORD);
        return NO;
    }
    
    return YES;
}

#pragma mark - callAPI
//===================================== callAPILoginEmail ==========================================
- (void)callAPILoginEmail
{
    if (![self validate]) {
        return;
    }
    
    [SVProgressHUD showErrorWithStatus:@"Login..."];
    //param init
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:tfName.text forKey:@"email"];
    [params setObject:tfPassword.text forKey:@"password"];
    
    [Services callAPIWithUrl:API_LOGIN_EMAIL TypeAPI:POST withParams:params completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            QLog(@"error : %@",error);
            [SVProgressHUD dismiss];
            return ;
            
        }
        
        if ([Services requestError:result]) return;
        
        User *user = [[User alloc] initWithDictionary:result];
        //chage user to current user
        [user setMySelf];
        [SVProgressHUD dismiss];
        [self pushViewControllerWithNibName:NSStringFromClass([BBHomeViewController class])];
    }];
}

//===================================== callAPILoginFacebook ==========================================
- (void)callAPILoginFacebook
{
    //param init
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[self FBToken] forKey:@"facebook_access_token"];
    
    [Services callAPIWithUrl:API_LOGIN_FB TypeAPI:POST withParams:params completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            QLog(@"err: %@",error);
            return ;
        }
        
        if ([Services requestError:result]) return;
        
        [self pushViewControllerWithNibName:NSStringFromClass([BBHomeViewController class])];
    }];
    
}

//===================================== callAPILoginGoogle ==========================================
- (void)callAPILoginGoogle:(NSString *)auToken
{
    //param init
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:auToken forKey:@"google_token"];
    
    [Services callAPIWithUrl:API_LOGIN_GOOGLE TypeAPI:POST withParams:params completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            QLog(@"err: %@",error);
            return ;
        }
        
        if ([Services requestError:result]) return;
        
        [self pushViewControllerWithNibName:NSStringFromClass([BBHomeViewController class])];
    }];
    
}

@end

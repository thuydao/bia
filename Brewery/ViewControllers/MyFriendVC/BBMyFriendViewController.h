//
//  BBMyFriendViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseTableViewController.h"

typedef enum : NSUInteger {
    type_myfriend = 0,
    type_findfriend = 1,
} typeView;

@interface BBMyFriendViewController : BaseTableViewController <UISearchBarDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIImageView *background;
    IBOutlet UISearchBar *sbSearchBar;
}

@property (nonatomic, assign) typeView type;

@end

//
//  BBMyFriendViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBMyFriendViewController.h"
#import "BBUserDetailViewController.h"
#import "UITableView+DragLoad.h"

@interface BBMyFriendViewController ()

@end

@implementation BBMyFriendViewController

#pragma mark - Defaults

- (void)dealloc
{
    QLog(@"//===============================================================================");
    background      = nil;
    self.tableView  = nil;
    scrollView      = nil;
    self.dataSource = nil;
    sbSearchBar     = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set Title
    
    NSString *title = NSLocalizedString(@"MY FRIENDS", nil);
    if (self.type == type_findfriend) title = NSLocalizedString(@"Find FRIENDS", nil);
    
    self.title = title;
    
    //add Home Button in navigationbar
    [self addBtnHome];
    
    
    //border tableview
    self.tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.tableView.layer.borderWidth = 0.5;
    self.tableView.layer.cornerRadius = 5;
    
    //add ads in top
    [self addAds:top];
    
    // fake data
//    [self fakeData];
    
    //request
    [self callAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - OverWrite
//overwrite back method
- (void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - fake data

- (void)fakeData
{
    for (NSInteger i = 0; i < 100; i++) {
        User *u = [[User alloc] init];
        u.name = @"vincent";
        u.score = @"100";
        u.icons = [[NSMutableArray alloc] initWithObjects:@"ic_twitter.png", @"ic_google.png", @"ic_facebook.png", nil];
        u.isFriend = NO;
        u.numverified = @"24";
        u.numNoVerified = @"34";
        u.point = @"1546";
        u.badges = @"500";
        
        if (i%2) u.isFriend = YES;
        
        [self.dataSource addObject:u];
    }
    
    [self.tableView reloadData];
}

#pragma mark - CallAPI
- (void)callAPI
{
    //create param
    NSMutableDictionary *dictParams = [NSMutableDictionary new];
    [dictParams setValue:[User auth_token] forKey:@"auth_token"];
    [dictParams setValue:[NSString stringFromInteger:self.page + 1] forKey:@"page"];
    
    /*
     
     ***
    //searchText == nil:
     /nếu đang view FindUser thì clear table và không search
     /nếu đang là Myfriend thì không send param name => Default hiển thị all friend
     
     ***
     //searchText != nil
     /thì send param name với value = searchText
    */
    
    NSString *keyword = sbSearchBar.text;
    if ([keyword isEqualToString:@""] ) {
        if (self.type != type_myfriend) {
            [self.dataSource removeAllObjects];
            [self.tableView reloadData];
            return;
        }
    }
    else
    {
         [dictParams setValue:keyword forKey:@"name"];
    }
   
    
    
    NSString *api = API_FRIEND_LOAD;
    if (self.type != type_myfriend) api = API_FRIEND_FIND;
    
    startProgressHUD(@"Loading");
    
    [Services callAPIWithUrl:api TypeAPI:POST withParams:dictParams completed:^(NSDictionary *result, NSError *error) {
        
        if ( error )
        {
            [self.tableView stopLoadMore];
            [self.tableView stopRefresh];
            TDAlertWithParam(@"Loi", @"Data load fails");
            stopProgressHUD();
        }
        else
        {
            if ([Services requestError:result]) {
                [self.tableView stopLoadMore];
                [self.tableView stopRefresh];
            }
            else
            {
                //count page
                self.page = self.page + 1;
                
                //getArray data
                NSArray *arrFriends = [result arrayForKey:K_FRIENDS];
                NSMutableArray *arr = [NSMutableArray new];
                
                for ( NSDictionary *dictData in arrFriends )
                {
                    User *object = [[User alloc] initWithDictionary:dictData];
                    
                    [arr addObject:object];
                }
                
                
                [self copyData:arr];
            }
             stopProgressHUD();
        }
    }];
}



- (void)callAPIAddFriend:(NSMutableDictionary *)dictPamras
{
    NSString *api = API_FRIEND_ADD;
    
    startProgressHUD(@"Adding");
    
    [Services callAPIWithUrl:api TypeAPI:POST withParams:dictPamras completed:^(NSDictionary *result, NSError *error) {
        
        if ( error )
        {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result]) return;
            else
            {
                [self addFriendSucced:[dictPamras objectForKey:@"friend_id"]];
                stopProgressHUD();
            }
            
        }
    }];
}

- (void)addFriendSucced:(id)friendID
{
    for (NSInteger i = 0; i< self.dataSource.count ; i ++) {
        User *u = [self.dataSource objectAtIndex:i];
        if ([u.identifier isEqualToString:friendID]) {
            u.isFriend = 2;
            [self.dataSource replaceObjectAtIndex:i withObject:u];
            break;
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Action

#pragma mark - Table View
//FriendCell

- (UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"";
    if (self.type == type_myfriend) {
        cellIdentifier = @"FriendCell";
    }
    else
    {
        cellIdentifier = @"findFriendCell";
    }
    
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    User *obj = [self.dataSource objectAtIndex:indexPath.row];
    [cell labelWithTag:1].text = obj.name;
    
    // My friend
    if (self.type == type_myfriend) {
        
        //seticon
        for (NSInteger i = 0; i < 3; i++) {
            //get UIImageview
            UIImageView *imv = [cell imageViewWithTag:i+2];
            //set background clearcolor
            [imv setBackgroundColor:[UIColor clearColor]];
            
            //check & setimage to imageview
            if (obj.icons.count > i)
                [imv setImage:[UIImage imageNamed:[obj.icons objectAtIndex:i]]];
            else
            {
                [imv setImage:nil];
            }
            
        }
    }
    
    // Find friend
    else
    {
        //check isfriend show icon
        //isfriend = 0: chưa là bạn
        // isfriend = 1: đã là bạn
        // isfriend = 2: đã gửi yêu cầu kết bạn, chờ đồng ý
        
        NSString *icon = @"ic_addFriend.png";
        if (obj.isFriend == 1) icon = @"ic_friended.png";
        if (obj.isFriend == 2) icon = @"";
        
        //set icon
        UIImage *img = [UIImage imageNamed:icon];
        [[cell imageViewWithTag:2] setImage:img];
        [cell imageViewWithTag:2].contentMode = UIViewContentModeCenter;
        
        //event tap 
        [[cell imageViewWithTag:2] handleTapBlock:^(UITapGestureRecognizer *sender) {
            QLog(@"");
            //chưa là friend thì call api request friend
            if (obj.isFriend == 0) {
                NSMutableDictionary *dictParams = [NSMutableDictionary new];
                [dictParams setValue:[User auth_token] forKey:K_AUTH_TOKEN];
                [dictParams setValue:obj.uID forKey:@"friend_id"];
                
                [self callAPIAddFriend:dictParams];
            }
            
        }];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //hide keyboard
    [sbSearchBar resignFirstResponder];
    
    //get user object
    User *obj = [self.dataSource objectAtIndex:indexPath.row];
    
    
    //goto View User Detail
    BBUserDetailViewController *vc = [self getViewControllerWithNibName:NSStringFromClass([BBUserDetailViewController class])];
    vc.object = obj;
    vc.title = obj.name;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    //page reset
    self.page = 0;
    
    //request api
    [self callAPI];
    QLog(@"");
}



- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    QLog(@"");
    
    NSString *key = searchBar.text;
    
    if ([key isEqualToString:@""]) {
        
        //page reset
        self.page = 0;
        
        //request api
        [self callAPI];
    }
}

@end

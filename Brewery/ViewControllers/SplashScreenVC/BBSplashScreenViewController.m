#import "BBSplashScreenViewController.h"

@implementation BBSplashScreenViewController

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self runProgress];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)runProgress
{
    progress.progress = 0;
    value = 0;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
}


- (void)updateProgress
{
    progress.progress += 0.002f;
    if (progress.progress < 1) {
        [progress setProgress:progress.progress animated:YES];
    }
    else
    {
        [timer invalidate];
        //goto login
        
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"BBLoginNavigationController"] animated:YES completion:^{
        }];
    }
}

@end

//
//  BBSyncViewController.m
//  Brewery
//
//  Created by Bunlv on 8/21/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBSyncViewController.h"

@interface BBSyncViewController ()

@end

@implementation BBSyncViewController

#pragma mark - View default
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = @"Sync";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - SVG
#pragma mark + Set
- (void)setViewInfo
{
    // Check call super
    [super setViewInfo];
}

#pragma mark + Get
- (void)getDataByLoadFromServer
{
    // Check call super
    [super getDataByLoadFromServer];
}

#pragma mark + Fake

#pragma mark + Add

#pragma mark + Remove

#pragma mark + Control Item
- (void)controlItem
{
    // Check call super
    [super controlItem];
}

#pragma mark - Draw

#pragma mark - Call API

#pragma mark - Action

#pragma mark - Delegate

#pragma mark - Subclass need overwrite

#pragma mark - Overwrite
- (void)clickRowWithIndex:(NSInteger)index
{
    QLog(@"");
    
    return;
    NSMutableDictionary *dictParams = [NSMutableDictionary new];
    [dictParams setValue:[User auth_token] forKeyPath:K_AUTH_TOKEN];
    
    [self callAPIIS:dictParams withAPI:@""];
}

@end

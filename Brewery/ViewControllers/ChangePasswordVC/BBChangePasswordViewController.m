//
//  BBChangePasswordViewController.m
//  Brewery
//
//  Created by Thuy Dao on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBChangePasswordViewController.h"

@interface BBChangePasswordViewController ()

@end

@implementation BBChangePasswordViewController

- (void)dealloc
{
    background = nil;
    scrollView = nil;
    lbNewPassword = nil;
    lbPassword = nil;
    lbTypePassword = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set Title
    self.title = NSLocalizedString(@"Change Password", nil);
    
    //update scrollview
    [scrollView setContentSize:background.frame.size];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action

- (IBAction)clickChange:(id)sender
{
    QLog(@"");
    if (![self Validate]) {
        return;
    }
}

- (BOOL)Validate
{
    if ([tfPassword.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_PASSWORD);
        return NO;
    }
    
    if ([tfNewPassword.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", @"Please fill in New Password before proceeding");
        return NO;
    }
    if ([tfTypePassword.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", @"Please fill in Retype New Password before proceeding");
        return NO;
    }
    
    return YES;

}

@end

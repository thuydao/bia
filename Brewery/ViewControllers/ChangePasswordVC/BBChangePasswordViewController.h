//
//  BBChangePasswordViewController.h
//  Brewery
//
//  Created by Thuy Dao on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"

@interface BBChangePasswordViewController : BaseViewController
{
    IBOutlet UIImageView *background;
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UITextField *tfNewPassword;
    IBOutlet UITextField *tfPassword;
    IBOutlet UITextField *tfTypePassword;
}

- (IBAction)clickChange:(id)sender;

@end

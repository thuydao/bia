#import "BaseViewController.h"
#import "BBHomeViewController.h"

@interface BBRegisterViewController : BaseViewController
{
    IBOutlet UIImageView    *background;
    IBOutlet UIScrollView   *scrollView;
    IBOutlet UIButton       *btnFacebook;
    
    IBOutlet UITextField    *tfEmail;
    IBOutlet UITextField    *tfName;
    IBOutlet UITextField    *tfPassword;
    IBOutlet UITextField    *tfConfirmPassword;
}


- (IBAction)clickRegisterTwitter:(id)sender;
- (IBAction)clickRegisterGoogle:(id)sender;
- (IBAction)clickRegisterEmail:(id)sender;

@end

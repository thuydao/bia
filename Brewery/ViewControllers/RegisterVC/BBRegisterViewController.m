#import "BBRegisterViewController.h"

@implementation BBRegisterViewController

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.title = NSLocalizedString(@"Create an Account", @"Register Title");
    
    [scrollView setContentSize:background.frame.size];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBAction

//===================================== clickRegisterTwitter ==========================================

- (IBAction)clickRegisterTwitter:(id)sender
{
    QLog(@"");
}

//===================================== clickRegisterGoogle ==========================================

- (IBAction)clickRegisterGoogle:(id)sender
{
    QLog(@"");
}

//===================================== clickRegisterEmail ==========================================

- (IBAction)clickRegisterEmail:(id)sender
{
    QLog(@"");
    
    if (![self validate]) {
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:tfName.text forKey:@"username"];
    [params setObject:tfEmail.text forKey:@"email"];
    [params setObject:tfPassword.text forKey:@"password"];
    
    
    startProgressHUD(@"Loading");
    //call API
    [Services callAPIWithUrl:API_CREATE_ACCOUNT TypeAPI:POST withParams:params completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                return ;
            }
            
          
            
            stopProgressHUD();
            [self callAPILogin];
        }
    }];
}

- (void)callAPILogin
{
    [SVProgressHUD showErrorWithStatus:@"Login..."];
    //param init
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:tfEmail.text forKey:@"email"];
    [params setObject:tfPassword.text forKey:@"password"];
    
    [Services callAPIWithUrl:API_LOGIN_EMAIL TypeAPI:POST withParams:params completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            QLog(@"error : %@",error);
            [SVProgressHUD dismiss];
            return ;
            
        }
        
        if ([Services requestError:result]) return;
        
        User *user = [[User alloc] initWithDictionary:result];
        //chage user to current user
        [user setMySelf];
        [SVProgressHUD dismiss];
        [self pushViewControllerWithNibName:NSStringFromClass([BBHomeViewController class])];
    }];

}

#pragma mark - action

- (BOOL)validate
{
    if ([tfEmail.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_EMAIL);
        return NO;
    }
    if ([tfName.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_NAME);
        return NO;
    }
    if ([tfPassword.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_PASSWORD);
        return NO;
    }
    if ([tfConfirmPassword.text isEqualToString:@""]) {
        TDAlertWithParam(@"Error", K_MSG_EMPTY_CF_PASSWORD);
        return NO;
    }
    if (![tfConfirmPassword.text isEqualToString:tfPassword.text]) {
        TDAlertWithParam(@"Error", K_MSG_MATCH_PASSWORD);
        return NO;
    }
    
    return YES;
}

@end

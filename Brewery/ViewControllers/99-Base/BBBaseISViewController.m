//
//  BBBaseISViewController.m
//  Brewery
//
//  Created by Bunlv on 8/21/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBBaseISViewController.h"

@interface BBBaseISViewController ()

@end

@implementation BBBaseISViewController

#pragma mark - View default
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    // Need call super
    [super viewDidLoad];
    
    // Begin code here
    [self addAds:top];
    [self addBtnHome];
    
    //
    [self setViewInfo];
    [self getDataByLoadFromServer];
    [self controlItem];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - SVG
#pragma mark + Set
- (void)setViewInfo
{
    // Need call super
    
    // Begin code here
    selectedRow = -1;
    
    if ( !arrDataSource )
    {
        arrDataSource = [NSMutableArray new];
    }
    [arrDataSource removeAllObjects];
    
    [arrDataSource addObject:@"Via Facebook"];
    [arrDataSource addObject:@"Via Twitter"];
    [arrDataSource addObject:@"Via Google Plus"];
    
    [self.tableView reloadData];
    [self.tableView sizeToFit];
    [self.tableView borderWithBorderWidth:1.0 withBoderColor:[UIColor lightGrayColor] withCornerRadius:4.0 andWithMasksToBounds:YES];
    self.tableView.scrollEnabled = NO;
}

#pragma mark + Get
- (void)getDataByLoadFromServer
{
    // Need call super
    
    // Begin code here
    
}

- (UIImage *)getImageForKey:(NSString *)key
{
    NSString *imageName = @"";
    
    if ( [key isEqualToStringInsensitive:[arrDataSource objectAtIndex:0]] )
    {
        imageName = @"ic_facebook.png";
    }
    else if ( [key isEqualToStringInsensitive:[arrDataSource objectAtIndex:1]] )
    {
        imageName = @"ic_twitter.png";
    }
    else if ( [key isEqualToStringInsensitive:[arrDataSource objectAtIndex:2]] )
    {
        imageName = @"ic_google.png";
    }
    
    return [UIImage imageNamed:imageName];
}

#pragma mark + Fake

#pragma mark + Add

#pragma mark + Remove

#pragma mark + Control Item
- (void)controlItem
{
    // Need call super
    
    // Begin code here
    
}

#pragma mark - Draw

#pragma mark - Call API
- (void)callAPIIS:(NSMutableDictionary *)dictParams withAPI:(NSString *)api
{
    startProgressHUD(@"Processing");
    
    [Services callAPIWithUrl:api TypeAPI:POST withParams:dictParams completed:^(NSDictionary *result, NSError *error) {
        
        if ( error )
        {
            TDAlertWithParam(@"", @"IS fails");
            
            stopProgressHUD();
        }
        else
        {
            if ( result )
            {
                if ( [result intForKey:K_ERROR_CODE] == DEFAULT_INT_ERROR_CODE_OK )
                {
                    // Work for data ...
                    
                }
                else
                {
                    TDAlertWithParam(@"", [result stringForKey:K_MESSAGE]);
                }
            }
            else
            {
                TDAlertWithParam(@"", @"Respone data is null");
            }
            
            stopProgressHUD();
        }
    }];
}

#pragma mark - Action

#pragma mark - Delegate
#pragma mark + UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifierXXX = @"BBBaseISCells";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierXXX];
    
    if ( !cell )
    {
        [tableView registerNib:[UINib nibWithNibName:cellIdentifierXXX bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifierXXX];
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierXXX];
    }
    
    // Configurment cell . . . . . .
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    NSString *text = [arrDataSource objectAtIndex:indexPath.row];
    
    // Background
    UIImageView *imageView1 = [cell imageViewWithTag:1];
    [imageView1 setBackgroundColor:[UIColor whiteColor]];
    
    // Icon
    UIImageView *imageView2 = [cell imageViewWithTag:2];
    [imageView2 setBackgroundColor:[UIColor clearColor]];
    [imageView2 setImage:[self getImageForKey:text]];
    [imageView2 borderWithBorderWidth:1.0 withBoderColor:[UIColor clearColor] withCornerRadius:2 andWithMasksToBounds:YES];
    
    // Name
    UILabel *label3 = [cell labelWithTag:3];
    [label3 setBackgroundColor:[UIColor clearColor]];
    [label3 setFont:[UIFont fontWithName:FONT_NAME size:15]];
    [label3 setTextColor:[UIColor blackColor]];
    [label3 setAlpha:0.7];
    [label3 setText:text];
    
    // Line
    UIImageView *imageView4 = [cell imageViewWithTag:4];
    [imageView4 setBackgroundColor:[UIColor lightGrayColor]];
    [imageView4 setAlpha:0.2];
    
    // Select
    UIImageView *imageView5 = [cell imageViewWithTag:5];
    if ( selectedRow == indexPath.row )
    {
        [imageView5 setBackgroundColor:[UIColor colorWithRed:237.0/255.0 green:156.0/255.0 blue:2.0/255.0 alpha:1.0]];
    }
    else
    {
        [imageView5 setBackgroundColor:[UIColor clearColor]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    selectedRow = indexPath.row;
    
    [self.tableView reloadData];
    [self.tableView sizeToFit];
    
    [self clickRowWithIndex:indexPath.row];
}

#pragma mark - Subclass need overwrite
- (void)clickRowWithIndex:(NSInteger )index
{
    QLog(@"");
}

#pragma mark - Overwrite
//overwrite back method
- (void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}



@end

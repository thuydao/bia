//
//  BBBaseISViewController.h
//  Brewery
//
//  Created by Bunlv on 8/21/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseTableViewController.h"

@interface BBBaseISViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *arrDataSource;
    NSInteger selectedRow;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

#pragma mark - View default

#pragma mark - SVG
#pragma mark + Set
- (void)setViewInfo;

#pragma mark + Get
- (void)getDataByLoadFromServer;

#pragma mark + Fake

#pragma mark + Add

#pragma mark + Remove

#pragma mark + Control Item
- (void)controlItem;

#pragma mark - Draw

#pragma mark - Call API
- (void)callAPIIS:(NSMutableDictionary *)dictParams withAPI:(NSString *)api;

#pragma mark - Action

#pragma mark - Delegate

#pragma mark - Subclass need overwrite
- (void)clickRowWithIndex:(NSInteger )index;

#pragma mark - Overwrite

@end

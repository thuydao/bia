#import "BBHomeViewController.h"

@implementation BBHomeViewController

#pragma mark - UIViewController

- (void)dealloc
{
    QLog(@"");
    background = nil;
    scrollView = nil;
    self.tabBarLeaderBoard = nil;
    self.tabBarNearByAZMAP = nil;
    self.tabbarFriend = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createTabBarNearByAZMAP];
    [self createTabBarLeaderBoard];
    [self createTabbarFriend];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //set Title
    self.title = NSLocalizedString(@"Brewery Explorer", @"Home title");
    
    //hide back button
    self.navigationItem.leftBarButtonItem = nil;
    
    //add ads
    [self addAds:bottom];

    
    [scrollView setContentSize:background.frame.size];
    
    
    for (NSInteger i = 1; i <= 8; i++) {
        UIImageView *imv = [self.view imageViewWithTag:i];
        
        //event click change view
        [imv handleTapBlock:^(UITapGestureRecognizer *sender) {
            QLog(@"");
            
            if (i == 1) {
                //go to mapview
                self.tabBarNearByAZMAP.selectedIndex = 2;
                [BaseTabbarControler setSelected:self.tabBarNearByAZMAP indexSelected:2];
                
                [self presentViewController:self.tabBarNearByAZMAP animated:YES completion:^{
                    
                }];
            }
            
            if (i == 2) {
                //Go to nearby
                self.tabBarNearByAZMAP.selectedIndex = 0;
                [BaseTabbarControler setSelected:self.tabBarNearByAZMAP indexSelected:0];
                
                [self presentViewController:self.tabBarNearByAZMAP animated:YES completion:^{
                    
                }];
            }
            
            if (i == 3) {
                self.tabBarLeaderBoard.selectedIndex = 1;
                [BaseTabbarControler setSelected:self.tabBarLeaderBoard indexSelected:1];
                
                [self presentViewController:self.tabBarLeaderBoard animated:YES completion:^{
                    
                }];
            }
            
            if (i == 4) {
                //Go to brewerys list
                self.tabBarNearByAZMAP.selectedIndex = 1;
                [BaseTabbarControler setSelected:self.tabBarNearByAZMAP indexSelected:1];
                
                //present tabbar
                [self presentViewController:self.tabBarNearByAZMAP animated:YES completion:^{
                    
                }];
            }
            
            if (i == 5) {
                //Go to myfriend
                //[self pushViewControllerWithNibName:NSStringFromClass([BBMyFriendViewController class])];
                //Go to brewerys list
                self.tabbarFriend.selectedIndex = 0;
                [BaseTabbarControler setSelected:self.tabbarFriend indexSelected:0];
                
                //present tabbar
                [self presentViewController:self.tabbarFriend animated:YES completion:^{
                    
                }];
            }
            
            if (i == 6) {
                //go to mybadge
                [self pushViewControllerWithNibName:NSStringFromClass([BBMyBadgeViewController class])];
            }

            if (i == 7) {
                //go to my profile
                [self pushViewControllerWithNibName:NSStringFromClass([BBMyProfileViewController class])];
            }
            
            if (i == 8) {
                [self pushViewControllerWithNibName:NSStringFromClass([BBInfoViewController class])];
            }
            
        }];
    }
}

- (UITabBarController *)createTabBarNearByAZMAP
{
    if (!self.tabBarNearByAZMAP) {
        
        //init Viewcontroller for tabbarcontrollers
        NSArray *viewControllers = [[NSArray alloc]
                        initWithObjects:[self getViewControllerWithNibName:NSStringFromClass([BBNearByViewController class])],
                        [self getViewControllerWithNibName:NSStringFromClass([BBStatesListViewController class])],
                        [self getViewControllerWithNibName:NSStringFromClass([BBMapBreweriesViewController class])],
                        nil];
        
        //init imagesNomal list
        NSMutableArray *imagesNomal = [NSMutableArray new];
        
        [imagesNomal addObject:@"btn_nearby_unselected.png"];
        [imagesNomal addObject:@"btn_anz_unselected.png"];
        [imagesNomal addObject:@"btn_map_unselected.png"];
       
        
        //init imagesSelected list
        
        NSMutableArray *imagesSelected = [NSMutableArray new];
        
        [imagesSelected addObject:@"btn_nearby_selected.png"];
        [imagesSelected addObject:@"btn_anz_selected.png"];
        [imagesSelected addObject:@"btn_map_selected.png"];
        
        
        //generate tabbarcontroller
        self.tabBarNearByAZMAP = [BaseTabbarControler tabbarFromViewControllers:viewControllers imageNomal:imagesNomal imageSelected:imagesSelected];
        
        
    }
    return self.tabBarNearByAZMAP;
}

- (UITabBarController *)createTabBarLeaderBoard
{

    if (!self.tabBarLeaderBoard) {
        
        //init two viewcontrollers
        BBLeaderBoardViewController *friendVc = [self getViewControllerWithNibName:NSStringFromClass([BBLeaderBoardViewController class])];
        friendVc.type = TYpeFriend;
        
        BBLeaderBoardViewController *globalVC = [self getViewControllerWithNibName:NSStringFromClass([BBLeaderBoardViewController class])];
        globalVC.type = TypeGlobal;
        
        //init Viewcontroller for tabbarcontrollers
        NSArray *viewControllers = [[NSArray alloc]
                                    initWithObjects:friendVc,globalVC,
                                    nil];
        
        //init imagesNomal list
        NSMutableArray *imagesNomal = [NSMutableArray new];
        
        [imagesNomal addObject:@"btn_myfriends_unselected.png"];
        [imagesNomal addObject:@"btn_global_unselected.png"];
        
        
        //init imagesSelected list
        
        NSMutableArray *imagesSelected = [NSMutableArray new];
        
        [imagesSelected addObject:@"btn_myfriends_selected.png"];
        [imagesSelected addObject:@"btn_global_selected.png"];
        
        //generate tabbarcontroller
        self.tabBarLeaderBoard = [BaseTabbarControler tabbarFromViewControllers:viewControllers imageNomal:imagesNomal imageSelected:imagesSelected];
    }
    
    return self.tabBarLeaderBoard;
}

- (void)createTabbarFriend
{
    if ( !self.tabbarFriend )
    {
        // Init two viewcontrollers
        // Friend list
        BBMyFriendViewController *myFriend = [self getViewControllerWithNibName:NSStringFromClass([BBMyFriendViewController class])];
        myFriend.type = type_myfriend;
        
        // Friend find
        BBMyFriendViewController *findFriend = [self getViewControllerWithNibName:NSStringFromClass([BBMyFriendViewController class])];
        findFriend.type = type_findfriend;
        
        // Friend invite
        BBInviteViewController *inviteFriend = [self getViewControllerWithNibName:NSStringFromClass([BBInviteViewController class])];
        
        // Friend sync
        BBSyncViewController *syncFriend = [self getViewControllerWithNibName:NSStringFromClass([BBSyncViewController class])];
        
        // Init view controller for tabbarcontrollers
        NSArray *viewControllers = [[NSArray alloc]
                                    initWithObjects: myFriend, findFriend, inviteFriend, syncFriend,
                                    nil];
        
        // Init imagesNomal list
        NSMutableArray *imagesNomal = [NSMutableArray new];
        
        [imagesNomal addObject:@"btn_myfriends1_unselected.png"];
        [imagesNomal addObject:@"btn_find_unselected.png"];
        [imagesNomal addObject:@"btn_invite_unselected.png"];
        [imagesNomal addObject:@"btn_sync_unselected.png"];
        
        
        // Init imagesSelected list
        NSMutableArray *imagesSelected = [NSMutableArray new];
        
        [imagesSelected addObject:@"btn_myfriends1_selected.png"];
        [imagesSelected addObject:@"btn_find_selected.png"];
        [imagesSelected addObject:@"btn_invite_selected.png"];
        [imagesSelected addObject:@"btn_sync_selected.png"];
        
        // Generate tabbarcontroller
        self.tabbarFriend = [BaseTabbarControler tabbarFromViewControllers:viewControllers imageNomal:imagesNomal imageSelected:imagesSelected];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

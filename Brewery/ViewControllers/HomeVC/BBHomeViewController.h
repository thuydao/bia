#import "BaseViewController.h"
#import "BBMapBreweriesViewController.h"
#import "BBLeaderBoardViewController.h"
#import "BBInfoViewController.h"
#import "BBMyBadgeViewController.h"
#import "BaseTabbarControler.h"
#import "BBNearByViewController.h"
#import "BBBreweriesListViewController.h"
#import "BBStatesListViewController.h"
#import "BBMyProfileViewController.h"
#import "BBMyFriendViewController.h"
#import "BBInviteViewController.h"
#import "BBSyncViewController.h"

@interface BBHomeViewController : BaseViewController
{
    IBOutlet UIImageView    *background;
    IBOutlet UIScrollView   *scrollView;
}

@property (nonatomic, retain) UITabBarController *tabBarNearByAZMAP;
@property (nonatomic, retain) UITabBarController *tabBarLeaderBoard;
@property (nonatomic, retain) UITabBarController *tabbarFriend;

@end

//
//  BBNearByViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBNearByViewController.h"
#import "UITableView+DragLoad.h"

@interface BBNearByViewController ()

@end

@implementation BBNearByViewController

#pragma mark - Defaults

- (void)dealloc
{
    QLog(@"");
    background = nil;
    self.tableView  = nil;
    self.dataSource = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //dataSource init
    self.dataSource  = [NSMutableArray new];
    
    //set Title
    self.title = NSLocalizedString(@"Nearby Breweries ", nil);
    
    //border tableview
    self.tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.tableView.layer.borderWidth = 0.5;
    self.tableView.layer.cornerRadius = 5;
    
    //add filter
    [self addFilter];
    
    //fake data
//    [self fakeData];
    
    //callAPI
    [self callAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - OVerWriter

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - fake data

- (void)fakeData
{
    for (NSInteger i = 0; i < 50; i ++) {
        Brewery *obj0   = [Brewery new];
        obj0.lat        = 40.740384;
        obj0.lon        = -73.991146;
        obj0.name       = @"Ftarbucks NY";
        obj0.icon       = @"ic_pin_verified.png";
        obj0.address    = @"Nguyen Phong Sac keo dai - Cau Giay - Ha Noi - Viet Nam";
        obj0.startTime1 = @"Mon - Fri: 10 - 6 pm";
        obj0.startTime2 = @"Sat - Sun: 11 - 7 pm";
        obj0.linkweb    = @"http://dantri.com";
        obj0.distance   = @"1.1 mi";
        [self.dataSource addObject:obj0];
    }
    
    //reload
    [self.tableView reloadData];
}

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");

    [[TDLocationManager shared] getCLLocationCompleteBlock:^(CLLocation *result, NSError *error) {
           //param init
        NSMutableDictionary *param = [NSMutableDictionary new];
        [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
        [param setObject:[NSString stringWithFormat:@"%d",self.page +1 ] forKey:@"page"];
        [param setObject:[NSString stringFromFloat:result.coordinate.latitude] forKey:@"lat"];
        [param setObject:[NSString stringFromFloat:result.coordinate.longitude] forKey:@"lng"];
        
        if (![[BBUserDefault filter] isEqualToStringInsensitive:NSLocalizedString(@"All", nil)]) {
            NSString *visited = @"0";
            if ([[BBUserDefault filter] isEqualToStringInsensitive:NSLocalizedString(@"Visited", nil)]) {
                visited = @"1";
            }
            [param setObject:visited forKey:@"visited"];
        }
        
        [Services callAPIWithUrl:API_BREWERY_NEAR TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
            if (error) {
                TDAlertWithParam(@"Loi", @"Data load fails");
                
                stopProgressHUD();
                [self.tableView stopLoadMore];
                [self.tableView stopRefresh];
            }
            
            else
            {
                if ([Services requestError:result])
                {
                    stopProgressHUD();
                    [self.tableView stopLoadMore];
                    [self.tableView stopRefresh];
                    
                    //if page == 0 ==> clear table
                    if (self.page == 0 ) {
                        [self copyData:[NSArray new]];
                    }
                    
                    return ;
                }
                self.page = self.page + 1;
                NSMutableArray *brewerys = [result objectForKey:K_BREWERYS];
                NSMutableArray *arr = [NSMutableArray new];
                for (NSDictionary *dict in brewerys) {
                    Brewery *obj = [[Brewery alloc] initWithDictionary:dict];
                    [arr addObject:obj];
                }
                [self copyData:arr];
                stopProgressHUD();
            }
        }];

        
    }];
}

#pragma mark - Action

- (void)changeFilter
{
    //reset page
    self.page = 0;
    [self callAPI];
}

#pragma mark - Table View
//NearByCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"NearByCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //get object
    Brewery *obj = [self.dataSource objectAtIndex:indexPath.row];
    UIImage *img = [UIImage imageNamed:obj.icon];
    
    [[cell imageViewWithTag:1] setImage:img];
    [cell labelWithTag:2].text = obj.name;
    [cell labelWithTag:3].text = obj.distance;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Brewery *obj = [self.dataSource objectAtIndex:indexPath.row];
    
    BBDetailViewController *detailVC = [self getViewControllerWithNibName:NSStringFromClass([BBDetailViewController class])];
    detailVC.BreweryID = obj.identifier;
    
    [self.navigationController pushViewController:detailVC animated:YES];

}

@end

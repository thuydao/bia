//
//  BBNearByViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBDetailViewController.h"
#import "BaseTableViewController.h"
#import "TDLocationManager.h"


@interface BBNearByViewController : BaseTableViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIImageView *background;
}


@end

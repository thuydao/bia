//
//  BBStaticWebViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"

@interface BBStaticWebViewController : BaseViewController
{
    IBOutlet UIWebView *webView;
}

@end

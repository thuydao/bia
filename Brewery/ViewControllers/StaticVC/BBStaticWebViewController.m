//
//  BBStaticWebViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBStaticWebViewController.h"

@interface BBStaticWebViewController ()

@end

@implementation BBStaticWebViewController

#pragma mark - Defaults

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set title
    [self setTitleText];
    
    //add Home Button in Navigationbar
    [self addBtnHome];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[BBUserDefault nameSource] ofType:@"html"];
    NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    [webView loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
}

- (void)setTitleText
{
    NSString *source = [BBUserDefault nameSource];
    
    if ([source isEqualToString:@"TermOfServices"])
    {
        self.title = NSLocalizedString(@"Terms Of Services",nil);
    }
    
    if ([source isEqualToString:@"About"]) {
        self.title = NSLocalizedString(@"About",nil);
    }
    
    if ([source isEqualToString:@"Privacy"]) {
        self.title = NSLocalizedString(@"Privacy Policy",nil);
    }
}

#pragma mark - fake data

#pragma mark - CallAPI

#pragma mark - Action




@end

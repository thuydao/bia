//
//  BBUserDetailViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBUserDetailViewController.h"

@interface BBUserDetailViewController ()

@end

@implementation BBUserDetailViewController
@synthesize object;

#pragma mark - Defaults

- (void)dealloc
{
    scrollView      = nil;
    background      = nil;
    lbName          = nil;
    lbPoint         = nil;
    lbbadges        = nil;
    lbNumVerified   = nil;
    lbNumNoVerified = nil;
    imv0            = nil;
    imv1            = nil;
    imv2            = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Update scrollview
    [scrollView setContentSize:background.frame.size];
    
    //add ads in top
    [self addAds:top];
    
    //add home button in navigationbar
    [self addBtnHome];
    
    // Call API
    NSMutableDictionary *dictParams = [NSMutableDictionary new];
    [dictParams setValue:[User auth_token] forKey:K_AUTH_TOKEN];
    [dictParams setValue:object.uID forKey:@"friend_id"];
    
    [self callAPILoadFriendDetail:dictParams];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //reload data
    [self reloadData];
}

#pragma mark - fake data

#pragma mark - CallAPI
- (void)callAPILoadFriendDetail:(NSMutableDictionary *)dictParams
{
    NSString *api = API_FRIEND_DETAIL;
    
    startProgressHUD(@"Loading");
    
    [Services callAPIWithUrl:api TypeAPI:POST withParams:dictParams completed:^(NSDictionary *result, NSError *error) {

        if ( error )
        {
            TDAlertWithParam(@"Loi", @"Data load fails");
            
            stopProgressHUD();
        }
        else
        {
            if ( result )
            {
                if ( [result intForKey:K_ERROR_CODE] == DEFAULT_INT_ERROR_CODE_OK )
                {
                    object = [[User alloc] initWithDictionary:[result dictionaryForKey:@"user"]];
                    
                    [self reloadData];
                }
                else
                {
                    TDAlertWithParam(@"", [result stringForKey:K_MESSAGE]);
                }
            }
            else
            {
                TDAlertWithParam(@"", @"");
            }
            
            stopProgressHUD();
        }
    }];
}

#pragma mark - Action

- (void)reloadData
{
    //check input
    if ( !object )
        return;
    
    lbName.text = object.name;
    lbPoint.text = object.point;
    lbbadges.text = object.badges;
    lbNumNoVerified.text = object.numNoVerified;
    lbNumVerified.text = object.numverified;
    
    //set icon
    for (NSString *icon in object.icons)
    {
        if ( !imv0.image )
        {
            [imv0 setImage:[UIImage imageNamed:icon]];
        }
        else if ( !imv1.image )
        {
            [imv1 setImage:[UIImage imageNamed:icon]];
        }
        else
        {
            [imv2 setImage:[UIImage imageNamed:icon]];
        }
    }
}

@end

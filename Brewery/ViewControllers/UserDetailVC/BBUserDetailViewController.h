//
//  BBUserDetailViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"

@interface BBUserDetailViewController : BaseViewController
{
    IBOutlet UIImageView *background;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lbName;
    IBOutlet UILabel *lbPoint;
    IBOutlet UILabel *lbbadges;
    IBOutlet UILabel *lbNumVerified;
    IBOutlet UILabel *lbNumNoVerified;
    IBOutlet UIImageView *imv0;
    IBOutlet UIImageView *imv1;
    IBOutlet UIImageView *imv2;
}

@property (nonatomic, retain) User *object;

@end

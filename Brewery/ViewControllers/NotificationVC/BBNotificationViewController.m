//
//  BBNotificationViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBNotificationViewController.h"
#import "UITableView+DragLoad.h"

@interface BBNotificationViewController ()

@end

@implementation BBNotificationViewController

#pragma mark - Defaults

- (void)dealloc
{
    background = nil;
    self.dataSource = nil;
    self.tableView = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //set Title
    self.title = NSLocalizedString(@"Notification Center", nil);
    
    //add home button in navigation controller
    [self addBtnHome];
    
    //add ads in top
    [self addAds:top];
    
    //fake data
    //    [self fakeData];
    
    //callAPI
    [self callAPI];
    
    //set background tableview
    [self.tableView setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - fake data
- (void)fakeData
{
    [self.dataSource addObject:@"1"];
    [self.dataSource addObject:@"2"];
    [self.dataSource addObject:@"2"];
    [self.dataSource addObject:@"2"];
    [self.dataSource addObject:@"2"];
    [self.dataSource addObject:@"2"];
    [self.dataSource addObject:@"2"];
    
    [self.tableView reloadData];
}

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    //param init
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
    [param setObject:[NSString stringWithFormat:@"%d",self.page + 1] forKey:@"page"];
    
    //call API
    [Services callAPIWithUrl:API_NOTIFICATION_LIST TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
        if (error) {
            TDAlertWithParam(@"Loi", @"Data load fails");
            [self.tableView stopLoadMore];
            [self.tableView stopRefresh];
            stopProgressHUD();
        }
        
        else
        {
            if ([Services requestError:result])
            {
                stopProgressHUD();
                [self.tableView stopLoadMore];
                [self.tableView stopRefresh];
                return ;
            }
            
            //TODO
            self.page = self.page + 1;
            NSArray *arr = [result arrayForKey:@"notify"];
            [self copyData:arr];
            
            
            stopProgressHUD();
        }
    }];
    
}

#pragma mark - Action

#pragma mark - tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
        if ([[dict stringForKey:@"notify_type"] isEqualToStringInsensitive:@"add_friend"]) return 110;
        return 57;
    }
    @catch (NSException *exception) {
        return 0;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
  
    @try {
        
        NSString *identifier = @"infocell";
        
        NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
        //if "notify_type" == "add_friend";
        if ([[dict stringForKey:@"notify_type"] isEqualToStringInsensitive:@"add_friend"])
            identifier = @"RequestFriendCell";
        
        cell = [tableView1 dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
        //fill data
        if ([[dict stringForKey:@"notify_type"] isEqualToStringInsensitive:@"add_friend"])
        {
            NSDictionary *object = [dict dictionaryForKey:@"object"];
            
            [cell labelWithTag:2].attributedText =
            [TDMultiTypeInString multiFontWithStrings:@[[object stringForKey:@"username"],
                                                        [object stringForKey:@"content"]]
                                                fonts:@[[UIFont fontWithName:FONT_NAME_BOLD size:14],
                                                        [UIFont fontWithName:FONT_NAME size:14]]];
        }
        
        else if ([[dict stringForKey:@"notify_type"] isEqualToStringInsensitive:@"accept_friend"])
        {
            NSDictionary *object = [dict dictionaryForKey:@"object"];
            
            [cell labelWithTag:2].attributedText =
            [TDMultiTypeInString multiFontWithStrings:@[[object stringForKey:@"username"],
                                                        [object stringForKey:@"content"]]
                                                fonts:@[[UIFont fontWithName:FONT_NAME_BOLD size:14],
                                                        [UIFont fontWithName:FONT_NAME size:14]]];
        }
        
        else if ([[dict stringForKey:@"notify_type"] isEqualToStringInsensitive:@"brewery_verified"])
        {
            NSDictionary *object = [dict dictionaryForKey:@"object"];
            
            [cell labelWithTag:2].attributedText =
            [TDMultiTypeInString multiFontWithStrings:@[[object stringForKey:@"username"],
                                                        [object stringForKey:@"content"],
                                                        [object stringForKey:@"brewery_name"]]
                                                fonts:@[[UIFont fontWithName:FONT_NAME_BOLD size:14],
                                                        [UIFont fontWithName:FONT_NAME size:14],
                                                        [UIFont fontWithName:FONT_NAME_BOLD size:14]]];
        }
        
        //brewery_visited
        else
        {
            NSDictionary *object = [dict dictionaryForKey:@"object"];
            
            [cell labelWithTag:2].attributedText =
            [TDMultiTypeInString multiFontWithStrings:@[[object stringForKey:@"username"],
                                                        [object stringForKey:@"content"],
                                                        [object stringForKey:@"brewery_name"]]
                                                fonts:@[[UIFont fontWithName:FONT_NAME_BOLD size:14],
                                                        [UIFont fontWithName:FONT_NAME size:14],
                                                        [UIFont fontWithName:FONT_NAME_BOLD size:14]]];
        }

    }
    @catch (NSException *exception) {
        QLog(@"crash khi mang qua nhanh");
        return [[UITableViewCell alloc] init];
    }
    
    //setting cell
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    return cell;
}


@end

//
//  BBNotificationViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import "TDMultiTypeInString.h"
#import "BaseTableViewController.h"

@interface BBNotificationViewController : BaseTableViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIImageView *background;
}

@end

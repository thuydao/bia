//
//  BBMapBreweriesViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBMapBreweriesViewController.h"

#define METERS_PER_MILE 1609.344

@interface BBMapBreweriesViewController ()

@end

@implementation BBMapBreweriesViewController

#pragma mark - Defaults

- (void)dealloc
{
    QLog(@"//===============================================================================");
    self.mapView.delegate = nil;
    self.mapView          = nil;
    dataSource            = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set Title
    self.title = NSLocalizedString(@"Map of Breweries ", nil);
    
    //add Home button in navigationbar
    [self addBtnHome];
    
    //add top ads
    [self addAds:top];
    
    //init datasource
    dataSource = [NSMutableArray new];
    
    //fakedata
//    [self fakeData];
    
    //callAPI
    [self callAPI];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)reloadMap
{
    //show all datasource
    for (Brewery *obj in dataSource) {
        myAnnotation *annotation = [[myAnnotation alloc] initWithObject:obj];
        [self.mapView addAnnotation:annotation];
    }
}

#pragma mark - OVerWriter
//overwrite back method
- (void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - fake data

- (void)fakeData
{
    //init object
    Brewery *obj0 = [Brewery new];
    obj0.lat = 40.740384;
    obj0.lon = -73.991146;
    obj0.name = @"Starbucks NY";
    obj0.icon = @"ic_pin_verified.png";
    [dataSource addObject:obj0];
    
    Brewery *obj1 = [Brewery new];
    obj1.lat = 40.741623;
    obj1.lon = -73.992021;
    obj1.name = @"Pascal Boyer Gallery";
    obj1.icon = @"ic_pin_notverify.png";
    [dataSource addObject:obj1];
    
    Brewery *obj2 = [Brewery new];
    obj2.lat = 40.739490;
    obj2.lon = -73.991154;
    obj2.name = @"Virgin Records";
    obj2.icon = @"ic_pin_notvisit.png";
    [dataSource addObject:obj2];
    
    //reload map
    [self reloadMap];
}

#pragma mark - CallAPI

- (void)callAPI
{
    startProgressHUD(@"Loading");
    
    [[TDLocationManager shared] getCLLocationCompleteBlock:^(CLLocation *result, NSError *error) {
        
        [self setupViewMap:result.coordinate];
        
        //param init
        NSMutableDictionary *param = [NSMutableDictionary new];
        [param setObject:[User auth_token] forKey:K_AUTH_TOKEN];
        [param setObject:[NSString stringWithFormat:@"1"] forKey:@"page"];
        [param setObject:[NSString stringFromFloat:result.coordinate.latitude] forKey:@"lat"];
        [param setObject:[NSString stringFromFloat:result.coordinate.longitude] forKey:@"lng"];
        
        if (![[BBUserDefault filter] isEqualToStringInsensitive:NSLocalizedString(@"All", nil)]) {
            NSString *visited = @"0";
            if ([[BBUserDefault filter] isEqualToStringInsensitive:NSLocalizedString(@"Visited", nil)]) {
                visited = @"1";
            }
            [param setObject:visited forKey:@"visited"];
        }
        
        [Services callAPIWithUrl:API_BREWERY_NEAR TypeAPI:POST withParams:param completed:^(NSDictionary *result, NSError *error) {
            if (error) {
                TDAlertWithParam(@"Loi", @"Data load fails");
                
                stopProgressHUD();
            }
            
            else
            {
                if ([Services requestError:result])
                {
                    stopProgressHUD();
                    return ;
                }
                NSMutableArray *brewerys = [result objectForKey:K_BREWERYS];
                for (NSDictionary *dict in brewerys) {
                    Brewery *obj = [[Brewery alloc] initWithDictionary:dict];
                    [dataSource addObject:obj];
                }
                [self reloadMap];
                stopProgressHUD();
            }
        }];
        
        
    }];
}

#pragma mark - Action

- (void)setupViewMap:(CLLocationCoordinate2D)zoomLocation
{
    //fix locaton
//    if (!zoomLocation) {
//        zoomLocation.latitude = 40.740848;
//        zoomLocation.longitude= -73.991145;
//    }
    
    
    
    //set distance mapview
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.2*METERS_PER_MILE, 0.2*METERS_PER_MILE);
    [self.mapView setRegion:viewRegion animated:YES];
}

#pragma mark -MapView Delegate Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    static NSString *identifier = @"myAnnotation";
    
    //init & reuse
    MyAnnotationCallOut * annotationView = (MyAnnotationCallOut *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!annotationView)
    {
        //init MKAnnotationView
        annotationView = [[MyAnnotationCallOut alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
        //get and resize image pin
        UIImage *image = [UIImage imageNamed:[(myAnnotation*)[annotationView annotation] imageName]];
        image = [image resizedImage:CGSizeMake(40, 40) interpolationQuality:kCGInterpolationDefault];
        
//        annotationView.image = image;
        
//        [annotationView.contentView setBackgroundColor:[UIColor redColor]];
        
        UIImageView *imv =[[UIImageView alloc] initWithImage:image];
        imv.tag = 1;
        imv.frame = CGRectMake((annotationView.contentView.frame.size.width - 40)/2, annotationView.contentView.frame.size.height - 40, 40, 40);
        [annotationView.contentView addSubview:imv];
        
    }else {
        annotationView.annotation = annotation;
    }
    return annotationView;
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {
        
        MyAnnotationCallOut *annotationView = (MyAnnotationCallOut*)view;
        
        //focus
        [mapView setCenterCoordinate: [view annotation].coordinate animated:YES];
        
        //calloutView init
        CalloutView *calloutView = (CalloutView *)[[[NSBundle mainBundle] loadNibNamed:@"calloutView" owner:self options:nil] objectAtIndex:0];
        
        //set frame
        [UIView animateWithDuration:0.7f
                         animations:^
         {
             CGRect calloutViewFrame = calloutView.frame;
             annotationView.frame = CGRectMake(0, 0, calloutView.frame.size.width, calloutView.frame.size.height + 40);
             annotationView.contentView.frame = annotationView.frame;
             
             UIImageView *imv =[annotationView.contentView imageViewWithTag:1];
             imv.frame = CGRectMake((annotationView.contentView.frame.size.width - 40)/2, annotationView.contentView.frame.size.height - 40, 40, 40);
             
             calloutViewFrame.origin = CGPointMake(0, 0);
             calloutView.frame = calloutViewFrame;
         }
                         completion:^(BOOL finished)
         {
         }];
        
        //set Title
        [calloutView.calloutLabel setText:[(myAnnotation*)[annotationView annotation] title]];
        
        
        
        //        view.canShowCallout = YES;
        //add to view
        [annotationView addSubview:calloutView];
        [self.mapView bringSubviewToFront:calloutView];
                
        [calloutView listen:^(id sender) {
            
            //get object
            
            Brewery *obj = [(myAnnotation*)[annotationView annotation] obj];
            
            BBDetailViewController *detailVC = [self getViewControllerWithNibName:NSStringFromClass([BBDetailViewController class])];
            detailVC.BreweryID = obj.identifier;
            
            [self.navigationController pushViewController:detailVC animated:YES];
        }];
    }
}


-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    
//    MyAnnotationCallOut *annotationView = (MyAnnotationCallOut*)view;
    for (id subview in view.subviews ){
        if ([subview isKindOfClass:[CalloutView class]]) {
            [subview removeFromSuperview];
            
        }
    }
        
}





@end

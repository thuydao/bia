//
//  BBMapBreweriesViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/6/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
#import "CalloutView.h"
#import "myAnnotation.h"
#import "UIImage+Resize.h"
#import "Brewery.h"
#import "BBDetailViewController.h"
#import "MyAnnotationCallOut.h"
#import "TDLocationManager.h"

@interface BBMapBreweriesViewController : BaseViewController <MKMapViewDelegate>
{
    NSMutableArray *dataSource;
}

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CalloutView *calloutAnnotation;

@end

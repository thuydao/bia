//
//  Brewery.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/11/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "Brewery.h"

@implementation Brewery

- (id)initWithDictionary:(NSDictionary *)aDictionary
{
    if (self = [super init]) {
        self.identifier        = [aDictionary stringForKey:@"id"];
        self.lat               = [aDictionary floatForKey:@"lat"];
        self.lon               = [aDictionary floatForKey:@"lng"];
        self.name              = [aDictionary stringForKey:K_NAME];
        self.verified          = [aDictionary boolForKey:@"verified"];
        self.visited           = [aDictionary boolForKey:@"visited"];

        [self genIcon];

        self.address           = [aDictionary stringForKey:@"address"];

        NSString *opening_hour = [aDictionary stringForKey:@"opening_hour"];
        
        [self openHour:opening_hour];

        self.linkweb           = [aDictionary stringForKey:@"website"];
        self.phone             = [aDictionary stringForKey:@"phone"];
        CGFloat radius         = [[aDictionary stringForKey:@"radius"] floatValue];
        self.distance          = [NSString stringWithFormat:@"%0.1f mil",radius];
    }
    return self;
}


- (void)genIcon
{
    if (self.verified) {
        self.icon = @"ic_pin_verified.png";
    }
    
    else
    {
        if (self.visited) {
            self.icon = @"ic_pin_notverify.png";
        }
        else
        {
            self.icon = @"ic_pin_notvisit.png";
        }
    }
}

- (void)openHour:(NSString *)str
{
    NSArray *arr = [str componentsSeparatedByString:@","];
    if (arr.count > 1) {
        self.startTime1 = [arr objectSafeAtIndex:0];
        self.startTime2 = [arr objectSafeAtIndex:1];
    }
    
    self.startTime1 = str;
}

@end

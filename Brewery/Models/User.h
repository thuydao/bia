//
//  User.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/14/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString *uID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *score;
@property (nonatomic, retain) NSMutableArray  *icons;
@property (nonatomic, assign) NSInteger isFriend;
@property (nonatomic, retain) NSString *point;
@property (nonatomic, retain) NSString *badges;
@property (nonatomic, retain) NSString *numverified;
@property (nonatomic, retain) NSString *numNoVerified;
@property (nonatomic, retain) NSString *auth_token;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, assign) BOOL      privacy_public;

@property (nonatomic, assign) BOOL isApp, isFacebook, isGoogle, isTwitter;


- (id)initWithDictionary:(NSDictionary *)aDictionary;

//===================================== setMySelf ==========================================
- (void)setMySelf;

//===================================== myself ==========================================
+ (User *)myself;

//===================================== auth_token ==========================================
+ (NSString *)auth_token;

//===================================== identifier ==========================================
+ (NSString *)identifier;

@end

//
//  State.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/20/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "State.h"

@implementation State

- (id)initWithDictionary:(NSDictionary *)aDictionary
{
    if (self = [super init]) {
        self.name       = [aDictionary objectForKey:K_NAME];
        self.identifier = [aDictionary objectForKey:@"id"];
    }
    return self;
}

@end

//
//  State.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/20/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface State : NSObject

@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *name;

- (id)initWithDictionary:(NSDictionary *)aDictionary;

@end

//
//  Brewery.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/11/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Brewery : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, assign) CGFloat  lat;
@property (nonatomic, assign) CGFloat  lon;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *startTime1;
@property (nonatomic, retain) NSString *startTime2;
@property (nonatomic, retain) NSString *linkweb;
@property (nonatomic, retain) NSString *distance;
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, assign) BOOL     verified;
@property (nonatomic, assign) BOOL     visited;

- (id)initWithDictionary:(NSDictionary *)aDictionary;
- (void)genIcon;


@end

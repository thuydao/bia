//
//  User.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/14/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize uID;
@synthesize auth_token, name, email, identifier, score, numNoVerified, numverified, point, badges;
@synthesize icons;
@synthesize isFriend, privacy_public;

@synthesize isApp, isFacebook, isGoogle, isTwitter;

- (id)initWithDictionary:(NSDictionary *)dictData
{
    if (self = [super init])
    {
        uID            = [dictData stringForKey:@"id"];

        auth_token     = [dictData stringForKey:@"auth_token"];
        email          = [dictData stringForKey:@"email"];
        identifier     = [dictData stringForKey:@"id"];
        privacy_public = [dictData boolForKey:@"privacy_public"];
        name           = [dictData stringForKey:@"username"];

        isApp          = [dictData boolForKey:@"is_app"];
        isFacebook     = [dictData boolForKey:@"is_app"];
        isGoogle       = [dictData boolForKey:@"is_app"];
        isTwitter      = [dictData boolForKey:@"is_app"];

        badges         = [dictData stringForKey:@"badges"];
        point          = [dictData stringForKey:@"total_points"];

        isFriend       = [dictData intForKey:@"is_friend"];
        score          = [dictData stringForKey:@"score"];

        numNoVerified  = [dictData stringForKey:@"total_visited"];
        numverified    = [dictData stringForKey:@"total_verified"];
        
        [self configIcon];
    }
    return self;
}

- (void)configIcon
{
    if ( !icons )
    {
        icons = [NSMutableArray new];
    }
    [icons removeAllObjects];
    
    if ( isApp )
    {
        [icons addObject:@""];
    }
    
    if ( isFacebook )
    {
        [icons addObject:@"ic_facebook.png"];
    }
    
    if ( isGoogle )
    {
        [icons addObject:@"ic_google.png"];
    }

    if ( isTwitter )
    {
        [icons addObject:@"c_twitter.png"];
    }
}

#pragma mark - encode


//===================================== encodeWithCoder ==========================================
-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:auth_token forKey:@"auth_token"];
    [encoder encodeObject:name forKey:@"name"];
    [encoder encodeObject:email forKey:@"email"];
    [encoder encodeObject:identifier forKey:@"identifier"];
    [encoder encodeObject:score forKey:@"score"];
    [encoder encodeObject:numNoVerified forKey:@"numNoVerified"];
    [encoder encodeObject:numverified forKey:@"numverified"];
    [encoder encodeObject:point forKey:@"point"];
    [encoder encodeObject:badges forKey:@"badges"];
    [encoder encodeObject:icons forKey:@"icons"];
    [encoder encodeBool:isFriend forKey:@"isFriend"];
    [encoder encodeBool:privacy_public forKey:@"privacy_public"];
}

//===================================== initWithCoder ==========================================
- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        auth_token     = [decoder decodeObjectForKey:@"auth_token"];
        name           = [decoder decodeObjectForKey:@"name"];
        email          = [decoder decodeObjectForKey:@"email"];
        identifier     = [decoder decodeObjectForKey:@"identifier"];
        score          = [decoder decodeObjectForKey:@"score"];
        numNoVerified  = [decoder decodeObjectForKey:@"numNoVerified"];
        numverified    = [decoder decodeObjectForKey:@"numverified"];
        point          = [decoder decodeObjectForKey:@"point"];
        badges         = [decoder decodeObjectForKey:@"badges"];
        icons          = [decoder decodeObjectForKey:@"icons"];
        isFriend       = [decoder decodeBoolForKey:@"isFriend"];
        privacy_public = [decoder decodeBoolForKey:@"privacy_public"];
    }
    return self;
}

//===================================== setMySelf ==========================================
- (void)setMySelf
{
    @synchronized(self) {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"MySelf.txt"];
    
    [NSKeyedArchiver archiveRootObject:(id)self toFile:appFile];
    }
}

//===================================== myself ==========================================
+ (User *)myself
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"MySelf.txt"];
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];
}

//===================================== auth_token ==========================================
+ (NSString *)auth_token
{
    User *user = [self myself];
    return user.auth_token;
}

//===================================== identifier ==========================================
+ (NSString *)identifier
{
    User *user = [self myself];
    return user.identifier;
}



@end

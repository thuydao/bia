//
//  AppDelegate.h
//  Brewery
//
//  Created by Dao Duy Thuy on 7/18/14.
//  Copyright BB 2014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, GPPDeepLinkDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

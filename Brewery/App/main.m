//
//  main.m
//  Brewery
//
//  Created by Dao Duy Thuy on 7/18/14.
//  Copyright BB 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

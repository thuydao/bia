//
//  Defines.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/18/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

//===================================== API ==========================================

#define API_CREATE_ACCOUNT  @"/user/create"
#define API_FORGOT_PASSWORD @"/user/forgotpassword"
#define API_LOGIN_EMAIL     @"/user/login"
#define API_LOGIN_FB        @"/facebook/login"
#define API_LOGIN_GOOGLE    @"/google/login"
#define API_LOGOUT          @"/user/logout"

#define API_FRIEND_LOAD     @"/friend/list"
#define API_FRIEND_DETAIL   @"/friend/detail"

#define API_FRIEND_FIND     @"/friend/findall"
#define API_FRIEND_ADD      @"/friend/add"

#define API_STATE_LIST      @"/states/list/page/"

#define API_BREWERY_LIST    @"/business/listbyname"
#define API_BREWERY_NEAR    @"/business/nearby"
#define API_BREWERY_DETAIL  @"/business/detail"

#define API_LEADERBOAD      @"/user/leaderboard"

#define API_VERIFY          @"/business/verified"

#define API_MY_PROFILE      @"/user/myprofile"
#define API_MY_BADGES       @"/user/mybadges"

#define API_NOTIFICATION_LIST @"/notify/list"

#define API_CONTACT_US  @"/contact/send"



#define K_ERROR_CODE        @"error_code"
#define K_MESSAGE           @"message"
#define K_FRIENDS           @"friends"
#define K_AUTH_TOKEN        @"auth_token"
#define K_PAGE              @"page"
#define K_STATES            @"states"
#define K_NAME              @"name"
#define K_BREWERYS          @"breweries"
#define K_BREWERY           @"brewery"

#define K_IS_FACEBOOK       @"is_facebook"
#define K_IS_GOOGLE         @"is_google"
#define K_IS_TWITTER        @"is_twitter"

#define DEFAULT_INT_ERROR_CODE_OK 0



//===================================== message ==========================================
#define K_MSG_EMPTY  @"Empty"
//#define K_MSG_EMPTY_NAME @"Please fill in Name address before proceeding"
#define K_MSG_EMPTY_MESSAGE @"Please fill in Message address before proceeding"
#define K_MSG_EMPTY_EMAIL  @"Please fill in E-mail address before proceeding"
#define K_MSG_EMPTY_PASSWORD  @"Please fill in Password before proceeding"
#define K_MSG_EMPTY_CF_PASSWORD  @"Please fill in Confirm Password before proceeding"
#define K_MSG_EMPTY_NAME @"Please fill in Name before proceeding"
#define K_MSG_MATCH_PASSWORD @"Password and Confirm Password not matching!"




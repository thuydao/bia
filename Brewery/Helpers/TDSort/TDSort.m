//
//  TDSort.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/14/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "TDSort.h"

#define WORD_LENGTH 5

@implementation TDSort

- (id)initWithDictionary:(NSDictionary *)aDictionary
{
    if (self = [super init]) {
    }
    return self;
}

+ (NSDictionary*) wordsFrom:(NSArray *)data
{
    
    NSMutableArray *sections = [NSMutableArray new];
    NSMutableArray *sources = [NSMutableArray new];
    
    char saveCharacter;
    
    NSMutableArray *temp = [NSMutableArray new];
    
    for (int i = 0; i < [data count]; i++ ) {
        
        Brewery *obj = [data objectAtIndex:i];
        
        char firstCharacter = [obj.name characterAtIndex:0];
        
        if (i == 0) {
            //the first loop => introduction
            saveCharacter = firstCharacter;
            [sections addObject:[NSString stringWithFormat:@"%c",firstCharacter]];
            [temp addObject:obj];
        }
        else
        {
            if (firstCharacter == saveCharacter)
            {
                //same secction
                [temp addObject:obj];
            }
            else
            {
                //new section
                [sources addObject:[[NSArray alloc] initWithArray:temp]];
                [temp removeAllObjects];
                [temp addObject:obj];
                
                saveCharacter = firstCharacter;
                [sections addObject:[NSString stringWithFormat:@"%c",firstCharacter]];
            }
            
        }
    }
    
    if (temp.count != 0) {
        [sources addObject:[[NSArray alloc] initWithArray:temp]];
        [temp removeAllObjects];

    }
    
    return @{@"sections":sections,@"sources":sources};
}


- (void)chooseState:(State *)obj
{
    NSMutableArray *arr = [self lassChoose];
    NSMutableArray *result = [NSMutableArray new];
    
    [result addObject:obj.name];
    if (arr.count > 0) {
        [result addObject:[arr objectAtIndex:0]];
    }
    if (arr.count > 1) {
         [result addObject:[arr objectAtIndex:1]];
    }
    
    [self saveLassChoose:result];
}


- (void)saveLassChoose:(NSMutableArray *)arr
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    [us setObject:arr forKey:@"bb_lassChoose"];
    [us synchronize];
}

- (NSMutableArray *)lassChoose
{
    NSMutableArray *arr;
    
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    NSArray *obj = [us objectForKey:@"bb_lassChoose"];
    
    if (!obj) {
        arr = [NSMutableArray new];
    }
    
    else  arr = [[NSMutableArray alloc] initWithArray:obj];
    
    return arr;
    
}

@end

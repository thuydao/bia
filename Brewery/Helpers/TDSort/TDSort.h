//
//  TDSort.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/14/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "State.h"

@interface TDSort : NSObject

- (id)initWithDictionary:(NSDictionary *)aDictionary;

+ (NSDictionary*) wordsFrom:(NSArray *)data;

@end

//
//  BBUserDefault.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BBUserDefault.h"

@implementation BBUserDefault


//===================================== setSource ==========================================

+ (void)setSource:(NSString *)filename
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    [us setObject:filename forKey:@"FILE_NAME"];
    [us synchronize];
}

//===================================== nameSource ==========================================

+ (NSString *)nameSource
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    return [us objectForKey:@"FILE_NAME"];
}

//===================================== saveFilter ==========================================

+ (void)saveFilter:(NSString *)str
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    [us setObject:str forKey:@"k_saveFilter"];
    [us synchronize];
}

//===================================== saveTimeFilter ==========================================
+ (void)saveTimeFilter:(NSString *)str
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    [us setObject:str forKey:@"k_saveTimeFilter"];
    [us synchronize];
}

//===================================== filter ==========================================

+ (NSString *)filter
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    NSString *rs = [us objectForKey:@"k_saveFilter"];
    if (!rs || [rs isEqualToString:@""]) {
        rs = @"All";
    }
    return rs;
}

//===================================== timeFilter ==========================================
+ (NSString *)timeFilter
{
    NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
    NSString *rs = [us objectForKey:@"k_saveTimeFilter"];
    if (!rs || [rs isEqualToString:@""]) {
        rs = @"All Time";
    }
    return rs;
}

@end

//
//  BBUserDefault.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BBUserDefault : NSObject

//===================================== setSource ==========================================
+ (void)setSource:(NSString *)filename;

//===================================== nameSource ==========================================
+ (NSString *)nameSource;

//===================================== saveFilter ==========================================
+ (void)saveFilter:(NSString *)str;

//===================================== saveTimeFilter ==========================================
+ (void)saveTimeFilter:(NSString *)str;

//===================================== filter ==========================================
+ (NSString *)filter;

//===================================== timeFilter ==========================================
+ (NSString *)timeFilter;

@end

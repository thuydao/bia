//
//  BaseViewController.m
//  BE
//
//  Created by Sinbad Fly on 5/30/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//

#import "BaseViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BaseNavigationController.h"
#import "BaseNavigationBar.h"
#import "UINavigationController+TDCustom.h"

@implementation BaseViewController
@synthesize views = _views;
@synthesize superViewController = _superViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma set NavigationBar

- (void) setColorNavigationbar:(UIColor*) color
{
    UIView *viewTitle = [[UIView alloc] init];
    CGRect titleRect = CGRectMake(0, 0,
                                  320,
                                  44);
    viewTitle.backgroundColor = color;
    viewTitle.frame = titleRect;
    self.navigationItem.hidesBackButton = YES;
    BaseNavigationController *bnvc = (id)self.navigationController;
    bnvc.baseNavigationBar.useBackgroundColor = color;
    [self.navigationController.navigationBar setBackgroundColor:color];
    self.navigationItem.titleView = viewTitle;
}

- (void) setLogoNavigationbar: (UIImage*) image
{
    UIButton *viewTitle = [[UIButton alloc] init];
    CGRect titleRect = CGRectMake(39, 5,
                                  243,
                                  35);
    viewTitle.frame = titleRect;
    [viewTitle endEditing:NO];
    [viewTitle setBackgroundImage:image forState:UIControlStateNormal];
    
    self.navigationItem.titleView = viewTitle;
}

- (void)initBaseAppearance
{
    if (DETECT_OS7) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"i_navigationbar7.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"i_navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    
    [self addBtnBack];
}

- (void)setTitle:(NSString *)aTitle
{
    UILabel *titleUI = [[UILabel alloc] initWithFrame:CGRectZero];
    UIView *viewTitle = [[UIView alloc] init];
    CGFloat wt = 0;
    
    viewTitle.backgroundColor = [UIColor clearColor];
    
    titleUI.backgroundColor = [UIColor clearColor];
//    titleUI.font = BIO_FONT_ARIAL_B(28);
    titleUI.textColor = [UIColor whiteColor];
    titleUI.textAlignment = NSTextAlignmentCenter;
    titleUI.text = aTitle;
    
    wt = [self widthFromString:aTitle font:titleUI.font];
    
    if (wt > 200) {
        wt = 200;
    }
    
    CGRect titleRect = CGRectMake(0, 0,
                                  wt,
                                  44);
    viewTitle.frame = titleRect;
    
    titleRect.origin.y = 0;
    titleRect.size.height = 44;
    titleUI.frame = titleRect;
    
    [viewTitle addSubview:titleUI];
    
    self.navigationItem.titleView = viewTitle;
    [self.navigationController.navigationBarViewCustom removeFromSuperview];
}


#pragma mark - String Untils

- (CGFloat)widthFromString:(NSString*)aString font:(UIFont*)aFont
{
	CGSize maximumLabelSize = CGSizeMake(999,42);
	CGSize sz = [aString sizeWithFont:aFont constrainedToSize:maximumLabelSize
                        lineBreakMode:NSLineBreakByWordWrapping];
    
    return sz.width;
}

#pragma mark - UINavigationBar

- (void)setBackButtonImageNormalName:(NSString*)imageNormalName
                     highlightedName:(NSString*)highlightedName
                              target:(id)target action:(SEL)action
{
    [self.navigationItem setHidesBackButton:YES];
    
    UIButton *tmpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *normalImage = [UIImage imageNamed:imageNormalName];
    UIImage *pressImage = [UIImage imageNamed: highlightedName];
    
    CGSize size = normalImage.size;
    
    [tmpButton setFrame:CGRectMake(10, (44 - size.height)/2, size.width, size.height)];
    [tmpButton setTitle:NSLocalizedString(@"", @"") forState:UIControlStateNormal];
    [tmpButton setBackgroundImage:normalImage forState:UIControlStateNormal];
    [tmpButton setBackgroundImage:pressImage forState:UIControlStateHighlighted];
    [tmpButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:tmpButton] ];
}

- (void)setLeftButtonImageNormalName:(NSString*)imageNormalName
                     highlightedName:(NSString*)highlightedName
                              target:(id)target action:(SEL)action
{
    UIButton *tmpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *normalImage = [UIImage imageNamed:imageNormalName];
    UIImage *pressImage = [UIImage imageNamed: highlightedName];
    
    CGSize size = normalImage.size;
    
    [tmpButton setFrame:CGRectMake(10, (44 - size.height)/2, size.width, size.height)];
    [tmpButton setTitle:NSLocalizedString(@"", @"") forState:UIControlStateNormal];
    [tmpButton setBackgroundImage:normalImage forState:UIControlStateNormal];
    [tmpButton setBackgroundImage:pressImage forState:UIControlStateHighlighted];
    [tmpButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:tmpButton]];
}

- (void)setRightButtonImageNormalName:(NSString*)imageNormalName
                      highlightedName:(NSString*)imageHighlightedName
                               target:(id)target action:(SEL)action
{
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageNormal = [UIImage imageNamed:imageNormalName];
    UIImage *imageHighlight = [UIImage imageNamed:imageHighlightedName];
    
    CGSize size = imageNormal.size;
    CGSize newSize = size;
    CGFloat maxHeight = 25;
    
    if ( size.height >= maxHeight )
    {
        newSize.height = maxHeight;
        newSize.width = maxHeight * size.width / size.height;
    }
    
    [btnRight setFrame:CGRectMake(10, (44 - newSize.height) / 2, newSize.width, newSize.height)];
    //[btnRight setBackgroundColor:COLOR_BASE_GRAY];
    [btnRight setBackgroundImage:imageNormal forState:UIControlStateNormal];
    [btnRight setBackgroundImage:imageHighlight forState:UIControlStateHighlighted];
    [btnRight setTitle:@"" forState:UIControlStateNormal];
    [btnRight addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:btnRight]];
}

- (void)addBtnBack
{
    [self setBackButtonImageNormalName:@"btn_back.png" highlightedName:@"btn_back.png" target:self action:@selector(back)];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
//    
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iphone5_bg_body@2x.png"]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
//    UIColor *status = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_statusbar.png"]];
//    [[UINavigationBar appearance] setBarTintColor:status];

    [self initBaseAppearance];
//    
//    //add log untils
//    UITapGestureRecognizer *twoFingerTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addQLogFuntion)];
//    twoFingerTapGesture.numberOfTapsRequired = 2;
//    twoFingerTapGesture.numberOfTouchesRequired = 1;
//    [self.view addGestureRecognizer:twoFingerTapGesture];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController hiddenNavigationBarCustom];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) setViews:(UIView *)views
{
    _views = views;
}

- (UIView*) views
{
    return _views;
}

#pragma mark - Brewery

//===================================== addBtnHome ==========================================

- (void)addBtnHome
{
    [self setRightButtonImageNormalName:@"ic_home.png" highlightedName:@"ic_home_selected.png" target:(id)self action:@selector(clickHome:)];
}

//===================================== addFilter ==========================================

- (void)addFilter
{
    [self setRightButtonImageNormalName:@"btn_filter.png" highlightedName:@"btn_filter_selected.png" target:(id)self action:@selector(clickFilter:)];
}

//===================================== addTimeFilter ==========================================
- (void)addTimeFilter
{
     [self setRightButtonImageNormalName:@"ic_time.png" highlightedName:@"ic_time.png" target:(id)self action:@selector(clickTimeFilter:)];
}

//===================================== addAds ==========================================

- (void)addAds:(typeAds)typeAds
{
    UIImageView *imv = [[UIImageView alloc] init];
    CGRect frame;
    if (typeAds == top)
    {
        frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    }
    else
    {
        frame = CGRectMake(0, self.view.frame.size.height  - 50, self.view.frame.size.width, 50);
    }
    [imv setFrame:frame];
    [imv setBackgroundImage:[UIImage imageNamed:@"ic_ads.png"]];
    [self.view addSubview:imv];
}

- (void)clickHome:(id)sender
{
    if (self.tabBarController) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        return;
    }
    [self popToHomeView];
    
}

- (void)popToHomeView
{
    Class class = NSClassFromString(@"BBHomeViewController");
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:class]) {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    }
}

- (void)clickFilter:(id)sender
{
    //dropdown list
    QLog(@"");
    
    //array filter init
    NSMutableArray *arr = [NSMutableArray new];
    
    [arr addObject:NSLocalizedString(@"All", nil)];
    [arr addObject:NSLocalizedString(@"Need to Visit", nil)];
    [arr addObject:NSLocalizedString(@"Visited", nil)];
    
    [self presentWYPopoverControllerWithArrayData:arr withViewPosition:(id)sender withMargins:UIEdgeInsetsMake(0,160,0,5) withSelected:[BBUserDefault filter] chooseData:^(NSString *text) {
        // changeFilter
        [self changeFilter];
        
        //save selected text
        [BBUserDefault saveFilter:text];
    }];
}

//===================================== clickTimeFilter ==========================================
- (void)clickTimeFilter:(id)sender
{
    //dropdown list
    QLog(@"");
    
    //array filter init
    NSMutableArray *arr = [NSMutableArray new];
    
    [arr addObject:NSLocalizedString(@"This Week", nil)];
    [arr addObject:NSLocalizedString(@"This Month", nil)];
    [arr addObject:NSLocalizedString(@"All Time", nil)];
    
    [self presentWYPopoverControllerWithArrayData:arr withViewPosition:(id)sender withMargins:UIEdgeInsetsMake(0,160,0,5) withSelected:[BBUserDefault timeFilter] chooseData:^(NSString *text) {
        // changeFilter
        [self changeTimeFilter];
        
        //save selected text
        [BBUserDefault saveTimeFilter:text];
    }];

}

#pragma mark - OverWrite
//===================================== changeFilter ==========================================
- (void)changeFilter
{
    
}

//===================================== changeTimeFilter ==========================================
- (void)changeTimeFilter
{
    QLog(@"");
}

@end
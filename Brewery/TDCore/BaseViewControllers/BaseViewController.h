//
//  BaseViewController.h
//  BE
//
//  Created by Thuy Dao on 5/30/12.
//  Copyright (c) 2012 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+TDUtils.h"
#import "UIViewController+WYPopoverControllerViaTableView.h"

typedef enum : NSUInteger {
    top = 0,
    bottom = 1,
} typeAds;

#define DETECT_OS7    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@class BaseNavigationController;

@interface BaseViewController : UIViewController

@property (strong) UINavigationController* superViewController;
@property (strong) UIView *views;


#pragma set NavigationBar
- (void)setColorNavigationbar:(UIColor *)color;
- (void)setLogoNavigationbar:(UIImage *)image;
- (void)initBaseAppearance;
- (void)setTitle:(NSString *)aTitle;


#pragma mark - String Untils
- (CGFloat)widthFromString:(NSString*)aString font:(UIFont*)aFont;


#pragma mark - UINavigationBar
- (void)setBackButtonImageNormalName:(NSString*)imageNormalName
                     highlightedName:(NSString*)highlightedName
                              target:(id)target action:(SEL)action;
- (void)setLeftButtonImageNormalName:(NSString*)imageNormalName
                     highlightedName:(NSString*)highlightedName
                              target:(id)target action:(SEL)action;
- (void)setRightButtonImageNormalName:(NSString*)imageNormalName
                      highlightedName:(NSString*)imageHighlightedName
                               target:(id)target action:(SEL)action;
- (void) addBtnBack;
- (void) back;

#pragma mark - Brewery

//===================================== addBtnHome ==========================================

- (void)addBtnHome;

//===================================== addFilter ==========================================
- (void)addFilter;

//===================================== addTimeFilter ==========================================
- (void)addTimeFilter;

//===================================== addAds ==========================================
- (void)addAds:(typeAds)typeAds;

#pragma mark - OverWrite
//===================================== changeFilter ==========================================
- (void)changeFilter;

//===================================== changeTimeFilter ==========================================
- (void)changeTimeFilter;

@end

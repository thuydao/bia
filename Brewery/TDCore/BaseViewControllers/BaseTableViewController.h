//
//  BaseTableViewController.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/19/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseViewController.h"


@interface BaseTableViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger page;

- (void)copyData:(NSArray *)arr;
- (void)callAPI;

@end

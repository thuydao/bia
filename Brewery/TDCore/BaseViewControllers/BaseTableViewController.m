//
//  BaseTableViewController.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/19/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseTableViewController.h"
#import "UITableView+DragLoad.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

#pragma mark - Defaults

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //data source init
    self.dataSource = [NSMutableArray new];
    
    //set more & refresh
    [self.tableView setShowRefreshView:YES];
    [self.tableView setShowLoadMoreView:YES];
    
    [self.tableView setDragDelegate:(id)self refreshDatePermanentKey:@"TDTableview-Drag"];
    
    //page init
    self.page = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


#pragma mark - CallAPI

- (void)callAPI
{
    
}

#pragma mark - Action

- (void)copyData:(NSArray *)arr
{
    if (!self.dataSource) self.dataSource = [NSMutableArray new];
    
    //refresh data
    if (self.page == 0 || self.page == 1) {
        [self.dataSource removeAllObjects];
        [self.dataSource addObjectsFromArray:arr];
        
        [self.tableView finishRefresh];
    }
    
    //more data
    else
    {
        [self.dataSource addObjectsFromArray:arr];
        [self.tableView stopLoadMore];
        [self.tableView finishLoadMore];
    }
    
    self.tableView.showLoadMoreView = YES;
    self.tableView.showRefreshView = YES;
    
    [self.tableView reloadData];
}


#pragma mark - tableview

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    return cell;
}


#pragma mark - UITableViewDragLoadDelegate
//Called when table trigger a refresh event.
- (void)dragTableDidTriggerRefresh:(UITableView *)tableView1
{
//    [self.tableView setShowLoadMoreView:NO];
    self.page = 0;
    [self callAPI];
}

//When trigger a load more event, refresh will be canceled. Then this method will be called.
//Cancel operations(Network requests, etc.) related to refresh in this method.
- (void)dragTableRefreshCanceled:(UITableView *)tableView1
{
//    [self.tableView stopRefresh];
}

//Called when table trigger a load more event.
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView1
{
//    [self.tableView setShowRefreshView:NO];
      [self callAPI];
}

//When trigger a refresh event, load more will be canceled. Then this method will be called.
//Cancel operations(Network requests, etc.) related to load more in this method.
- (void)dragTableLoadMoreCanceled:(UITableView *)tableView1
{
//    [self.tableView stopLoadMore];
  
}

@end

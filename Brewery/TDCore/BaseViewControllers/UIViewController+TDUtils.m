//
//  UIViewController+TDUntils.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "UIViewController+TDUtils.h"

@implementation UIViewController (TDUtils)

#pragma mark - TabbarController

- (void)hideTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
        }
    }
    
    [UIView commitAnimations];
}

- (void)showTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        NSLog(@"%@", view);
        
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
            
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
        }
    }
    
    [UIView commitAnimations];
}


#pragma mark - TextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    // Any new character added is passed in as the "text" parameter
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        
        // Return FALSE so that the final '\n' character doesn't get added
        return FALSE;
    }
    // For any other character return TRUE so that the text gets added to the view
    return TRUE;
}


#pragma mark - NavigatonController

- (id)getViewControllerWithNibName:(NSString *)nibName
{
    id viewController = [self.storyboard instantiateViewControllerWithIdentifier:nibName];
    
    return viewController;
}

- (void)pushViewControllerWithNibName:(NSString *)nibName
{
    UIViewController *viewController = [self getViewControllerWithNibName:nibName];
    
    if ([NSStringFromClass([self class]) isEqualToString:nibName]) return;
    
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - GetView

+ (id)getViewFormNibName:(NSString *)nibname index:(NSInteger)index
{
    NSString *nibName = [nibname stringByReplacingOccurrencesOfString:@".xib" withString:@""];
    
    NSArray *arr =  [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    
    if (arr.count > index) {
        return [arr objectAtIndex:index];
    }
    else {
        NSLog(@"nil object  %@", @"getViewFormNibName");
        return nil;
    }
}

@end

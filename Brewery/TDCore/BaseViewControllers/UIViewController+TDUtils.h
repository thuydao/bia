//
//  UIViewController+TDUntils.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TDUtils)

#pragma mark - TabbarController
- (void)hideTabBar:(UITabBarController *) tabbarcontroller;
- (void)showTabBar:(UITabBarController *) tabbarcontroller;


#pragma mark - NavigatonController
- (id)getViewControllerWithNibName:(NSString *)nibName;
- (void)pushViewControllerWithNibName:(NSString *)nibName;


#pragma mark - GetView
+ (id)getViewFormNibName:(NSString *)nibname index:(NSInteger)index;

@end

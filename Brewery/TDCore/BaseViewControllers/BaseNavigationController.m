//
//  BaseNavigatorController.m
//  HealthCare
//
//  Created by Pham Minh Len on 6/4/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{   
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

+ (BaseNavigationController*)createFromNib
{
    return [[BaseNavigationController alloc] init];
}

- (void)setRootViewController:(UIViewController*)rootViewController
{
    [self setViewControllers:[NSArray arrayWithObject:rootViewController]];
}

- (BaseNavigationBar*)baseNavigationBar
{
    return (id)self.navigationBar;
}

@end

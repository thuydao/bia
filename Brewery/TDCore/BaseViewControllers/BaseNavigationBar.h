//
//  BaseNavigationBar.h
//  BE
//
//  Created by Thuy Dao on 9/21/12.
//  Copyright (c) 2012 Qsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationBar : UINavigationBar

@property (retain) UIColor *useBackgroundColor;
@property (retain) UIImage *useBackgroundImage;

- (void)applyDropShadow;

@end

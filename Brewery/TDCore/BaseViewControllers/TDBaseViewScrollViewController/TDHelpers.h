//
//  TDHelpers.h
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDHelpers : NSObject

+ (NSString *)clearText:(NSString *)text;

@end

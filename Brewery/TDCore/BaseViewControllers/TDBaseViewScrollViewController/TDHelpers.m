//
//  TDHelpers.m
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import "TDHelpers.h"

@implementation TDHelpers

+ (NSString *)clearText:(NSString *)text
{
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return text;
}

@end

//
//  TDView.m
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import "TDView.h"

@implementation TDView

- (id)initView:(NSString *)nibName index:(NSInteger)index
{
    if (self = [super init])
    {
        nibName = [nibName stringByReplacingOccurrencesOfString:@".xib" withString:@""];
        
        NSArray *arr =  [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        if (arr.count > index) {
            self = [arr objectAtIndex:index];
        }
    }
    return self;
}



- (id)initDictionary:(NSDictionary *)dict
{
    if (self = [super init])
    {
        @try {
            NSString *nibName = [[dict allKeys] objectAtIndex:0];
            NSInteger index = [[dict objectForKey:nibName] integerValue];
            
            nibName = [nibName stringByReplacingOccurrencesOfString:@".xib" withString:@""];
            
            NSArray *arr =  [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
            if (arr.count > index) {
                self = [arr objectAtIndex:index];
            }
            
        }
        @catch (NSException *exception) {
            NSLog(@"==================================%@=====================================================",exception);
        }
    }
    return self;
}

+ (UIView *)getViewFromDictionary:(NSDictionary *)dict
{
    @try {
        NSString *nibName = [[dict allKeys] objectAtIndex:0];
        NSInteger index = [[dict objectForKey:nibName] integerValue];
        
        nibName = [nibName stringByReplacingOccurrencesOfString:@".xib" withString:@""];
        
        NSArray *arr =  [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        if (arr.count > index) {
            return [arr objectAtIndex:index];
        }
        return nil;
    }
    @catch (NSException *exception) {
        NSLog(@"==================================%@=====================================================",exception);
    }

}

+ (NSString *)generateCode:(NSDictionary *)dict
{
    UIView *allView;
    
    @try {
        NSString *nibName = [[dict allKeys] objectAtIndex:0];
        NSInteger index = [[dict objectForKey:nibName] integerValue];
        
        nibName = [nibName stringByReplacingOccurrencesOfString:@".xib" withString:@""];
        
        NSArray *arr =  [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        if (arr.count > index) {
             allView = [arr objectAtIndex:index];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"==================================%@=====================================================",exception);
    }

    NSLog(@"cancel view if tag = 0");
    NSLog(@"register name same as label name");
    
    NSString *code = @"";
    
    for (UIView* view in allView.subviews) {
        if (view.tag != 0) {
            code = [NSString stringWithFormat:@"%@ %@",code, [self codeLineWithView:view]];
        }
        else
        {
            NSLog(@"cancel view if tag = 0");
        }
    }
    
    return code;
}



+ (NSString *)codeLineWithView:(UIView *)view
{
    NSString *content = @"";

    
    if ([view isKindOfClass:[UIImageView class]]) {
        
        NSString *name = [NSString stringWithFormat:@"imv%d",view.tag];
        
        content = [NSString stringWithFormat:@"\t UIImageView *%@ = [view imageViewWithTag:%d];\n\
                   [%@ setBackgroundColor:[UIColor clearColor]];\n\
                   //[%@ setFrame:<#(CGRect)#>];\n\
                   [%@ setImage:[UIImage imageNamed:@\"\"]];\n\n",name,view.tag,name,name,name];
    }
    
    else if ([view isKindOfClass:[UIButton class]]) {
        
        NSString *name = [NSString stringWithFormat:@"btn%d",view.tag];

        //[btn0 setBackgroundColor:<#(UIColor *)#>];
        //[btn0 setBackgroundImage:<#(UIImage *)#> forState:UIControlStateNormal];
        //[btn0 setFrame:<#(CGRect)#>];
        //[btn0 setSelected:NO];
        //[btn0 addTarget:(id)self action:@selector(<#selector#>) forControlEvents:UIControlEventTouchUpInside];
        
        
        content = [NSString stringWithFormat:@"\tUIButton *%@ = [view buttonWithTag:%d];\n\
                   [%@ setBackgroundColor:[UIColor clearColor]];\n\
                   [%@ setBackgroundImage:[UIImage imageNamed:@\"\"] forState:UIControlStateNormal];\n\
                   //[%@ setFrame:<#(CGRect)#>];\n\
                   [%@ setSelected:NO];\n\
                   //[%@ addTarget:(id)self action:@selector(<#selector#>) forControlEvents:UIControlEventTouchUpInside];\n\n",name,view.tag,name,name,name,name,name];
    }
    
    else if ([view isKindOfClass:[UILabel class]]) {
        
        NSString *name = [NSString stringWithFormat:@"lbl%d",view.tag];

        content = [NSString stringWithFormat:@"\tUILabel *%@ = [view labelWithTag:%d];\n\
                           [%@ setBackgroundColor:[UIColor clearColor]];\n\
                           [%@ setText:@\"\"];\n\
                           [%@ setTextAlignment:NSTextAlignmentLeft];\n\
                           [%@ setTextColor:[UIColor blackColor]];\n\
                   //        [%@ setFont:<#(UIFont *)#>];\n\
                   ",name,view.tag,name,name,name,name,name];
    }
    else if ([view isKindOfClass:[UISwitch class]]) {
        content = [NSString stringWithFormat:@""];
    }
    
    else if ([view isKindOfClass:[UITextField class]]) {
        content = [NSString stringWithFormat:@""];
    }
    
    else if ([view isKindOfClass:[UITextView class]]) {
        content = [NSString stringWithFormat:@""];
    }
    
    else if ([view isKindOfClass:[UITableView class]]) {
        content = [NSString stringWithFormat:@""];
    }
    
    else if ([view isKindOfClass:[UIScrollView class]]) {
        content = [NSString stringWithFormat:@""];
    }
    
    else if ([view isKindOfClass:[UIWebView class]]) {
        content = [NSString stringWithFormat:@""];
    }
    
    else
    {
        //other view
        content = [NSString stringWithFormat:@""];
    }
    

    return  content;
}

@end

//
//  TDGenerateSource.h
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDView.h"

@interface NSString (core)

- (NSString *)add:(NSString *)aStr;

@end

@implementation NSString (core)

- (NSString *)add:(NSString *)aStr
{
    return [NSString stringWithFormat:@"%@ \n %@",self,aStr];
}

@end

@interface TDGenerateSource : NSObject


+ (void)newViewControllerWithDictionary:(NSDictionary *)dict;

@end

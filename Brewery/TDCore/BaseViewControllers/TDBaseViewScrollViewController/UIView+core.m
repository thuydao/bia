//
//  UIView+core.m
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import "UIView+core.h"

@implementation UIView (core)


- (UIImageView *)imageViewWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UIButton *)buttonWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UIWebView *)webViewWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UILabel *)labelWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UISwitch *)switchWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UITableView *)tableViewWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UIScrollView *)scrollViewWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UITextField *)textFieldWithTag:(NSInteger )tag
{
    return (id )[self viewWithTag:tag];
}

@end

//
//  UIView+core.h
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (core)

- (UIImageView *)imageViewWithTag:(NSInteger)tag;
- (UIButton *)buttonWithTag:(NSInteger)tag;
- (UIWebView *)webViewWithTag:(NSInteger)tag;
- (UILabel *)labelWithTag:(NSInteger)tag;
- (UISwitch *)switchWithTag:(NSInteger)tag;
- (UITableView *)tableViewWithTag:(NSInteger)tag;
- (UIScrollView *)scrollViewWithTag:(NSInteger)tag;
- (UITextField *)textFieldWithTag:(NSInteger )tag;

@end

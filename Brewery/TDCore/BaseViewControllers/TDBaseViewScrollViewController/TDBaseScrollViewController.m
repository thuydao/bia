//
//  TDBaseScrollViewController.m
//  HealthCare
//
//  Created by Dao Duy Thuy on 6/26/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "TDBaseScrollViewController.h"

@interface TDBaseScrollViewController ()

@end

@implementation TDBaseScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Untils

- (NSArray *)getArrayViewFormNibName:(NSString *)nibName
{
    nibName = [nibName stringByReplacingOccurrencesOfString:@".xib" withString:@""];
    
    return [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
}

- (id)getViewFormArrayControl:(NSMutableArray *)arrControl andWithIndex:(NSInteger)index
{
    return [arrControl objectAtIndex:index] ? [arrControl objectAtIndex:index] : nil;
}

- (id)getViewFormNibName:(NSString *)nibname index:(NSInteger)index
{
    return  [[self getArrayViewFormNibName:nibname] objectAtIndex:index] ? [[self getArrayViewFormNibName:nibname] objectAtIndex:index] : nil;
}

- (void)removeAllSubViews
{
    for (UIView *view in self.scrollView.subviews)
    {
        [view removeFromSuperview];
    }
}

#pragma mark - Data Sources

- (void)useCustomXibFileName:(NSString *)nibName
{
    if (self.allViews) [self.allViews removeAllObjects];
    else self.allViews = [NSMutableArray new];
    
    [self.allViews addObjectsFromArray:[self getArrayViewFormNibName:nibName]];
}

- (void)useCustomXibFileName:(NSString *)nibName andIndexs:(NSArray *)indexs
{
    if (indexs) {
        
        if (self.allViews) [self.allViews removeAllObjects];
        else self.allViews = [NSMutableArray new];
        
        for (NSInteger i = 0; i < indexs.count; i++) {
            
            NSInteger index = [[indexs objectAtIndex:i] integerValue];
            
            UIView *subview = [self getViewFormNibName:nibName index:index];
            [self.allViews addObject:subview];
        }
    }
    else NSLog(@"indexs nil.");
}

- (void)useViews:(NSArray *)views
{
    if (views) {
        if (self.allViews) [self.allViews removeAllObjects];
        else self.allViews = [NSMutableArray new];
        
        [self.allViews addObjectsFromArray:views];
        
    }
    else NSLog(@"views nil.");
}

- (void)useTDViewArray:(NSArray *)array
{
    if ( array )
    {
        if ( self.allViews ) [self.allViews removeAllObjects];
        else self.allViews = [NSMutableArray new];
        
        for (NSDictionary *dict in array)
        {
            UIView *subview = [TDView getViewFromDictionary:dict];
            [self.allViews addObject:subview];
        }
    }
    else NSLog(@"array nil");
}

- (void)setHeightView:(CGFloat)height atIndex:(NSInteger)index
{
    [UIView animateWithDuration:0.4f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         CGFloat y = 0;
                             UIView *lassView = [self.allViews objectAtIndex:index];
                             y = lassView.frame.origin.y;
                         
                         
                         for (NSInteger i = index; i < [self.allViews count]; i++) {
                             UIView *viewtmp = [self.allViews objectAtIndex:i];
                             if (viewtmp) {
                                 if (i == index) {
                                     viewtmp.frame = CGRectMake(0, y, 320, height);
                                     y = y + height;
                                     
                                 }
                                 else
                                 {
                                     viewtmp.frame = CGRectMake(0, y, 320, viewtmp.frame.size.height);
                                     y = y + viewtmp.frame.size.height;
                                 }
                                 
                             }
                         }
                     }
                     completion:^(BOOL finished){
                         UIView *viewtmp = [self.allViews objectAtIndex:index];
                         if (height == 0) {
                             viewtmp.hidden = YES;
                         }
                         else viewtmp.hidden = NO;
                     }
     ];
}

#pragma mark - controll

- (void)drawUI
{
    [self removeAllSubViews];
    
    UIView *view = [[UIView alloc] init];
    view.tag = VIEW_TAG;
    CGFloat y = 0;
    
    //draw rows
    for (NSInteger i = 0; i < [self.allViews count]; i++) {
        UIView *viewtmp = [self.allViews objectAtIndex:i];
        if (viewtmp) {
            
            viewtmp.frame = CGRectMake(0, y, 320, viewtmp.frame.size.height);
            y = y + viewtmp.frame.size.height;
            
            [view addSubview:viewtmp];
            if (self.canClickRow) {
                viewtmp.userInteractionEnabled = YES;
                [viewtmp addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:(id)self action:@selector(clickRow:)]];
            }
        }
    }
    [view setFrame:CGRectMake(0, 0, self.view.frame.size.width, y)];
    
    [self.scrollView addSubview:view];
    [self.scrollView setContentSize:CGSizeMake(320, y)];
    
    [self.view addSubview:self.scrollView];
}

- (void)reloadData
{
    for (NSInteger i = 0; i < [self.allViews count]; i++) {
    
        UIView *viewtmp = [self.allViews objectAtIndex:i];
        [self scrollView:self  reloadView:viewtmp atIndex:i];
    }
}


#pragma mark - acttion

- (void)clickRow:(UIGestureRecognizer *)sender 
{
    UIView *view = sender.view;
    NSInteger index = [self.allViews indexOfObject:view];
    [self scrollView:(id)self DidSelectedView:view atIndex:index];
}

#pragma mark - overWrite

- (void)scrollView:(TDBaseScrollViewController*)scrollView reloadView:(UIView*)view atIndex:(NSInteger)index
{
    
}

- (void)scrollView:(TDBaseScrollViewController *)scrollView DidSelectedView:(UIView *)view atIndex:(NSInteger)index
{
    
}

@end

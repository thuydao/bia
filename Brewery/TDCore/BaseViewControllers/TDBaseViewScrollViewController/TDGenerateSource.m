//
//  TDGenerateSource.m
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import "TDGenerateSource.h"

@implementation TDGenerateSource

+ (void)newViewControllerWithDictionary:(NSDictionary *)dict
{
    
    NSString *name = [dict objectForKey:@"name"];
    NSString *path = [dict objectForKey:@"path"];
    NSString *title = [dict objectForKey:@"title"];
    NSArray *array = [dict objectForKey:@"array"];
    
    path = [NSString stringWithFormat:@"%@/%@",path,name];
    //TODO: header file
    
    NSString *hcontent = @"";
    NSString *mcontent = @"";
    
    //@interface
    hcontent = [NSString stringWithFormat:@"\n\n\n\n\n#import <UIKit/UIKit.h>\n\
                #import \"TDBaseScrollViewController.h\"\n\
                #import \"TDView.h\"\n\
                #import \"UIView+core.h\"\n\
                \n\n@interface %@ : TDBaseScrollViewController \n {\n}\n",name];
    
    hcontent = [hcontent add:@"@property (nonatomic, retain) NSArray *dataSource;\n"];
    
    //@end
    hcontent = [NSString stringWithFormat:@"%@ @end",hcontent];
    
    
    [self writeFile:[NSString stringWithFormat:@"%@.h",path] content:hcontent];
    
    
    //TODO: impliment file
    mcontent = [NSString stringWithFormat:@"\n\n\n\n\n#import \"%@.h\" \n\n\
                @implementation %@\n\n\
                ",name,name];
    
    
    
    
    //TODO: View DidLoad
    
    mcontent = [NSString stringWithFormat:@"%@ - (void)viewDidLoad \n\
                { \n\
                [super viewDidLoad]; \n\
                self.title = NSLocalizedString(@\"%@\", nil);\n\
                //setup view \n\
                %@ \n\
                [self useTDViewArray:array]; \n\
                [self drawUI];\n\
                [self reloadData];\n\
                } \n\n\
                ",mcontent,title, [self stringFromArray:array]];
    
    //TODO: View WillApper
    
    mcontent = [mcontent add:@"\n- (void)viewWillAppear:(BOOL)animated\n\
                {\n\
                [super viewWillAppear:animated];\n\
                }\n\n"];
    
    
    //TODO: scrollView: reloadView: atIndex:
    
    mcontent = [mcontent add:@"\n- (void)scrollView:(TDBaseScrollViewController*)scrollView reloadView:(UIView*)view atIndex:(NSInteger)index {\n"];
    
    //Setup reload view
    mcontent = [mcontent add:@"switch (index) { \n"];///
    for (NSInteger i = 0; i < array.count; i++) {
        
        
        NSString *codetemp = [TDView  generateCode:[array objectAtIndex:i]];
        
        mcontent = [NSString stringWithFormat:@" %@ case %d: {\n\
                    %@\n } \n\
                    break;\n",mcontent, i,codetemp];
    }
    
    mcontent = [mcontent add:@"default:\n\
                break;\n\
                }"];
    
    mcontent=  [mcontent add:@"}\n"];
    
    
    mcontent = [NSString stringWithFormat:@"%@ \n\n @end \n",mcontent];
    
    
    [self writeFile:[NSString stringWithFormat:@"%@.m",path] content:mcontent];
}

#pragma mark - Utils

+ (void)writeFile:(NSString *)path content:(NSString *)content
{
    [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
    [content writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"\n\n\n\n\n =================================\n\n\n save: %@ success", path);
}

+ (NSString *)stringFromArray:(NSArray *)arr
{
    @try {
        NSString *content = @"NSArray *array = [[NSArray alloc] initWithObjects:";
        
        for (NSDictionary *dict in arr) {
            NSString *nibName = [[dict allKeys] objectAtIndex:0];
            NSString *index = [dict objectForKey:nibName];
            
            
            nibName = [NSString stringWithFormat:@"@\"%@\"",nibName];
            index = [NSString stringWithFormat:@"@\"%@\"",index];
            
            content = [NSString stringWithFormat:@"%@@{%@:%@},",content,nibName,index];
        }
        
        content = [NSString stringWithFormat:@"%@nil];",content];
        
        return content;
    }
    @catch (NSException *exception) {
        NSLog(@"==================================%@=====================================================",exception);
    }
}

@end

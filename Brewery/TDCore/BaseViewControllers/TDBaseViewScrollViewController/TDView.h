//
//  TDView.h
//  QsoftBase
//
//  Created by Dao Duy Thuy on 7/2/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TDView : UIView

@property (nonatomic, retain) NSMutableDictionary *modelForView;

- (id)initView:(NSString *)nibName index:(NSInteger)index;

/**
 *  getView FromDictionary
 *
 *  @param dict @{index,nibName}
 *
 *  @return view
 */
- (id)initDictionary:(NSDictionary *)dict;

/**
 *  generateUIControl
 *
 *  @return sourceCode
 */
+ (NSString *)generateCode:(NSDictionary *)dict;

+ (UIView *)getViewFromDictionary:(NSDictionary *)dict;

@end

//
//  TDBaseScrollViewController.h
//  HealthCare
//
//  Created by Dao Duy Thuy on 6/26/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TDView.h"
#import "UIView+core.h"

#define VIEW_TAG 14564

@interface TDBaseScrollViewController : BaseViewController

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) NSMutableArray *allViews;
@property (nonatomic, assign) BOOL canClickRow;

#pragma mark - Until

- (id)getViewFormNibName:(NSString *)nibname index:(NSInteger)index;

#pragma mark - Data Sources

- (void)useCustomXibFileName:(NSString *)nibName;
- (void)useCustomXibFileName:(NSString *)nibName andIndexs:(NSArray *)indexs;
- (void)useTDViewArray:(NSArray *)array;

- (void)setHeightView:(CGFloat)height atIndex:(NSInteger)index;

#pragma mark - Controll
- (void)drawUI;
- (void)reloadData;


#pragma mark - OverWrite
- (void)scrollView:(TDBaseScrollViewController*)scrollView reloadView:(UIView*)view atIndex:(NSInteger)index;
- (void)scrollView:(TDBaseScrollViewController *)scrollView DidSelectedView:(UIView *)view atIndex:(NSInteger)index;




@end

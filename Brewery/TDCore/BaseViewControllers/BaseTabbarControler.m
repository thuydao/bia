//
//  BaseTabbar.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "BaseTabbarControler.h"
#import "UIView+block.h"

#define BUTTON_TAG 9685


@implementation BaseTabbarControler


//===================================== tabbarFromViewControllers ==========================================

+ (UITabBarController *)tabbarFromViewControllers:(NSArray *)viewControllers barItems:(NSArray *)barItems
{
    UITabBarController *tabbarVC = [[UITabBarController alloc] init];
    
    NSMutableArray *arr = [NSMutableArray new];
    
    //settabbar item & add NavigationController
    for (NSInteger i = 0 ; i < viewControllers.count ; i ++) {
        
        UIViewController *vc =  [viewControllers objectAtIndex:i];
        UITabBarItem *tabbarItem = [barItems objectAtIndex:i];
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.tabBarItem = tabbarItem;
        [arr addObject:nav];
    }
    
    //set viewcontroller to TabbarController
    [tabbarVC setViewControllers:arr];
    
    //  sett tabbar custom image
    if (arr.count == 2) {
        UIImage *tabBarSelectedImage = [[UIImage imageNamed:@"bg_tabbar_selected2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];

         [[UITabBar appearance] setSelectionIndicatorImage:tabBarSelectedImage];
    }
    
    if (arr.count == 3) {
         [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"bg_tabbar_selected3.png"]];
    }
   
    
    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed:@"bg_tabbar.png"];
    
    
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    
    //set white text
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],
                                                            NSForegroundColorAttributeName : [UIColor whiteColor]
                                                            } forState:UIControlStateSelected];
    }
    else {
        
    }

    return tabbarVC;
}

//===================================== tabbarFromViewControllers ==========================================

+ (UITabBarController *)tabbarFromViewControllers:(NSArray *)viewControllers imageNomal:(NSArray *)imageNomal imageSelected:(NSArray *)imageSelected
{
    UITabBarController *tabbarVC = [[UITabBarController alloc] init];
    
    NSMutableArray *arr = [NSMutableArray new];
    
    //height TabbarItem
    CGFloat heightTabbarItem = 320 /viewControllers.count + 1;
    
    //settabbar item & add NavigationController
    for (NSInteger i = 0 ; i < viewControllers.count ; i ++) {
        
        UIViewController *vc =  [viewControllers objectAtIndex:i];
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [arr addObject:nav];
        
        //add button display background
        UIButton *btnCustom = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCustom setFrame:CGRectMake(heightTabbarItem * i, tabbarVC.view.frame.size.height - 50, heightTabbarItem, 50)];
        [btnCustom setBackgroundColor:[UIColor clearColor]];
        btnCustom.tag = BUTTON_TAG + i;
        
        //custom selected
        [btnCustom setBackgroundImage:[UIImage imageNamed:[imageSelected objectAtIndex:i]] forState:UIControlStateSelected];
        [btnCustom setBackgroundImage:[UIImage imageNamed:[imageNomal objectAtIndex:i]] forState:UIControlStateNormal];
        
        [tabbarVC.view addSubview:btnCustom];
        
        //add target for button
        
        [btnCustom handleTapBlock:^(UITapGestureRecognizer *sender) {
            for (NSInteger ii = BUTTON_TAG; ii < BUTTON_TAG + viewControllers.count; ii ++) {
                UIButton *btn = [tabbarVC.view buttonWithTag:ii];
                [btn setSelected:NO];
            }
            ((UIButton *)sender.view).selected = YES;
            tabbarVC.selectedIndex = i;
        }];
        
    }
    
    //set viewcontroller to TabbarController
    [tabbarVC setViewControllers:arr];
    
    return tabbarVC;
}

//===================================== setSelected ==========================================

+ (void)setSelected:(UITabBarController *)tabbarVC indexSelected:(NSInteger)indexSelected
{
    for (NSInteger ii = BUTTON_TAG; ii < BUTTON_TAG + tabbarVC.viewControllers.count; ii ++) {
        UIButton *btn = [tabbarVC.view buttonWithTag:ii];
        [btn setSelected:NO];
    }

    [tabbarVC.view buttonWithTag:(BUTTON_TAG + indexSelected)].selected = YES;
}

//===================================== initTabBarItemWithTitle: imageName ==========================================

+ (UITabBarItem *)initTabBarItemWithTitle:(NSString *)title imageName:(NSString *)imageName
{
    //init tabbarItem
    UITabBarItem *tabbarItem = [[UITabBarItem alloc] init];
    //set title
    tabbarItem.title = title;
    //set imate
    tabbarItem.image = [UIImage imageNamed:imageName];
    
    return tabbarItem;
}

@end

//
//  BaseTabbar.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/7/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseTabbarControler : NSObject


//===================================== tabbarFromViewControllers ==========================================

+ (UITabBarController *)tabbarFromViewControllers:(NSArray *)viewControllers barItems:(NSArray *)barItems;


//===================================== tabbarFromViewControllers ==========================================

+ (UITabBarController *)tabbarFromViewControllers:(NSArray *)viewControllers imageNomal:(NSArray *)imageNomal imageSelected:(NSArray *)imageSelected;

//===================================== setSelected ==========================================

+ (void)setSelected:(UITabBarController *)tabbarVC indexSelected:(NSInteger)indexSelected;

//===================================== initTabBarItemWithTitle: imageName ==========================================

+ (UITabBarItem *)initTabBarItemWithTitle:(NSString *)title imageName:(NSString *)imageName;

@end

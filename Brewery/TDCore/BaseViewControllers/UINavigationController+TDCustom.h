//
//  UINavigationController+TDCustom.h
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/11/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseNavigationBar.h"
#import <objc/runtime.h>

@class BaseNavigationBar;

@interface UINavigationController (TDCustom)

@property (nonatomic, retain) BaseNavigationBar *baseNavigationBar;
@property (nonatomic, retain) UIView *navigationBarViewCustom;

#pragma mark - Untils

- (void)setRootViewController:(UIViewController*)rootViewController;
- (void)hiddenNavigationBarCustom;
#pragma mark - PushToView

- (void)pushViewWithStoryBoardID:(NSString *)identify;

@end

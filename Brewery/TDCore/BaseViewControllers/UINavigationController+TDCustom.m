//
//  UINavigationController+TDCustom.m
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/11/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "UINavigationController+TDCustom.h"

static const void *BaseNavigationBarVoid             = &BaseNavigationBarVoid;
static const void *NavigationBarViewVoid             = &NavigationBarViewVoid;

@implementation UINavigationController (TDCustom)

#pragma mark - seter & geter

- (BaseNavigationBar *)baseNavigationBar
{
    return (BaseNavigationBar *)objc_getAssociatedObject(self, BaseNavigationBarVoid);
}

- (void)setBaseNavigationBar:(BaseNavigationBar *)baseNavigationBar
{
    objc_setAssociatedObject(self, BaseNavigationBarVoid, baseNavigationBar, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView*)navigationBarViewCustom
{
     return objc_getAssociatedObject(self, NavigationBarViewVoid);
}

- (void)setNavigationBarViewCustom:(UIView *)navigationBarViewCustom
{
    if (self.navigationBarViewCustom) {
        [self hiddenNavigationBarCustom];
    }
    
    objc_setAssociatedObject(self, NavigationBarViewVoid, navigationBarViewCustom, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self.navigationBar addSubview:navigationBarViewCustom];
}


#pragma mark - Untils

- (void)setRootViewController:(UIViewController*)rootViewController
{
    [self setViewControllers:[NSArray arrayWithObject:rootViewController]];
}

- (void)hiddenNavigationBarCustom
{
    if (self.navigationBarViewCustom) {
        [self.navigationBarViewCustom removeFromSuperview];
    }
}

- (void)pushViewWithStoryBoardID:(NSString *)identify
{
    
}

@end

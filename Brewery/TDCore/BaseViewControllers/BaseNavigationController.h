//
//  BaseNavigatorController.h
//  HealthCare
//
//  Created by Thuy Dao on 6/4/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BaseNavigationBar;

@interface BaseNavigationController : UINavigationController
@property (readonly) BaseNavigationBar *baseNavigationBar;

+ (BaseNavigationController*)createFromNib;
- (void)setRootViewController:(UIViewController*)rootViewController;

@end

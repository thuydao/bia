//
//  UIViewController+FB.m
//  HelloFacebookSample
//
//  Created by Dao Duy Thuy on 8/8/14.
//
//

#import "UIViewController+FB.h"

static const void *FBResponseBlockVoid = &FBResponseBlockVoid;

@implementation UIViewController (FB)

#pragma mark - SETTER & GETTER

- (void)setFBResponseBlock:(FBBlock)FBResponseBlock
{
    objc_setAssociatedObject(self, FBResponseBlockVoid, FBResponseBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (FBBlock)FBResponseBlock
{
    return objc_getAssociatedObject(self, FBResponseBlockVoid);
}

- (void)customBtnFacebook:(FBLoginView *)loginview
{
    for (id view in loginview.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn setBackgroundColor:[UIColor clearColor]];
            [btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn setTitle:@"" forState:UIControlStateSelected];
        }
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
    }
}

#pragma mark - Utils

//===================================== addLoginFBButtonWithFrame: FBBlock: ==========================================

- (void)addLoginFBButtonWithFrame:(CGRect)frame FBBlock:(FBBlock)block
{
    //init block
    self.FBResponseBlock = block;
    
    // Create Login View so that the app will be granted "status_update" permission.
    FBLoginView *loginview = [[FBLoginView alloc] init];
    
    //set delegate
    loginview.delegate = (id)self;
    //set frame
    loginview.frame = frame;
    [self customBtnFacebook:loginview];
    //add to view
    [self.view addSubview:loginview];
}

//===================================== addLoginFBButtonWithFrame: permissions:  audience: FBBlock:==========================================

- (void)addLoginFBButtonWithFrame:(CGRect)frame permissions:(NSArray *)permissions audience:(FBSessionDefaultAudience)audience FBBlock:(FBBlock)block
{
    //init block
    self.FBResponseBlock = block;
    
    // Create Login View so that the app will be granted "status_update" permission.
    FBLoginView *loginview = [[FBLoginView alloc] initWithPublishPermissions:permissions defaultAudience:audience];
    
    //set delegate
    loginview.delegate = (id)self;
    //set frame
    loginview.frame = frame;
    //add to view
    [self.view addSubview:loginview];
}

//===================================== FBToken ==========================================

- (NSString *)FBToken
{
    return [[[FBSession activeSession] accessTokenData] accessToken];
}


#pragma mark - FBLoginViewDelegate

//===================================== loginViewShowingLoggedInUser ==========================================

/*!
 @abstract
 Tells the delegate that the view is now in logged in mode
 
 @param loginView   The login view that transitioned its view mode
 */

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    self.FBResponseBlock(LoggedIn, loginView, nil, nil);
}

//===================================== loginViewShowingLoggedOutUser ==========================================

/*!
 @abstract
 Tells the delegate that the view is now in logged out mode
 
 @param loginView   The login view that transitioned its view mode
 */
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    self.FBResponseBlock(LoggedOut, loginView, nil, nil);
}


//===================================== loginView: handleError: ==========================================

/*!
 @abstract
 Tells the delegate that there is a communication or authorization error.
 
 @param loginView           The login view that transitioned its view mode
 @param error               An error object containing details of the error.
 @discussion See https://developers.facebook.com/docs/technical-guides/iossdk/errors/
 for error handling best practices.
 */

- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error
{
    self.FBResponseBlock(Other, loginView, nil, error);
}

//===================================== loginViewFetchedUserInfo: user: ==========================================

/*!
 @abstract
 Tells the delegate that the view is has now fetched user info
 
 @param loginView   The login view that transitioned its view mode
 
 @param user        The user info object describing the logged in user
 */

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
     self.FBResponseBlock(Other, loginView, user, nil);
}

@end

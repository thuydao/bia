//
//  UIViewController+FB.h
//  HelloFacebookSample
//
//  Created by Dao Duy Thuy on 8/8/14.
//
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <objc/runtime.h>

typedef enum : NSUInteger {
    LoggedIn = 0,
    LoggedOut = 1,
    Other = 2
} LogType;

typedef void (^FBBlock) (LogType type, FBLoginView *loginView, id<FBGraphUser> user, NSError *err);

@interface UIViewController (FB) <FBLoginViewDelegate>


@property (copy, nonatomic) FBBlock FBResponseBlock;

//===================================== addLoginFBButtonWithFrame:FBBlock: ==========================================

- (void)addLoginFBButtonWithFrame:(CGRect)frame FBBlock:(FBBlock)block;

//===================================== addLoginFBButtonWithFrame:permissions:audience:FBBlock:==========================================

- (void)addLoginFBButtonWithFrame:(CGRect)frame permissions:(NSArray *)permissions audience:(FBSessionDefaultAudience)audience FBBlock:(FBBlock)block;

//===================================== FBToken ==========================================

- (NSString *)FBToken;



@end

//
//  UIViewController+Twitter.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/12/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "UIViewController+Twitter.h"

static const void *twitterVoid = &twitterVoid;

@implementation UIViewController (Twitter)


#pragma mark - SETTER & GETTER

- (STTwitterAPI *)twitter
{
//    if (!self.twitter) {
//        STTwitterAPI *ttwitter = [STTwitterAPI twitterAPIOSWithFirstAccount];
//        objc_setAssociatedObject(self, twitterVoid, ttwitter, OBJC_ASSOCIATION_COPY_NONATOMIC);
//    }
    return objc_getAssociatedObject(self, twitterVoid);
}

- (void)setTwitter:(STTwitterAPI *)twitter
{
    objc_setAssociatedObject(self, twitterVoid, twitter, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


//===================================== signInTwitter ==========================================

- (void)signInTwitter
{
    self.twitter = [STTwitterAPI twitterAPIOSWithFirstAccount];
    
    
    [self.twitter verifyCredentialsWithSuccessBlock:^(NSString *username) {
        
        NSLog(@"%@",[self.twitter bearerToken]);
        
    } errorBlock:^(NSError *error) {
         NSLog(@"%@",error);
        NSDictionary *dict = error.userInfo;
        if ([[dict objectForKey:@"NSLocalizedDescription"] containsString:@"This system cannot access Twitter"]) {
            [self loginViaSafari];
        }
        
    }];

}

- (void)loginViaSafari
{
    self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:k_consumerKey
                                                 consumerSecret:k_consumerSecret];
    
    
    [self.twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
        NSLog(@"-- url: %@", url);
        NSLog(@"-- oauthToken: %@", oauthToken);
        
        [[UIApplication sharedApplication] openURL:url];
    } authenticateInsteadOfAuthorize:NO
                    forceLogin:@(YES)
                    screenName:nil
                 oauthCallback:@"myapp://twitter_access_tokens/"
                    errorBlock:^(NSError *error) {
                        NSLog(@"-- error: %@", error);
                    }];

}

@end

//
//  UIViewController+Google.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/11/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "UIViewController+Google.h"

static const void *GGResponseBlockVoid = &GGResponseBlockVoid;

@implementation UIViewController (Google)


#pragma mark - SETTER & GETTER

- (void)setGGResponseBlock:(GGBlock)GGResponseBlock
{
    objc_setAssociatedObject(self, GGResponseBlockVoid, GGResponseBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (GGBlock)GGResponseBlock
{
    return objc_getAssociatedObject(self, GGResponseBlockVoid);
}


#pragma mark - Utils


- (void)addButtonGGLogin:(CGRect)frame
{
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = (id)self;
    
    [signIn trySilentAuthentication];
    
    GPPSignInButton *signInButton1 = [[GPPSignInButton alloc] init];
    signInButton1.style = kGPPSignInButtonStyleWide;
    signInButton1.frame = CGRectMake(100, 100, 10000, 10000);
    [signInButton1 setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:signInButton1];

}

//===================================== signInGoogle ==========================================

- (void)signInGoogle:(GGBlock)block
{
    self.GGResponseBlock = block;
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.delegate = self;
    signIn.shouldFetchGoogleUserEmail = YES;
    signIn.clientID = kClientID;
    signIn.scopes = [NSArray arrayWithObjects:kGTLAuthScopePlusLogin,nil];
    signIn.actions = [NSArray arrayWithObjects:@"http://schemas.google.com/ListenActivity",nil];
    [signIn authenticate];
}

#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    self.GGResponseBlock (auth,error);
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)disconnect {
    [[GPPSignIn sharedInstance] disconnect];
}

- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
    }
}

// Update the interface elements containing user data to reflect the
// currently signed in user.
- (void)refreshUserInfo {
    //  if ([GPPSignIn sharedInstance].authentication == nil) {
    //    self.userName.text = kPlaceholderUserName;
    //    self.userEmailAddress.text = kPlaceholderEmailAddress;
    //    self.userAvatar.image = [UIImage imageNamed:kPlaceholderAvatarImageName];
    //    return;
    //  }
    
    //self.userEmailAddress.text = [GPPSignIn sharedInstance].userEmail;
    
    // The googlePlusUser member will be populated only if the appropriate
    // scope is set when signing in.
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (person == nil) {
        return;
    }
    
   // self.userName.text = person.displayName;
    
    // Load avatar image asynchronously, in background
    dispatch_queue_t backgroundQueue =
    dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(backgroundQueue, ^{
        NSData *avatarData = nil;
        NSString *imageURLString = person.image.url;
        if (imageURLString) {
            NSURL *imageURL = [NSURL URLWithString:imageURLString];
            avatarData = [NSData dataWithContentsOfURL:imageURL];
        }
        
        if (avatarData) {
            // Update UI from the main thread when available
            dispatch_async(dispatch_get_main_queue(), ^{
               // self.userAvatar.image = [UIImage imageWithData:avatarData];
            });
        }
    });
}



@end

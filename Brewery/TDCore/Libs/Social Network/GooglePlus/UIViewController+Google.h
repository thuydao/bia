//
//  UIViewController+Google.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/11/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <QuartzCore/QuartzCore.h>

@class GPPSignInButton;

typedef void (^GGBlock) (GTMOAuth2Authentication *auth, NSError *err);

@interface UIViewController (Google) <GPPSignInDelegate>

@property (copy, nonatomic) GGBlock GGResponseBlock;

- (void)addButtonGGLogin:(CGRect)frame;

//===================================== signInGoogle ==========================================
- (void)signInGoogle:(GGBlock)block;

@end

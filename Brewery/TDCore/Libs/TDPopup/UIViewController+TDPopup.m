//
//  UIViewController+TDPopup.m
//  All
//
//  Created by Dao Duy Thuy on 7/5/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#import "UIViewController+TDPopup.h"
#import <objc/runtime.h>

static const void *UIVCpopupView = &UIVCpopupView;
static const void *UIisShowPopup = &UIisShowPopup;
static const void *UItransView   = &UItransView;
static const void *UITapGestureRecognizerBlockVoid = &UITapGestureRecognizerBlockVoid;

@implementation UIViewController (TDPopup)

- (UIView *)transView
{
    UIView *value = objc_getAssociatedObject(self, UItransView);
    if (!value)
    {
        value = [[UIView alloc] init];
        objc_setAssociatedObject(self, UItransView, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return value;
}

- (void)setTransView:(UIView *)transView
{
    objc_setAssociatedObject(self, UItransView, transView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)popupView
{
    UIView *value = objc_getAssociatedObject(self, UIVCpopupView);
    if (!value)
    {
        value = [[UIView alloc] init];
        objc_setAssociatedObject(self, UIVCpopupView, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }

    return value;
}

- (UITapGestureRecognizerBlock)tapGestureRecognizerBlock
{
    return objc_getAssociatedObject(self, UITapGestureRecognizerBlockVoid);
}

- (void)setTapGestureRecognizerBlock:(UITapGestureRecognizerBlock)tapGestureRecognizerBlock
{
    objc_setAssociatedObject(self, UITapGestureRecognizerBlockVoid, tapGestureRecognizerBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)setPopupView:(UIView *)popupView
{
    if ( self.popupView )
    {
        if ( ![NSStringFromClass([self.popupView class]) isEqualToString:NSStringFromClass([popupView class])] )
        {
            [self hidePopup];
        }
    }
    
    if ( [self.popupView isEqual:popupView] )
    {
        [self.view addSubview:self.transView];
        [self.view addSubview:self.popupView];
        
        return;
    }
    
    objc_setAssociatedObject(self, UIVCpopupView, popupView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    popupView.frame = CGRectMake(0, self.view.frame.size.height, popupView.frame.size.width, popupView.frame.size.height);
    self.transView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.transView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
    self.transView.alpha = 0.0;
    [self.view addSubview:self.transView];
    [self.view addSubview:popupView];
    self.isShowPopup = NO;
    
    popUphandle_tap(self.transView, self, @selector(handleSingleTapTDPopup:));
}

- (BOOL)isShowPopup
{
    return [objc_getAssociatedObject(self, UIisShowPopup) boolValue];
}

- (void)setIsShowPopup:(BOOL)isShowPopup
{
     objc_setAssociatedObject(self, UIisShowPopup, [NSNumber numberWithBool:isShowPopup], OBJC_ASSOCIATION_ASSIGN);
}

- (void)showPopup:(CGRect)frame tapBackground:(UITapGestureRecognizerBlock)tapBackground
{
    if (self.isShowPopup) {
        return;
    }
    
    self.popupView.frame = CGRectMake(frame.origin.x, self.view.frame.size.height, self.popupView.frame.size.width, self.popupView.frame.size.height);
    
    self.tapGestureRecognizerBlock = tapBackground;
    self.isShowPopup = YES;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.popupView.frame = frame;
    self.transView.alpha = 1.0;
    [UIView commitAnimations];
}

- (void)hidePopup
{
    self.isShowPopup = NO;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.popupView.frame;
    rect.origin.y = self.view.frame.size.height;
    self.popupView.frame = rect;
    self.transView.alpha = 0.0;
    [UIView commitAnimations];
}

- (void)handleSingleTapTDPopup:(UITapGestureRecognizer *)sender
{
    if (self.isShowPopup)
    {
        [self hidePopup];
        
        if (self.tapGestureRecognizerBlock)
        {
            self.tapGestureRecognizerBlock(sender);
        }
    }
}

@end

//
//  UIViewController+TDPopup.h
//  All
//
//  Created by Dao Duy Thuy on 7/5/14.
//  Copyright (c) 2014 Qsoft. All rights reserved.
//

#define popUphandle_tap(view, delegate, selector) do {\
view.userInteractionEnabled = YES;\
[view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:delegate action:selector]];\
} while(0)


typedef void (^UITapGestureRecognizerBlock) (UITapGestureRecognizer *sender);

@interface UIViewController (TDPopup)

@property (retain, nonatomic) UIView* popupView;
@property (assign) BOOL isShowPopup;
@property (retain, nonatomic) UIView* transView;

@property (copy, nonatomic) UITapGestureRecognizerBlock tapGestureRecognizerBlock;

- (void)showPopup:(CGRect)frame tapBackground:(UITapGestureRecognizerBlock)tapBackground;
- (void)hidePopup;

@end

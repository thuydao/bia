//
//  TDLocationManager.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/20/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "TDLocationManager.h"
#import "INTULocationManager.h"

static TDLocationManager *helpers = nil;

@implementation TDLocationManager

+ (id)shared
{
    if (!helpers) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            helpers = [[TDLocationManager alloc] init];
        });
    }
    return helpers;
}

#pragma mark-location
/*
//===================================== getLocationInforCompleteBlock ==========================================
-(void)getLocationInforCompleteBlock:(void(^)(id result, NSError *error))completeBlock
{
    [self getCLLocationCompleteBlock:^(CLLocation *result, NSError *error) {
        if (result!=nil) {
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            
            
            
            [geocoder reverseGeocodeLocation:result
                           completionHandler:^(NSArray *placemarks, NSError *error) {
                               
                               if (error) {
                                   NSLog(@"Geocode failed with error: %@", error);
                                   completeBlock(@{@"location": result},nil);
                                   
                                   return;
                               }
                               
                               if (placemarks && placemarks.count > 0)
                               {
 
                                    CLPlacemark *placemark = placemarks[0];
                                    
                                    NSDictionary *addressDictionary =
                                    placemark.addressDictionary;
                                    
                                    
                                    NSLog(@"%@ ", addressDictionary);
                                    NSString *address = [addressDictionary
                                    objectForKey:(NSString *)kABPersonAddressStreetKey];
                                    NSString *city = [addressDictionary
                                    objectForKey:(NSString *)kABPersonAddressCityKey];
                                    NSString *street = [addressDictionary
                                    objectForKey:(NSString *)kABPersonAddressStreetKey];
                                    
                                    completeBlock(@{@"location": result,
                                    @"address":address,
                                    @"city":city,
                                    @"street":street},nil);
                                    NSLog(@"%@ %@ %@", address,city, street);
 
                               }
                               
                           }];
        }else{
            completeBlock(nil,[NSError errorWithDomain:@"" code:1 userInfo:nil]);
        }
    }];
}
 */

//===================================== getCLLocationCompleteBlock ==========================================
-(void)getCLLocationCompleteBlock:(void(^)(CLLocation *result, NSError *error))completeBlock
{
    
    [self getCLLocationShowMessage:YES CompleteBlock:^(CLLocation *result, NSError *error) {
        completeBlock(result,error);
    }];
}

//===================================== getCLLocationShowMessage ==========================================
-(void)getCLLocationShowMessage:(BOOL)isShow CompleteBlock:(void(^)(CLLocation *result, NSError *error))completeBlock{
    [[INTULocationManager sharedInstance] requestLocationWithDesiredAccuracy:INTULocationAccuracyCity timeout:10 block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        NSString *error=nil;
        if (status == INTULocationStatusSuccess) {
            completeBlock(currentLocation,nil);
        }
        else if (status == INTULocationStatusTimedOut) {
            // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
            
            error= [NSString stringWithFormat:@"Location request timed out. Current Location:\n%@", currentLocation];
        }
        else {
            // An error occurred
            if (status == INTULocationStatusServicesNotDetermined) {
                error = @"User has not responded to the permissions alert.";
            } else if (status == INTULocationStatusServicesDenied) {
                error = @"User has denied this app permissions to access device location.";
            } else if (status == INTULocationStatusServicesRestricted) {
                error = @"User is restricted from using location services by a usage policy.";
            } else if (status == INTULocationStatusServicesDisabled) {
                error = @"Location services are turned off for all apps on this device.";
            } else {
                error = @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)";
            }
        }
        if (error) {
            if (isShow) {
                NSString *title = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error message:title delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            completeBlock(nil,[NSError errorWithDomain:@"" code:status userInfo:@{@"mess": error}]);
        }
    }];
}

@end

//
//  TDLocationManager.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/20/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TDLocationManager : NSObject

+ (id)shared;

//-(void)getLocationInforCompleteBlock:(void(^)(id result, NSError *error))completeBlock;
-(void)getCLLocationCompleteBlock:(void(^)(CLLocation *result, NSError *error))completeBlock;
-(void)getCLLocationShowMessage:(BOOL)isShow CompleteBlock:(void(^)(CLLocation *result, NSError *error))completeBlock;

@end

//
//  UIViewController+WYPopoverController.h
//  Framework
//
//  Created by Bunlv on 7/25/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"
#import "ContentsTableViewController.h"

typedef void (^WYPopoverControllerBlock) (NSString *text);

@interface UIViewController (WYPopoverControllerViaTableView) <WYPopoverControllerDelegate, ContentsTableViewControllerDelegate>

@property (nonatomic, retain) WYPopoverController *wyPopoverController;
@property (nonatomic, retain) ContentsTableViewController *contentsTableViewController;
@property (nonatomic, copy) WYPopoverControllerBlock wyPopoverControllerBlock;

- (void)presentWYPopoverControllerWithArrayData:(NSMutableArray *)arrData withViewPosition:(UIView *)view withMargins:(UIEdgeInsets )margins withSelected:(NSString *)selected chooseData:(WYPopoverControllerBlock )wyPopoverControllerBlock;

@end

//
//  PopupViewController.m
//  Framework
//
//  Created by Bunlv on 7/24/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "ContentsTableViewController.h"

@interface ContentsTableViewController ()

@end

@implementation ContentsTableViewController

@synthesize arrDataSource = _arrDataSource;
@synthesize selectedValue = _selectedValue;
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (id)initWithWidth:(CGFloat )width
{
    self = [super initWithStyle:UITableViewStylePlain];
    
    if ( self )
    {
        widthTableView = width;
        
        self.modalInPopover = NO;
        self.title = nil;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - SVG
#pragma mark + Set
- (void)setArrDataSource:(NSMutableArray *)arrDataSource
{
    if ( !_arrDataSource )
    {
        _arrDataSource = [NSMutableArray new];
    }
    
    [_arrDataSource removeAllObjects];
    [_arrDataSource addObjectsFromArray:arrDataSource];
    
    NSLog(@"width : %.2f", widthTableView);
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        self.preferredContentSize = CGSizeMake(widthTableView, _arrDataSource.count * 44 - 1);
    }
    else
    {
        if ( [self respondsToSelector:@selector(setPreferredContentSize:)] )
        {
            // Current not processes
        }
        else
        {
            self.contentSizeForViewInPopover = CGSizeMake(widthTableView, _arrDataSource.count * 44 - 1);
        }
    }
}

- (void)setSelectedValue:(NSString *)selectedValue
{
    _selectedValue = selectedValue;
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _arrDataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}

//*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"CELL_POP_UP_VIEW_CONTROLLER";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    
    if ( !cell )
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    
    //add line
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(5, cell.frame.size.height - 1, cell.frame.size.width - 10, 0.5)];
    [imv setBackgroundColor:[UIColor lightGrayColor]];
    [cell addSubview:imv];
    
    
    // Configure the cell...
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    // Text
    NSString *value = [_arrDataSource objectAtIndex:indexPath.row];
    [cell.textLabel setText:value];
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    
    //
    if ( [[_selectedValue uppercaseString] isEqualToString:[value uppercaseString]] )
    {
        cell.textLabel.textColor = [UIColor colorWithRed:204.0/255 green:147.0/255 blue:71/255 alpha:1.0];
    }
    else
    {
         [cell.textLabel setTextColor:[UIColor grayColor]];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
//*/

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_delegate contensTableViewController:self withText:[_arrDataSource objectAtIndex:indexPath.row]];
}

@end

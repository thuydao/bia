//
//  PopupViewController.h
//  Framework
//
//  Created by Bunlv on 7/24/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContentsTableViewControllerDelegate;

@interface ContentsTableViewController : UITableViewController
{
    CGFloat widthTableView;
}

@property (nonatomic, retain) NSMutableArray *arrDataSource;
@property (nonatomic, retain) NSString *selectedValue;

@property id<ContentsTableViewControllerDelegate> delegate;

#pragma mark -
- (id)initWithWidth:(CGFloat )width;

#pragma mark -

@end

// Delegate
@protocol ContentsTableViewControllerDelegate <NSObject>

- (void)contensTableViewController:(ContentsTableViewController *)contensTableViewController withText:(NSString *)text;

@end

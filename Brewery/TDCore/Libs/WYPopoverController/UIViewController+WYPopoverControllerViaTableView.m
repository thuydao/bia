//
//  UIViewController+WYPopoverController.m
//  Framework
//
//  Created by Bunlv on 7/25/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "UIViewController+WYPopoverControllerViaTableView.h"
#import <objc/runtime.h>

static const void *wyPopoverControllerVoid = &wyPopoverControllerVoid;
static const void *contentsTableViewControllerVoid = &contentsTableViewControllerVoid;
static const void *wyPopoverControllerBlockVoid = &wyPopoverControllerBlockVoid;

@implementation UIViewController (WYPopoverControllerViaTableView)

#pragma mark - SVG
#pragma mark + Set
- (void)setWyPopoverController:(WYPopoverController *)wyPopoverController
{
    objc_setAssociatedObject(self, wyPopoverControllerVoid, wyPopoverController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setContentsTableViewController:(ContentsTableViewController *)contentsTableViewController
{
    objc_setAssociatedObject(self, contentsTableViewControllerVoid, contentsTableViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setWyPopoverControllerBlock:(WYPopoverControllerBlock)wyPopoverControllerBlock
{
    objc_setAssociatedObject(self, wyPopoverControllerBlockVoid, wyPopoverControllerBlock, OBJC_ASSOCIATION_COPY);
}

#pragma mark + Get
- (WYPopoverController *)wyPopoverController
{
    return objc_getAssociatedObject(self, wyPopoverControllerVoid);
}

- (ContentsTableViewController *)contentsTableViewController
{
    return objc_getAssociatedObject(self, contentsTableViewControllerVoid);
}

- (WYPopoverControllerBlock )wyPopoverControllerBlock
{
    return objc_getAssociatedObject(self, wyPopoverControllerBlockVoid);
}

#pragma mark - Action
- (void)presentWYPopoverControllerWithArrayData:(NSMutableArray *)arrData withViewPosition:(UIView *)view withMargins:(UIEdgeInsets )margins withSelected:(NSString *)selected chooseData:(WYPopoverControllerBlock )wyPopoverControllerBlock
{
    self.wyPopoverControllerBlock = wyPopoverControllerBlock;
    
    if ( !self.contentsTableViewController )
    {
        self.contentsTableViewController = [[ContentsTableViewController alloc] initWithWidth:self.view.frame.size.width];
        
        self.contentsTableViewController.delegate  = (id)self;
        self.contentsTableViewController.arrDataSource = arrData;
    }
    
    self.contentsTableViewController.selectedValue = selected;
    
    if ( !self.wyPopoverController )
    {
        self.wyPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.contentsTableViewController];
        
        self.wyPopoverController.delegate = (id)self;
        self.wyPopoverController.passthroughViews = @[view];
        self.wyPopoverController.popoverLayoutMargins = margins;
        self.wyPopoverController.wantsDefaultContentAppearance = NO;
        
        [self.wyPopoverController presentPopoverFromRect:view.bounds
                                                  inView:view
                                permittedArrowDirections:WYPopoverArrowDirectionAny
                                                animated:YES
                                                 options:WYPopoverAnimationOptionFadeWithScale];
    }
}

- (void)chooseValue:(NSString *)text
{
    NSLog(@"");
    
    self.wyPopoverControllerBlock(text);
}

#pragma mark - Delegate
#pragma mark + PopoverController
- (void)popoverControllerDidPresentPopover:(WYPopoverController *)controller
{
    NSLog(@"popoverControllerDidPresentPopover");
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    self.wyPopoverController.delegate = nil;
    self.wyPopoverController = nil;
    
    self.contentsTableViewController.delegate = nil;
    self.contentsTableViewController = nil;
}

- (BOOL)popoverControllerShouldIgnoreKeyboardBounds:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverController:(WYPopoverController *)popoverController willTranslatePopoverWithYOffset:(float *)value
{
    // keyboard is shown and the popover will be moved up by 163 pixels for example ( *value = 163 )
    *value = 0; // set value to 0 if you want to avoid the popover to be moved
}

#pragma mark + ContentsTableViewControll
- (void)contensTableViewController:(ContentsTableViewController *)contensTableViewController withText:(NSString *)text
{
    self.wyPopoverController.delegate = nil;
    self.wyPopoverController = nil;
    
    self.contentsTableViewController.delegate = nil;
    self.contentsTableViewController = nil;
    
    // Return block
    [self chooseValue:text];
}

@end

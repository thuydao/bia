

#import "CalloutView.h"

static const void *CalloutBlockVoid                   = &CalloutBlockVoid;

@implementation CalloutView

#pragma mark - setget

- (void)setBlock:(CalloutBlock)block
{
    objc_setAssociatedObject(self, CalloutBlockVoid, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CalloutBlock)block
{
   return objc_getAssociatedObject(self, CalloutBlockVoid);
}

- (void)listen:(CalloutBlock)block
{
    self.block = block;
}

- (IBAction)clickCallOut:(id)sender
{
//    QLog(@"");
    self.block(sender);
}

@end

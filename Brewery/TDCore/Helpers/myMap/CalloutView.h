
#import <UIKit/UIKit.h>

typedef void (^CalloutBlock) (id sender);

@interface CalloutView : UIView
@property (weak, nonatomic) IBOutlet UILabel *calloutLabel;
@property (nonatomic, copy) CalloutBlock block;

- (void)listen:(CalloutBlock)block;

- (IBAction)clickCallOut:(id)sender;

@end

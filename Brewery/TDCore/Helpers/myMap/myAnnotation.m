#import "myAnnotation.h"

@implementation myAnnotation

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title {
    
  if ((self = [super init])) {
    self.coordinate =coordinate;
    self.title = title;
  }
  return self;
    
}

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title imageName:(NSString *)imageName
{
    if (self = [super init]) {
        self.coordinate = coordinate;
        self.title = title;
        self.imageName = imageName;
    }
    return self;
}

-(id) initWithObject:(Brewery *)obj
{
    if (self = [super init]) {
        self.obj = obj;
        CLLocationCoordinate2D coordinate1;
        coordinate1.latitude = obj.lat;
        coordinate1.longitude = obj.lon;
        self.coordinate = coordinate1;
        self.title = obj.name;
        self.imageName = obj.icon;
    }
    return self;
}

@end

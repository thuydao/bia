//
//  MyAnnotationCallOut.h
//  Brewery
//
//  Created by Dao Duy Thuy on 8/13/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyAnnotationCallOut : MKAnnotationView

@property (nonatomic, strong) UIView *contentView;

//- (void)drawInContext:(CGContextRef)context;
//- (void)getDrawPath:(CGContextRef)context;

- (void)resetFrame;

@end

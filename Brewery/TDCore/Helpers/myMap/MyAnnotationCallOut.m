//
//  MyAnnotationCallOut.m
//  Brewery
//
//  Created by Dao Duy Thuy on 8/13/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "MyAnnotationCallOut.h"



#import <QuartzCore/QuartzCore.h>

#define  Arror_height 15


@implementation MyAnnotationCallOut

@synthesize contentView;

- (void)dealloc
{
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.canShowCallout = NO;
        self.centerOffset = CGPointMake(0, -68);
        self.frame = CGRectMake(0, 0, 280, 95);
        
        UIView *_contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - Arror_height)];
        _contentView.backgroundColor   = [UIColor clearColor];
        [self addSubview:_contentView];
        self.contentView = _contentView;
    }
    
    return self;
}

- (void)changeFrame
{
}

- (void)resetFrame
{
    self.frame = CGRectMake(0, 0, 280, 95);
    
    self.contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - Arror_height);
}


//- (void)drawInContext:(CGContextRef)context
//{
//    CGContextSetLineWidth(context, 2.0);
//    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
//    
//    [self getDrawPath:context];
//    CGContextFillPath(context);
//}
//
//- (void)getDrawPath:(CGContextRef)context
//{
//    CGRect rrect = self.bounds;
//	CGFloat radius = 4.0;
//    
//	CGFloat minx = CGRectGetMinX(rrect),
//    midx = CGRectGetMidX(rrect),
//    maxx = CGRectGetMaxX(rrect);
//	CGFloat miny = CGRectGetMinY(rrect),
//    maxy = CGRectGetMaxY(rrect)-Arror_height;
//    CGContextMoveToPoint(context, midx+Arror_height, maxy);
//    CGContextAddLineToPoint(context,midx, maxy+Arror_height);
//    CGContextAddLineToPoint(context,midx-Arror_height, maxy);
//    
//    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
//    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
//    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
//    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
//    CGContextClosePath(context);
//}
//
//- (void)drawRect:(CGRect)rect
//{
//	[self drawInContext:UIGraphicsGetCurrentContext()];
//    
//    self.layer.shadowColor = [[UIColor blackColor] CGColor];
//    self.layer.shadowOpacity = 0.5f;
//    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//}

@end

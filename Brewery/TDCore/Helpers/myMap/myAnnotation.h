
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface myAnnotation : NSObject <MKAnnotation>
@property (strong, nonatomic) NSString *title;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) Brewery *obj;

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title;
-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title imageName:(NSString *)imageName;
-(id) initWithObject:(Brewery *)obj;

@end

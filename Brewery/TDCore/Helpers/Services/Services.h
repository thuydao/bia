

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "TDReachability.h"
#import "SVProgressHUD-Prefix.pch"

typedef enum : NSUInteger {
    POST = 0,
    GET = 1,
} TypeAPI;

@interface Services : NSObject


#pragma mark - Untils
+ (BOOL)requestError:(id)responseObject;


#pragma mark - Call API
+ (void)callAPIWithUrl:(NSString *)url TypeAPI:(TypeAPI)type withParams:(NSMutableDictionary *)dictParams completed:(void (^)(NSDictionary *result, NSError *error))block;

//+ (void)addImageWithUrlAndParamContent:(NSString *)url  param:(NSMutableDictionary*)param   content:(UIImage*)contentImage completed:(void (^)(NSDictionary *result, NSError *error))block;


@end

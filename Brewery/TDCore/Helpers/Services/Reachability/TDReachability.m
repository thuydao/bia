//
//  TDReachability.m
//  FrameWork
//
//  Created by Dao Duy Thuy on 5/20/14.
//  Copyright (c) 2014 TD. All rights reserved.
//

#import "TDReachability.h"

@implementation TDReachability

+ (BOOL)connectedToNetwork
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

@end

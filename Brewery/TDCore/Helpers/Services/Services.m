

#import "Services.h"
#import "NSData+Base64.h"
#import "SVProgressHUD.h"

#define S_DOMAIN (@"http://192.168.1.147:8089/api")

@implementation Services


#pragma mark - Untils

+ (BOOL)requestError:(id)responseObject
{
    if ([[responseObject objectForKey:@"error_code"] integerValue] != 0) {
        [SVProgressHUD dismiss];
        //request not succes
        TDAlertWithParam(nil, [responseObject objectForKey:@"message"]);
        return YES;
    }
    return NO;
}

#pragma mark - SG
+ (BOOL )checkConnection
{
    if ( ![TDReachability connectedToNetwork] )
    {
        TDAlertWithParam(@"" ,@"No internet connection.");
        return NO;
    }
    
    return YES;
}




+ (void)callAPIWithUrl:(NSString *)url TypeAPI:(TypeAPI)type withParams:(NSMutableDictionary *)dictParams completed:(void (^)(NSDictionary *result, NSError *error))block
{    
    // Check network
    if ( [self checkConnection] )
    {
        // Use AFHTTPRequestOperationManager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        QLog(@"\n =============URL ============= \n %@ \n============= dictParams =========== \n %@",[S_DOMAIN stringByAppendingString:url], dictParams);
        
        // Request API
        //POST
        if (type == POST) {
            [manager POST:[S_DOMAIN stringByAppendingString:url] parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                // Success
                if ( !responseObject )
                {
                    TDAlertWithParam(nil,@"Have not response.");
                }
                else  QLog(@"Result : %@", (NSDictionary *)responseObject);
                
                //return block
                block(responseObject, nil);
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                // Fails
                QLog(@"Error : %@", [error description]);
                
                block(nil, error);
            }];
            
        }
        else if (type == GET)
        {
            [manager GET:[S_DOMAIN stringByAppendingString:url] parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                // Success
                if ( !responseObject )
                {
                    TDAlertWithParam(nil,@"Have not response.");
                }
                else  QLog(@"Result : %@", (NSDictionary *)responseObject);
                
                block(responseObject, nil);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                //fails
                QLog(@"Error : %@", [error description]);
                block(nil,error);
            }];
        }
    }
    else
    {
        //No internet connection
         [SVProgressHUD dismiss];
    }
}


#pragma mark - UPDATE AVATAR
//Use for upload avatar and change avatar
+ (void)addImageWithUrlAndParamContent:(NSString *)url  param:(NSMutableDictionary*)param   content:(UIImage*)contentImage completed:(void (^)(NSDictionary *result, NSError *error))block {
    
    // Check network
    if ( [self checkConnection] )
    {
        //encode image to base 64 string
        NSData *data = UIImagePNGRepresentation(contentImage);
        NSString *str= [data base64EncodedString];
        [param setValue:str forKey:@"content"];
        
        //use AFHTTPRequestOperationManager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        //request api
        [manager POST:[S_DOMAIN stringByAppendingString:url] parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //success
            block(responseObject, nil);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            //fails
            block(nil,error);
        }];
    }
    else
    {
        [SVProgressHUD dismiss];
    }
}


@end

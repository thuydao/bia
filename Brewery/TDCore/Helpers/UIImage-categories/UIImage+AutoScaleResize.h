//
//  UIImage+AutoScaleResize.h
//  HealthCare
//
//  Created by Bunlv on 6/19/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (AutoScaleResize)

- (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize;

@end

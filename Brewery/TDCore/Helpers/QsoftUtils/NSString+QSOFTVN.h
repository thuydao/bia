//
//  NSString+QSOFTVN.h
//  test
//
//  Created by Dao Duy Thuy on 4/14/14.
//  Copyright QSoft 2014. All rights reserved.
//  Provider : Dao Duy Thuy
//

#import <Foundation/Foundation.h>

@interface NSString (QSOFTVN)

//check
- (BOOL)isEmpty;

// Initialization
+ (NSString *)stringFromInteger:(NSInteger)anInteger;
+ (NSString *)stringFromFloat:(float)aFloat;
+ (NSString *)stringFromDouble:(double)aDouble;
+ (NSString *)stringWithUUID;

// Validation
- (BOOL)isValidEmail;
- (BOOL)isValidURI;

// Removing & Trimming
- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet*)characterSet;
- (NSString *)stringByReplacingCharactersInSet:(NSCharacterSet*)characterSet withString:(NSString*)string;
- (NSString *)stringByTrimmingWhitespace;
- (NSString *)stringByRemovingAmpEscapes;
- (NSString *)stringByAddingAmpEscapes;

// Query
- (BOOL)containsString:(NSString *)s;
- (BOOL)containsCaseInsensitiveString:(NSString *)s;
- (BOOL)caseInsensitiveHasPrefix:(NSString *)s;
- (BOOL)caseInsensitiveHasSuffix:(NSString *)s;
- (BOOL)isCaseInsensitiveEqual:(NSString *)s;

- (BOOL )isEqualToStringInsensitive:(NSString *)strNeedEqual;

@end

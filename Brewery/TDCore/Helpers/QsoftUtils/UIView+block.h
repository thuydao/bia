//
//  UITapGestureRecognizer+block.h
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/11/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>


typedef void (^UITapGestureRecognizerBlock) (UITapGestureRecognizer *sender);

@interface UIView (block)

@property (copy, nonatomic) UITapGestureRecognizerBlock tapGestureRecognizerBlock;

- (void)handleTapBlock:(UITapGestureRecognizerBlock)block;

@end

//
//  UIViewController+Protocol.h
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/11/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@protocol baseDelgate <NSObject>

//demo
- (void)demo;


// TraineeAreasDelegate
- (void) didSelectedAreaRow:(id)areaE;

// TraineeCitiesDelegate
- (void) didSelectedCityRow:(id)cityE;

// TraineeCountriesDelegate
- (void) didSelectedCountryRow:(id)countryE;

@end

@interface UIViewController (Protocol)

@property (assign) id<baseDelgate> baseDelegate;

@end

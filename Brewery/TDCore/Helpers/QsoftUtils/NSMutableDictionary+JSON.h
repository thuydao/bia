//
//  NSMutableDictionary+BunLV.h
//  HealthCare
//
//  Created by Bunlv on 7/7/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (JSON)

- (NSString *)parseJSON;

@end

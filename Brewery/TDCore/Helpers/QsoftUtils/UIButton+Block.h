//
//  UIButton+Block.h
//  BoothTag
//
//  Created by Thuy Dao on 4/22/12.
//  Copyright (c) 2012 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

typedef void (^UIButtonBlock) (UIButton *sender);

@interface UIButton (Block)
@property (copy, nonatomic) UIButtonBlock touchUpInside;

- (void)clickBtn:(UIButtonBlock)touchUpInside;

@end
//
//  Defines.h
//  Hoibi
//
//  Created by Dao Duy Thuy on 5/19/14.
//  Copyright (c) 2014 TD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Untils)

- (id)safeObjectForKey:(NSString *)key;
- (NSInteger)intForKey:(NSString *)key;
- (NSInteger)floatForKey:(NSString *)key;
- (NSString*)stringForKey:(NSString *)key;
- (BOOL)boolForKey:(NSString *)key;
- (NSDictionary*)dictionaryForKey:(NSString *)key;
- (NSArray *)arrayForKey:(NSString *)key;
- (NSMutableArray *)mutableArrayForKey:(NSString *)key;
- (NSMutableDictionary *)mutableDictionaryForKey:(NSString *)key;

@end

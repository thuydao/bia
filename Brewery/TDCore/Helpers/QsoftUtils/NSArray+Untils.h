//
//  NSArray+Untils.h
//  FrameWork
//
//  Created by Dao Duy Thuy on 6/3/14.
//  Copyright (c) 2014 TD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Untils)
- (BOOL)isEmpty;
- (id)objectSafeAtIndex:(NSUInteger)index;
- (id)firstObject;
- (id)lastObject;
@end

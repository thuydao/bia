//
//  UITapGestureRecognizer+block.m
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/11/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "UIView+block.h"

static const void *UITapGestureRecognizerBlockVoid                   = &UITapGestureRecognizerBlockVoid;

@implementation UIView (block)

#pragma mark - Setter & Getter

- (UITapGestureRecognizerBlock)tapGestureRecognizerBlock
{
    return objc_getAssociatedObject(self, UITapGestureRecognizerBlockVoid);
}

- (void)setTapGestureRecognizerBlock:(UITapGestureRecognizerBlock)tapGestureRecognizerBlock
{
    objc_setAssociatedObject(self, UITapGestureRecognizerBlockVoid, tapGestureRecognizerBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)handleTapBlock:(UITapGestureRecognizerBlock)block
{
    self.tapGestureRecognizerBlock = block;
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:(id)self action:@selector(UITapGestureRecognizerBlockVoid:)]];
}

#pragma mark - handleTap

- (void)UITapGestureRecognizerBlockVoid:(UITapGestureRecognizer *)sender
{
    self.tapGestureRecognizerBlock(sender);
}

@end

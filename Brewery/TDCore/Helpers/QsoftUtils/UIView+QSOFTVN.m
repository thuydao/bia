//
//  NSString+QSOFTVN.h
//  test
//
//  Created by Dao Duy Thuy on 4/14/14.
//  Copyright QSoft 2014. All rights reserved.
//  Provider : Dao Duy Thuy
//

#import "UIView+QSOFTVN.h"

@implementation UIView (QSOFTVN)

#pragma mark - GetView

- (UIImageView *)imageViewWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UIButton *)buttonWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UIWebView *)webViewWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UILabel *)labelWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UISwitch *)switchWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UITableView *)tableViewWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UIScrollView *)scrollViewWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

- (UITextField *)textFieldWithTag:(NSInteger )tag
{
    return (id )[self viewWithTag:tag];
}

- (UITextView *)textViewWithTag:(NSInteger)tag
{
    return (id )[self viewWithTag:tag];
}

#pragma mark - Add Line
- (void)addTopLineColor:(UIColor *)color
{
    [self addTopLineColor:color withAlpha:1 withSize:1];
}

- (void)addTopLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size
{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, self.frame.size.width, 1)];
    [imv setBackgroundColor:color];
    [imv setAlpha:alpha];
    
    [self addSubview:imv];
}

- (void)addBottomLineColor:(UIColor *)color
{
    [self addBottomLineColor:color withAlpha:1 withSize:1];
}

- (void)addBottomLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size
{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - size, self.frame.size.width, size)];
    [imv setBackgroundColor:color];
    [imv setAlpha:alpha];
    
    [self addSubview:imv];
}

- (void)addLeftLineColor:(UIColor *)color
{
    [self addLeftLineColor:color withAlpha:1 withSize:1];
}

- (void)addLeftLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size
{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, self.frame.size.height)];
    [imv setBackgroundColor:color];
    [imv setAlpha:alpha];
    
    [self addSubview:imv];
}

- (void)addRightLineColor:(UIColor *)color
{
    [self addRightLineColor:color withAlpha:1 withSize:1];
}

- (void)addRightLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size
{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width -1 , 0, 1, self.frame.size.height)];
    [imv setBackgroundColor:color];
    [imv setAlpha:alpha];
    
    [self addSubview:imv];
}

#pragma mark - layer
- (void)borderWithBorderWidth:(float )borderWidth withBoderColor:(UIColor *)color withCornerRadius:(float )cornerRadius andWithMasksToBounds:(BOOL )isMasksToBounds
{
    self.layer.borderColor = [color CGColor];
    self.layer.borderWidth = borderWidth;
    
    self.layer.cornerRadius = cornerRadius;
    
    self.layer.masksToBounds = isMasksToBounds;
}

- (void)cornerTopLeftWithCornerRadius:(float )cornerRadius
{
    UIRectCorner corner = UIRectCornerTopLeft;
    
    [self cornerWithRectConrder:corner withCornerRadius:cornerRadius];
}

- (void)cornerTopRightWithCornerRadius:(float )cornerRadius
{
    UIRectCorner corner = UIRectCornerTopRight;
    
    [self cornerWithRectConrder:corner withCornerRadius:cornerRadius];
}

- (void)cornerBottomLeftWithCornerRadius:(float )cornerRadius
{
    UIRectCorner corner = UIRectCornerBottomLeft;
    
    [self cornerWithRectConrder:corner withCornerRadius:cornerRadius];
}

- (void)cornerBottomRightWithCornerRadius:(float )cornerRadius
{
    UIRectCorner corner = UIRectCornerBottomRight;
    
    [self cornerWithRectConrder:corner withCornerRadius:cornerRadius];
}

- (void)cornerWithRectConrder:(UIRectCorner )corner withCornerRadius:(float )cornerRadius
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

#pragma mark - backgroundImage

- (void)setBackgroundImage:(UIImage*)image
{
    self.backgroundColor = [UIColor colorWithPatternImage:image];
}

@end

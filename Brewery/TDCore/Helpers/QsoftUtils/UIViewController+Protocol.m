//
//  UIViewController+Protocol.m
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/11/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "UIViewController+Protocol.h"

static const void *BaseDelegateVoid = &BaseDelegateVoid;

@implementation UIViewController (Protocol)

#pragma mark - Setter & Getter

- (void)setBaseDelegate:(id<baseDelgate>)baseDelegate
{
    objc_setAssociatedObject(self, BaseDelegateVoid, baseDelegate, OBJC_ASSOCIATION_ASSIGN);
}

- (id<baseDelgate>)baseDelegate
{
    return objc_getAssociatedObject(self, BaseDelegateVoid);
}

@end

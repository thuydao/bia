//
//  UIButton+Block.h
//  BoothTag
//
//  Created by Thuy Dao on 4/22/12.
//  Copyright (c) 2012 Thuy Dao. All rights reserved.
//

#import "UIButton+Block.h"

static const void *touchUpInsideVoid                   = &touchUpInsideVoid;

@implementation UIButton (Block)

#pragma mark - Setter & Getter

- (UIButtonBlock)touchUpInside
{
    return objc_getAssociatedObject(self, touchUpInsideVoid);
}

- (void)setTouchUpInside:(UIButtonBlock)touchUpInside
{
    objc_setAssociatedObject(self, touchUpInsideVoid, touchUpInside, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)clickBtn:(UIButtonBlock)touchUpInside
{
    self.touchUpInside = touchUpInside;
    
    [self addTarget:self action:@selector(clickBtnBlock:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - clickButton

- (void)clickBtnBlock:(id)sender
{
    self.touchUpInside(sender);
}

@end
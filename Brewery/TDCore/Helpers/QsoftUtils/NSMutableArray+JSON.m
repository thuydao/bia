//
//  NSMutableArray+BunLV.m
//  HealthCare
//
//  Created by Bunlv on 7/7/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "NSMutableArray+JSON.h"

@implementation NSMutableArray (JSON)

- (NSString *)parseJSON
{
    NSLog(@"Data : %@", self);
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    
    if ( error )
    {
        return @"";
    }
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

@end

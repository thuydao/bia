//
//  UIImageView+QSOFTVN.h
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/9/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (QSOFTVN)

- (void)addTopButtonWithTarget:(id)target action:(SEL)action;

@end

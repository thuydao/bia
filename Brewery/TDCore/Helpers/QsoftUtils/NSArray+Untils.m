//
//  NSArray+Untils.m
//  FrameWork
//
//  Created by Dao Duy Thuy on 6/3/14.
//  Copyright (c) 2014 TD. All rights reserved.
//

#import "NSArray+Untils.h"

@implementation NSArray (Untils)

- (BOOL)isEmpty
{
    if (self == nil || [self count]  == 0) {
        return YES;
    }
    return NO;
}

- (id)objectSafeAtIndex:(NSUInteger)index
{
    if (self || [self count] > index) {
        return [self objectAtIndex:index];
    }
    return nil;
}

- (id)firstObject
{
    if (self || [self count] > 0) {
        return [self objectAtIndex:0];
    }
    return nil;
}

- (id)lastObject
{
    if (![self isEmpty]) {
        return  [self objectAtIndex:[self count] - 1];
    }
    return nil;
}

@end

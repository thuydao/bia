//
//  QSUntils.h
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/9/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#pragma mark - UIViewController
#import "UIViewController+Protocol.h"   //manage delegate on viewcontroller

#pragma mark - View

#import "UIView+block.h"   //handleTap - click on view
//- (void)handleTapBlock:(UITapGestureRecognizerBlock)block;

#import "UIView+position.h"  // manage position
//- (void)addCenteredSubview:(UIView *)subview;
//- (void)moveToCenterOfSuperview;
//- (void)centerVerticallyInSuperview;
//- (void)centerHorizontallyInSuperview;

#import "UIView+QSOFTVN.h"
//#pragma mark - Get View
//- (UIImageView *)imageViewWithTag:(NSInteger)tag;
//- (UIButton *)buttonWithTag:(NSInteger)tag;
//- (UIWebView *)webViewWithTag:(NSInteger)tag;
//- (UILabel *)labelWithTag:(NSInteger)tag;
//- (UISwitch *)switchWithTag:(NSInteger)tag;
//- (UITableView *)tableViewWithTag:(NSInteger)tag;
//- (UIScrollView *)scrollViewWithTag:(NSInteger)tag;
//- (UITextField *)textFieldWithTag:(NSInteger )tag;
//- (UITextView *)textViewWithTag:(NSInteger)tag;
//
//#pragma mark - Add Line
//// Add line top
//- (void)addTopLineColor:(UIColor *)color;
//- (void)addTopLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;
//
//// Add line botton
//- (void)addBottomLineColor:(UIColor *)color;
//- (void)addBottomLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;
//
//// Add line left
//- (void)addLeftLineColor:(UIColor *)color;
//- (void)addLeftLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;
//
//// Add line right
//- (void)addRightLineColor:(UIColor *)color;
//- (void)addRightLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;
//
//
//#pragma mark - layer
//
//- (void)borderWithBorderWidth:(float )borderWidth withBoderColor:(UIColor *)color withCornerRadius:(float )cornerRadius andWithMasksToBounds:(BOOL )isMasksToBounds;
//
//#pragma mark - backgroundImage
//
//- (void)setBackgroundImage:(UIImage*)image;

#pragma mark - UIImageView

#import "UIImageView+QSOFTVN.h"
//- (void)addTopButtonWithTarget:(id)target action:(SEL)action;

#pragma mark - UIButton

#import "UIButton+Block.h"
//- (void)clickBtn:(UIButtonBlock)touchUpInside;

#pragma mark - NSString

#import "NSString+QSOFTVN.h"
////check
//- (BOOL)isEmpty;
//
//// Initialization
//+ (NSString *)stringFromInteger:(NSInteger)anInteger;
//+ (NSString *)stringFromFloat:(float)aFloat;
//+ (NSString *)stringFromDouble:(double)aDouble;
//+ (NSString *)stringWithUUID;
//
//// Validation
//- (BOOL)isValidEmail;
//- (BOOL)isValidURI;
//
//// Removing & Trimming
//- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet*)characterSet;
//- (NSString *)stringByReplacingCharactersInSet:(NSCharacterSet*)characterSet withString:(NSString*)string;
//- (NSString *)stringByTrimmingWhitespace;
//- (NSString *)stringByRemovingAmpEscapes;
//- (NSString *)stringByAddingAmpEscapes;
//
//// Query
//- (BOOL)containsString:(NSString *)s;
//- (BOOL)containsCaseInsensitiveString:(NSString *)s;
//- (BOOL)caseInsensitiveHasPrefix:(NSString *)s;
//- (BOOL)caseInsensitiveHasSuffix:(NSString *)s;
//- (BOOL)isCaseInsensitiveEqual:(NSString *)s;

#pragma mark - NSDate

#import "NSDate+QSOFTVN.h"

#pragma mark - UIAlertView

#import "UIAlertView+Blocks.h"

#pragma mark - Array

#import "NSMutableArray+JSON.h"
//- (NSString *)parseJSON;

#import "NSArray+Untils.h"
//- (BOOL)isEmpty;
//- (id)objectSafeAtIndex:(NSUInteger)index;
//- (id)firstObject;
//- (id)lastObject;

#pragma mark - Dictionary

#import "NSMutableDictionary+JSON.h"
//- (NSString *)parseJSON;

#import "NSDictionary+Untils.h"
//- (id)safeObjectForKey:(NSString *)key;
//- (NSInteger)intForKey:(NSString *)key;
//- (NSInteger)floatForKey:(NSString *)key;
//- (NSString*)stringForKey:(NSString *)key;
//- (BOOL)boolForKey:(NSString *)key;
//- (NSDictionary*)dictionaryForKey:(NSString *)key;
//- (NSArray *)arrayForKey:(NSString *)key;
//- (NSMutableArray *)mutableArrayForKey:(NSString *)key;
//- (NSMutableDictionary *)mutableDictionaryForKey:(NSString *)key;




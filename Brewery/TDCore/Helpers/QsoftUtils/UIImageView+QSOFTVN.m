//
//  UIImageView+QSOFTVN.m
//  HealthCare
//
//  Created by Dao Duy Thuy on 7/9/14.
//  Copyright (c) 2014 QSoft. All rights reserved.
//

#import "UIImageView+QSOFTVN.h"

@implementation UIImageView (QSOFTVN)

- (void)addTopButtonWithTarget:(id)target action:(SEL)action
{
    float w = 30;
    if ( self.frame.size.width > 30 )
    {
        w = self.frame.size.width;
    }
    
    float h = 30;
    if ( self.frame.size.height > 30 )
    {
        h = self.frame.size.height;
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, w, h);
    btn.center = self.center;
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [self.superview addSubview:btn];
}

@end

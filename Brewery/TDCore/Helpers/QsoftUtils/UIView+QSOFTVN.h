//
//  NSString+QSOFTVN.h
//  test
//
//  Created by Dao Duy Thuy on 4/14/14.
//  Copyright QSoft 2014. All rights reserved.
//  Provider : Dao Duy Thuy
//

#import <UIKit/UIKit.h>

@interface UIView (QSOFTVN)

#pragma mark - Get View
- (UIImageView *)imageViewWithTag:(NSInteger)tag;
- (UIButton *)buttonWithTag:(NSInteger)tag;
- (UIWebView *)webViewWithTag:(NSInteger)tag;
- (UILabel *)labelWithTag:(NSInteger)tag;
- (UISwitch *)switchWithTag:(NSInteger)tag;
- (UITableView *)tableViewWithTag:(NSInteger)tag;
- (UIScrollView *)scrollViewWithTag:(NSInteger)tag;
- (UITextField *)textFieldWithTag:(NSInteger )tag;
- (UITextView *)textViewWithTag:(NSInteger)tag;

#pragma mark - Add Line
// Add line top
- (void)addTopLineColor:(UIColor *)color;
- (void)addTopLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;

// Add line botton
- (void)addBottomLineColor:(UIColor *)color;
- (void)addBottomLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;

// Add line left
- (void)addLeftLineColor:(UIColor *)color;
- (void)addLeftLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;

// Add line right
- (void)addRightLineColor:(UIColor *)color;
- (void)addRightLineColor:(UIColor *)color withAlpha:(float )alpha withSize:(float )size;


#pragma mark - layer

- (void)borderWithBorderWidth:(float )borderWidth withBoderColor:(UIColor *)color withCornerRadius:(float )cornerRadius andWithMasksToBounds:(BOOL )isMasksToBounds;
- (void)cornerTopLeftWithCornerRadius:(float )cornerRadius;
- (void)cornerTopRightWithCornerRadius:(float )cornerRadius;
- (void)cornerBottomLeftWithCornerRadius:(float )cornerRadius;
- (void)cornerBottomRightWithCornerRadius:(float )cornerRadius;

#pragma mark - backgroundImage

- (void)setBackgroundImage:(UIImage*)image;

@end

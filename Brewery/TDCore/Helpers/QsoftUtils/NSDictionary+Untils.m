//
//  Defines.h
//  Hoibi
//
//  Created by Dao Duy Thuy on 5/19/14.
//  Copyright (c) 2014 TD. All rights reserved.
//

#import "NSDictionary+Untils.h"

@implementation NSDictionary (Untils)

- (BOOL)hasProperty:(NSString *)key
{
    id object = [self objectForKey:key];
    
    if (object == nil)
    {
        return NO;
    }
    
    return YES;
}

- (id)safeObjectForKey:(NSString *)key
{
    id object = [self objectForKey:key];
    
    if (object == nil || object == NULL || object == [NSNull null])
    {
        return nil;
    }
    
    return object;
}

- (NSString*)stringForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if (!object || [object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]])
    {
        return @"";
    }
    
    return [NSString stringWithFormat:@"%@", object];
}

- (NSInteger)intForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if (!object || [object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]])
    {
        return 0;
    }
    
    return [object intValue];
}

- (NSInteger)floatForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if (!object || [object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]])
    {
        return 0;
    }
    
    return [object floatValue];
}

- (BOOL)boolForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if ( !object || [object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]] )
    {
        return NO;
    }
    
    return [object boolValue];
}

- (NSDictionary*)dictionaryForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if ([object isKindOfClass:[NSDictionary class]])
    {
        return object;
    }
    
    return [NSDictionary new];
}

- (NSArray *)arrayForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if ([object isKindOfClass:[NSArray class]])
    {
        return object;
    }
    
    return [NSArray new];
}

- (NSMutableArray *)mutableArrayForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if ([object isKindOfClass:[NSArray class]])
    {
        return object;
    }
    
    return [NSMutableArray new];
}

- (NSMutableDictionary *)mutableDictionaryForKey:(NSString *)key
{
    id object = [self safeObjectForKey:key];
    
    if ([object isKindOfClass:[NSDictionary class]])
    {
        return object;
    }
    
    return [NSMutableDictionary new];
}

@end

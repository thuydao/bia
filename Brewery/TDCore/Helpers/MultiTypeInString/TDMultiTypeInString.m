//
//  TDMultiTypeInString.m
//  Brewery
//
//  Created by Thuy Dao on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import "TDMultiTypeInString.h"

@implementation TDMultiTypeInString

+ (NSMutableAttributedString*)multiFontWithStrings:(NSArray *)arrStr fonts:(NSArray *)arrFont
{
    NSString *string = [arrStr componentsJoinedByString:@""];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSInteger start = 0;
    
    for (NSInteger i = 0; i < arrStr.count; i ++) {
        NSString *aStr = [arrStr objectAtIndex:i];
        UIFont *aFont = [arrFont objectAtIndex:i];
        [str addAttribute:NSFontAttributeName value:aFont range:NSMakeRange(start, aStr.length)];
        start = start + aStr.length;
    }
    
    return str;
}

+ (NSMutableAttributedString*)multiColorWithStrings:(NSArray *)arrStr colors:(NSArray *)colors
{
    NSString *string = [arrStr componentsJoinedByString:@""];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSInteger start = 0;
    
    for (NSInteger i = 0; i < arrStr.count; i ++) {
        NSString *aStr = [arrStr objectAtIndex:i];
        UIColor *aColor = [colors objectAtIndex:i];
        [str addAttribute:NSBackgroundColorAttributeName value:aColor range:NSMakeRange(start, aStr.length)];
        start = start + aStr.length;
    }
    
    return str;
}

@end

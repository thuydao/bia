//
//  TDMultiTypeInString.h
//  Brewery
//
//  Created by Thuy Dao on 8/15/14.
//  Copyright (c) 2014 BB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDMultiTypeInString : NSObject

+ (NSMutableAttributedString*)multiFontWithStrings:(NSArray *)arrStr fonts:(NSArray *)arrFont;
+ (NSMutableAttributedString*)multiColorWithStrings:(NSArray *)arrStr colors:(NSArray *)colors;

@end
